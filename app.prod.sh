#!/bin/bash
set -a

export STAGE="prod"

echo "Enter the number of current replica: "
read -r REPLICA
export REPLICA

if [ "$REPLICA" -lt 1 ] || [ "$REPLICA" -gt 10 ]; then
  echo "Replica must be between 1 and 10"
  exit 1
fi

./app.runscript.sh "$@"
