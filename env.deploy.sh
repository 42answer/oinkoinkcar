#!/bin/bash
set -a

DEBUG=0
DOWN=0
FORCE_RECREATE=""
BUILD=0
RUN=0
SHOWHELP=0
NO_FLAGS=1

while [[ $# -gt 0 ]]; do
    case "$1" in
        -d|--debug)
            DEBUG=1
            ;;
        --down)
            DOWN=1
            NO_FLAGS=0
            ;;
        -f|--force)
            FORCE_RECREATE="--force-recreate"
            ;;
        -b|--build)
            BUILD=1
            NO_FLAGS=0
            ;;
        -r|--run)
            RUN=1
            NO_FLAGS=0
          ;;
        -h|--help)
            SHOWHELP=1
            ;;
        *)
            echo "Unknown option: $1, use --help to see the available options."
            exit 1
            ;;
    esac
    shift
done

if [ $SHOWHELP -eq 1 ] || [ $NO_FLAGS -eq 1 ]; then
    echo "Usage: ./runscript.sh [OPTIONS]"
    echo "Options:"
    echo "  -d, --debug          Run the Nomeroff server in debug mode."
    echo "  --down               Stop and remove the containers."
    echo "  -f, --force          Force recreate the containers."
    echo "  -b, --build          Build the containers."
    echo "  -r, --run            Run the environment servers."
    echo "  -h, --help           Show this help message."
    exit 0
fi

if [ ! -f ./.envrc.prod ]; then
    echo "The .envrc.prod file does not exist. Please create it before running this script."
    exit 1
fi

echo "Sourcing the .envrc.prod file..."
source ./.envrc.prod

if [ -n "$YAC_LOCKBOX_SID" ]; then
    echo "\$YAC_LOCKBOX_SID is found. Loading secrets from Yandex.Cloud Lockbox..."
    IAM_TOKEN="$(yc iam create-token)"
    secrets=$(curl -X GET -H "Authorization: Bearer ${IAM_TOKEN}" https://payload.lockbox.api.cloud.yandex.net/lockbox/v1/secrets/$YAC_LOCKBOX_SID/payload)

    # Thanks a lot to the author of this gist https://gist.github.com/IAmStoxe/a36b6f043819fad1821e7cfd7e903a5b
    for row in $(echo "${secrets}" | jq -r '.entries | .[] | @base64'); do
        _jq() {
        echo "${row}" | base64 --decode | jq -r "${1}"
        }

        name=$(_jq '.key')
        value=$(_jq '.textValue')

        export "$name"="$value"
    done
else
    echo "\$YAC_LOCKBOX_SID is not set. Skipping loading secrets from Yandex.Cloud Lockbox..."
fi

if [ -z "$TRAEFIK_AUTH_USER" ] || [ -z "$TRAEFIK_AUTH_PASSWORD" ]; then
    echo "The TRAEFIK_AUTH_USER and TRAEFIK_AUTH_PASSWORD variables are not set. Please set them in the .envrc.prod file or in the Yandex.Cloud Lockbox secrets."
    exit 1
fi

export TRAEFIK_AUTH_PASSWORD_HASH=$(htpasswd -nbB ${TRAEFIK_AUTH_USER} ${TRAEFIK_AUTH_PASSWORD} | cut -d ":" -f 2)

USER=oinkuser
GROUP=oinkgroup
USER_ID=1000
GROUP_ID=1000

export USER GROUP USER_ID GROUP_ID

if [ $DOWN -eq 1 ]; then
    echo "Stopping and removing the containers..."
    docker compose -f docker-compose.prod.env.yml -p "$COMPOSE_PROJECT_NAME" down
    exit 0
fi

if [ $DEBUG -eq 1 ]; then
    export NOMEROFF_NET_RUN_SRVR_COMMAND="flask run -h 0.0.0.0 -p 3116"
else
    export NOMEROFF_NET_RUN_SRVR_COMMAND="gunicorn --timeout 240 main:app --threads 2 --workers 2 --log-level info --bind \"0.0.0.0:3116\""
fi

if [ $BUILD -eq 1 ]; then
    echo "Pulling the latest changes from the repository..."
    git pull
    echo "Building the containers..."
    docker compose -f docker-compose.prod.env.yml -p "$COMPOSE_PROJECT_NAME" build
fi

if [ $RUN -eq 1 ]; then
    echo "Running the environment servers..."
    docker compose -f docker-compose.prod.env.yml -p "$COMPOSE_PROJECT_NAME" up $FORCE_RECREATE
fi

exit 0
