worker_processes auto;
worker_rlimit_nofile 65535;

events {
    worker_connections 65535;
    accept_mutex on;
    use epoll;
}

http {

    server_tokens off;
    client_body_buffer_size 1K;
    client_header_buffer_size 1k;

    include /etc/nginx/mime.types;

    sendfile on;
    gzip on;
    gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon;

    error_log stderr warn;
    access_log /dev/stdout;

    error_log /var/log/nginx/$STAGE/$REPLICA/error.log warn;
    access_log /var/log/nginx/$STAGE/$REPLICA/access.log;

    server {
        server_name xxkap.app staging.xxkap.app;
        client_max_body_size 50M;
        listen 80;

        location /static/ {
            allow $SUBNET;
            deny all;
            root /;
            gzip_static on;
        }

        location /robots.txt {
            allow $SUBNET;
            deny all;
            return 200 "$ROBOTSTXT";
        }

        location /nomeroff/status {
            allow $SUBNET;
            deny all;
            proxy_pass http://nomeroff:3116/status;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header Host $http_host;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $http_connection;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_redirect off;
        }

        location /api/v1/violations/ {
            proxy_pass $BACKEND_URL;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header Host $http_host;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $http_connection;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_redirect off;

            gzip on;
            gzip_min_length 1000;
            gzip_comp_level 4;
            gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon application/geo+json;

            add_header Cache-Control "no-store, private, must-revalidate";
            add_header Pragma "no-cache";
            expires off;
        }

        location / {
            allow $SUBNET;
            deny all;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header Host $http_host;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $http_connection;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_redirect off;

            location ^~ /assets/ {
                root /static;
                try_files $uri $uri/ =404;
                gzip_static on;
                expires max;
                add_header Cache-Control "public, max-age=31536000, s-maxage=31536000, immutable";
            }

            location ~* \.(png|ico)$ {
                root /static;
                try_files $uri $uri/ =404;
                gzip_static on;
                expires max;
                add_header Cache-Control "public, max-age=31536000, s-maxage=31536000, immutable";
            }

            location ^~ /workbox- {
                root /static;
                try_files $uri $uri/ =404;
                gzip_static on;
                expires max;
                add_header Cache-Control "public, max-age=31536000, s-maxage=31536000, immutable";
            }

            location / {
                root /static;
                autoindex off;
                if_modified_since off;
                expires off;
                etag off;
                gzip off;
                add_header Cache-Control "private, no-store, no-cache, max-age=0, s-maxage=0, proxy-revalidate, must-revalidate" always;
                add_header Pragma "no-cache" always;
                add_header Last-Modified $date_gmt always;
                try_files $uri /index.html =404;
            }
        }
    }

    server {
        server_name xn----7sba2bdrgbnmid4o.xn--p1ai;
        client_max_body_size 50M;
        listen 80;

        location /parkings-static/ {
            allow $SUBNET;
            deny all;
            root /;
            gzip_static on;
        }

        location /robots.txt {
            allow $SUBNET;
            deny all;
            return 200 "$ROBOTSTXT";
        }

        location / {
            allow $SUBNET;
            deny all;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header Host $http_host;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $http_connection;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_redirect off;

            location ^~ /assets/ {
                root /parkings-static;
                try_files $uri $uri/ =404;
                gzip_static on;
                expires max;
                add_header Cache-Control "public, max-age=31536000, s-maxage=31536000, immutable";
            }

            location ~* \.(png|ico)$ {
                root /parkings-static;
                try_files $uri $uri/ =404;
                gzip_static on;
                expires max;
                add_header Cache-Control "public, max-age=31536000, s-maxage=31536000, immutable";
            }

            location ^~ /workbox- {
                root /parkings-static;
                try_files $uri $uri/ =404;
                gzip_static on;
                expires max;
                add_header Cache-Control "public, max-age=31536000, s-maxage=31536000, immutable";
            }

            location / {
                root /parkings-static;
                autoindex off;
                if_modified_since off;
                expires off;
                etag off;
                gzip off;
                add_header Cache-Control "private, no-store, no-cache, max-age=0, s-maxage=0, proxy-revalidate, must-revalidate" always;
                add_header Pragma "no-cache" always;
                add_header Last-Modified $date_gmt always;
                try_files $uri /index.html =404;
            }
        }
    }

    server {
        listen 80;
        server_name app.xxkap.app;
        autoindex off;
        if_modified_since off;
        expires off;
        gzip off;
        add_header Cache-Control "private, no-store, no-cache, max-age=0, s-maxage=0, proxy-revalidate, must-revalidate" always;
        add_header Pragma "no-cache" always;
        add_header Last-Modified $date_gmt always;
        return 301 $scheme://xxkap.app$request_uri;
    }
}
