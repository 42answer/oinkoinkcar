#!/bin/bash
set -e
if [[ $EUID -ne 0 ]]; then
    echo -e "\e[31mError: This script must be run as root\e[0m"
    exit 1
fi

C="RU"
ST="Saratov"
L="Balakovo"
O="FKM.ONE"
OU="DevelopmentDepartment"
emailAddress="brothers@yarvis.tech"
CN="dev.xxkap.app"

if [ ! -d "./ssl" ]; then
    mkdir "./ssl"
fi

# create a config file if it doesn't exist
if [ ! -f "./ssl/$CN.cnf" ]; then
cat > ssl/$CN.cnf <<EOL
[req]
default_bits = 2048
prompt = no
default_md = sha256
distinguished_name = dn
req_extensions = req_ext

[dn]
C=$C
ST=$ST
L=$L
O=$O
OU=$OU
emailAddress=$emailAddress
CN=$CN

[req_ext]
subjectAltName = @alt_names

[alt_names]
DNS.1   = $CN
EOL
fi

# generate the rootCA key and certificate if they don't exist
if [ ! -f "./ssl/rootCA.key" ]; then
    openssl genrsa -out ssl/rootCA.key 2048
    openssl req -x509 -new -nodes -key ssl/rootCA.key -sha256 -days 1024 -out ssl/rootCA.pem \
    -subj "/C=$C/ST=$ST/L=$L/O=$O/OU=$OU/CN=RootCA/emailAddress=$emailAddress"
    # add the certificate to the system keychain
    cp ssl/rootCA.pem /usr/local/share/ca-certificates/
    update-ca-certificates
fi

openssl genrsa -out ssl/$CN.key 2048
openssl req -new -key ssl/$CN.key -out ssl/$CN.csr -config ssl/$CN.cnf

openssl x509 -req -in ssl/$CN.csr -CA ssl/rootCA.pem -CAkey ssl/rootCA.key -CAcreateserial \
-out ssl/$CN.crt -days 500 -sha256 -extfile ssl/$CN.cnf -extensions req_ext

if ! grep -q "$CN" /etc/hosts; then
    echo "127.0.0.1 $CN" | sudo tee -a /etc/hosts
fi



CURRENT_USER=$(whoami)
CURRENT_GROUP=$(id -gn)

chown -R $CURRENT_USER:$CURRENT_GROUP ssl/

echo -e "\e[32mCertificate for $CN generated successfully\e[0m"
