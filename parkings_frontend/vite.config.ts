// frontend/vite.config.ts
import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from "vite-plugin-pwa";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({
      registerType: "autoUpdate",
      injectRegister: "auto",
      devOptions: {
        enabled: true,
      },
      workbox: {
        maximumFileSizeToCacheInBytes: 10 * 1024 * 1024,
        globPatterns: ["**/*.{js,ts,css,scss,html,ico,png,svg,vue,otf,ttf}"],
        navigateFallback: '/index.html',
        navigateFallbackAllowlist: [
          new RegExp('^/(\\?.*)?$'),
        ],
      },
      includeAssets: [
        "favicon.ico",
        "btn_google_light_normal.svg",
        "btn_google_light_pressed.svg",
        "logo.png",
      ],
      manifest: {
        name: "Карта стоянок",
        short_name: "КСБ",
        description: "Некоммерческий сервис для поиска места хранения автомобиля",
        background_color: "#ffffff",
        theme_color: "#ffffff",
        display: "standalone",
        start_url: "/",
        scope: "/",
        handle_links: "preferred",
        icons: [
          {
            src: "pwa-192x192.png",
            sizes: "192x192",
            type: "image/png",
          },
          {
            src: "pwa-512x512.png",
            sizes: "512x512",
            type: "image/png",
          },
        ],
      },
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    port: 5173
  },
  css: {
    devSourcemap: true,
  },
})
