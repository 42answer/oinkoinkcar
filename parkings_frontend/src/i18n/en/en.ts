import mdb from "@/i18n/en/mdb";
import pageTitles from "@/i18n/en/page-titles";
import content from "@/i18n/en/content";

const en = {
  mdb,
  pageTitles,
  content,
};

export default en;
