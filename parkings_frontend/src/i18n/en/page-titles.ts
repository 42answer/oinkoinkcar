const pageTitles = {
  home: 'Balakovo Parking Map',
  default: 'Balakovo Parking Map',
  header: 'parking map',
  subtitle: 'IN THE BALAKOVO CITY',
};

export default pageTitles;
