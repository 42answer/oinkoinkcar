const content = {
  close: "Закрыть",
  address: "адрес стоянки",
  space: "мест",
  spaces: "наличие мест",
  empty: "свободно",
  outOf: "из",
  phone: "Телефон",
  monthlyRent: "стоимость аренды",
  rubPerMonth: "руб/мес",
  carParkTitle: "Информация о стоянке",
  updated: "обновлено: ",
  reportInaccuracy: "Сообщить о неточности",
};

export default content;
