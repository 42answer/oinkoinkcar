import mdb from "@/i18n/ru/mdb";
import pageTitles from "@/i18n/ru/page-titles";
import content from "@/i18n/ru/content";

const ru = {
  mdb,
  pageTitles,
  content,
};

export default ru;
