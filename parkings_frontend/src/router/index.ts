import { createRouter, createWebHistory } from 'vue-router'
import i18n from "@/i18n/vue-i18n";
import HomeView from '@/ui/views/HomeView.vue'
import {
  HOME_PATH,
  HOME_ROUTE_NAME,
} from '@/helpers/constants/routes'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: HOME_PATH,
      name: HOME_ROUTE_NAME,
      component: HomeView,
      meta: { titleKey: 'pageTitles.home' },
    }
  ]
})

router.beforeEach((to, from, next) => {
    let titleKey = to.meta.titleKey;
    if(titleKey === undefined) {
      titleKey = 'pageTitles.default';
    }
    document.title = i18n.global.t(String(titleKey));
    next();
})

export default router
