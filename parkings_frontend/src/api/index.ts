// src/api/index.ts is a module that exports all the modules in the api folder.

import api from "./api";
import { authModule } from "./auth";
import { applicationModule } from "./application";

export { api, authModule, applicationModule };
