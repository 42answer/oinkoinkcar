export interface FrontendUserSettings {
  id: string;
  modified_at?: string | null;
  created_at: string;
  show_info_messages: boolean;
  show_warning_messages: boolean;
  show_error_messages: boolean;
  show_debug_messages: boolean;
}
