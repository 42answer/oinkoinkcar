interface AbstractUser {
  username: string;
  email?: string;
  first_name?: string;
  last_name?: string;
  is_staff?: boolean;
  is_active?: boolean;
  is_superuser?: boolean;
  date_joined?: string;
}

export interface User extends AbstractUser {
  id: string;
  locale?: string | null;
  modified_at?: string | null;
  created_at?: string;
  oauth_provider?: string;
  groups?: string[];
}
