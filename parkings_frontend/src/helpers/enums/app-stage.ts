export enum APP_STAGE {
  DEV = "dev",
  PROD = "prod",
  STAGING = "staging",
}
