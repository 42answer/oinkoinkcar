export enum OPERATION_MODE {
  MODE_NORMAL = "normal",
  MODE_MAINTENANCE = "maintenance",
}
