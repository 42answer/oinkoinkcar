// src/helpers/enums/system-message-type.ts
export enum SYSTEM_MESSAGE_TYPE {
  INFO = "info",
  SUCCESS = "success",
  WARNING = "warning",
  ERROR = "danger",
  DEBUG = "debug",
}
