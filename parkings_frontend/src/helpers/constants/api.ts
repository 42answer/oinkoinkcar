export const BASE_URL = window.location.protocol + "//" + window.location.host + "/";
// Api endpoints
export const SITE_STATE_ENDPOINT = `api/v1/site_state/`;

// account system and authentication
export const REFRESH_TOKEN_ENDPOINT = `api/auth/token/`;
export const LOG_OUT = `api/auth/logout/`;
export const USER_GET_PATCH_ENDPOINT = `api/v1/account/me/`;
export const USER_APP_SETTINGS_ENDPOINT = `api/v1/account/settings/app/`;
export const CARPARKS_ENDPOINT_FOR_MAP = `api/v1/car_parks/`;
export const IDEMPOTENCY_KEY_HEADER = "Idempotency-Key";
