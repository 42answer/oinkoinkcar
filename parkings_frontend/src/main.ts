// If user can't access some component, we must reload the page
import {setupInterceptors} from "@/api/api";

window.addEventListener('vite:preloadError', (event) => {
  window.location.reload();
});

import VueCookies from 'vue3-cookies'
import "mdb-vue-ui-kit/css/mdb.min.css";
import "@/ui/assets/styles/global.scss";

import { createPinia } from 'pinia'

import OpenLayersMap, { type Vue3OpenlayersGlobalOptions, } from "vue3-openlayers";
import { createApp } from "vue";

const olOptions: Vue3OpenlayersGlobalOptions = {
  debug: false,
};

const pinia = createPinia();

import App from "./App.vue";

const app = createApp(App);

setupInterceptors();

app.use(VueCookies, { expireTimes: "7d", secure: true, sameSite: "Strict" })

import i18n from "./i18n/vue-i18n";

app.use(i18n)
app.use(OpenLayersMap, olOptions)
app.use(pinia)

import router from "./router";

app.use(router)
app.mount("#app");
