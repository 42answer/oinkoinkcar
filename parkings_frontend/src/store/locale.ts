// src/store/locale.ts
import { defineStore } from 'pinia';
import { useCookies } from 'vue3-cookies';
import { LOCALE_EN, LOCALE_RU } from '@/helpers/constants/locale';
import { availableLanguages } from '@/i18n/vue-i18n';


const { cookies } = useCookies();

export const startLanguage = cookies.get("pin-locale") || LOCALE_RU

export const useLocaleStore = defineStore('locale', {
  state: () => ({
    currentLocale: startLanguage as string,
    availableLocales: [...availableLanguages] as string[],
    localesWithMachineTranslation: [LOCALE_EN] as string[],
    localeToFlagCodeMap: {
      en: 'united-kingdom',
      ru: 'russia',
    } as { [key: string]: string },
    localeToInternationalLanguageMap: {
      en: 'English',
      ru: 'Russian',
    } as { [key: string]: string },
    localeToLocalLanguageMap: {
      en: 'English',
      ru: 'Русский',
    } as { [key: string]: string },
  }),

  getters: {
    isTranslatedByMachine: (state) => (locale: string) =>
      state.localesWithMachineTranslation.includes(locale),
    getFlag: (state) => (locale) => {
      if (state.availableLocales.includes(locale)) {
        return state.localeToFlagCodeMap[locale];
      } else {
        console.debug(`Локаль ${locale} недоступна`);
        return '';
      }
    },
    getInternationalLanguage: (state) => (locale: string) => {
      if (state.availableLocales.includes(locale)) {
        return state.localeToInternationalLanguageMap[locale];
      } else {
        console.debug(`Локаль ${locale} недоступна`);
        return '';
      }
    },
    getLocalLanguage: (state) => (locale: string) => {
      if (state.availableLocales.includes(locale)) {
        return state.localeToLocalLanguageMap[locale];
      } else {
        console.error(`Локаль ${locale} недоступна`);
        return '';
      }
    },
  },

  actions: {
    async setCurrentLocale(locale: string) {
      console.debug(`Попытка установить локаль ${locale}`);
      if (this.availableLocales.includes(locale)) {
        this.currentLocale = locale;
        cookies.set('pin-locale', locale, '1y');
        console.debug(`Локаль установлена на ${locale}`);
      } else {
        console.error(`Локаль ${locale} недоступна`);
      }
    },
  },

});
