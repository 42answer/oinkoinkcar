import { MODERATION_STATUS } from '@/helpers/enums/moderation-status'

export interface BaseModel {
  id?: string;
  modifiedAt?: Date | null;
  createdAt?: Date;
}
