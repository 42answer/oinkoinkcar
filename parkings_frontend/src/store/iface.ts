// src/store/iface.ts
// Module for application-wide interface state.
import { defineStore } from 'pinia';

export const useIfaceStore = defineStore('iface', {
  state: () => ({
    isLocaleDropdownOpen: false as boolean,
  }),

  actions: {
    async toggleLocaleDropdown() {
      this.isLocaleDropdownOpen = !this.isLocaleDropdownOpen;
    },
    async setLocaleDropdown(value: boolean) {
      this.isLocaleDropdownOpen = value;
    }
  },
});
