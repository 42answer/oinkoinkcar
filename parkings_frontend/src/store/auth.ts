// src/store/auth.ts
// Module for authentication and authorization.
import { defineStore } from 'pinia';
import { User } from '@/store/models/user.model';
import { mapApiUserToStoreUser } from '@/helpers/mappers/user.mapper';
import { authModule } from '@/api';
import { useCookies } from 'vue3-cookies';
import { useAppStore } from '@/store/app';

const { cookies } = useCookies();


export const useAuthStore = defineStore('auth', {
  state: () => ({
    authToken: null as string | null,
    expiresAt: null as Date | null,
    refreshToken: null as string | null,
    isAuthenticated: false as boolean,
    user: null as User | null,
    refreshTokenIntervalId: undefined as number | undefined,
    userLoaded: false as boolean,
    isInitialized: false as boolean,
    initializationStarted: false as boolean,
  }),

  actions: {
    async clearAuthState() {
      const appStore = useAppStore();
      await this.stopRefreshTokenInterval();
      await appStore.waitForBackendAvailable();

      try {
        await authModule.logOut();
      } catch (error) {
        console.error(`Ошибка при выходе из системы: ${error}`);
      }

      await this.setIsAuthenticated(false);
      await this.setAuthToken(null);
      await this.setExpiresAt(null);
      await this.setRefreshToken(null);
      await this.checkAndClearCookies();
      console.debug('Состояние авторизации очищено');
    },

    async checkAndClearCookies() {
      for (const key of ['authToken', 'expiresAt', 'refreshToken']) {
        if (cookies.isKey(key)) {
           cookies.remove(key);
        }
      }
    },

    async setAuthToken(authToken: string | null) {
      this.authToken = authToken;
    },

    async setExpiresAt(expiresAt: Date | null) {
      this.expiresAt = expiresAt;
    },

    async setRefreshToken(refreshToken: string | null) {
      this.refreshToken = refreshToken;
    },

    async setIsAuthenticated(isAuthenticated: boolean) {
      this.isAuthenticated = isAuthenticated;
      if(isAuthenticated) {
        console.debug('Пользователь авторизован');
        this.startRefreshTokenInterval();
        await this.loadUser()
      } else {
        await this.setUser(null);
        console.debug('Пользователь вышел из системы');
      }
    },

    async setUser(user: User | null) {
      this.user = user;
      this.userLoaded = user != null;
      console.debug('Пользователь установлен');
    },

    async setIsInitialized() {
      this.isInitialized = true;
      console.debug('Инициализация завершена');
    },

    async waitIsAuthenticationFinished() {
      return new Promise((resolve) => {
        const interval = setInterval(() => {
          if (this.isInitialized) {
            clearInterval(interval);
            resolve(this.isAuthenticated);
          }
        }, 250);
      });
    },

    async notAuthenticated() {
      await this.waitIsAuthenticationFinished();
      return !this.isAuthenticated;
    },

    async notHaveCredentials() {
      return !this.isAuthenticated || !this.authToken
    },

    async initStore() {
      const appStore = useAppStore();
      await appStore.waitForBackendAvailable();
      return new Promise(async (resolve) => {
        try {
          if (this.initializationStarted) {
            return resolve(true);
          }
          this.initializationStarted = true;
          console.debug('Попытка загрузить учетные данные из cookie');
          if (cookies.isKey('authToken') && cookies.isKey('expiresAt') && cookies.isKey('refreshToken')) {
            console.debug('Учетные данные найдены в cookie');
            const authToken = cookies.get('authToken');
            const expiresAt = new Date(cookies.get('expiresAt'));
            const validity = expiresAt.getTime() - new Date().getTime();
            const refreshToken = cookies.get('refreshToken');
            await this.setAuthToken(authToken);
            await this.setExpiresAt(expiresAt);
            await this.setRefreshToken(refreshToken);
            if (validity < 10 * 60 * 1000) {
              console.debug('Токен просрочен');
              await this.tryToRefreshToken();
            } else {
              console.debug('Токен действителен');
              await this.setIsAuthenticated(true);
            }
          }
          await this.setIsInitialized();
          return resolve(true);
        } catch (error) {
          console.error(`Ошибка при инициализации учетных данных: ${error}`, true);
          await this.setIsInitialized();
          return resolve(false);
        }
      });
    },

    async stopRefreshTokenInterval() {
      if (this.refreshTokenIntervalId) {
        clearInterval(this.refreshTokenIntervalId);
        this.refreshTokenIntervalId = undefined;
        console.debug('Обновление токена остановлено');
      }
    },

    async startRefreshTokenInterval() {
      const self = this;
      if (self.expiresAt == null) {
        return;
      }
      self.stopRefreshTokenInterval().then(() => {
        let timeout = self.expiresAt.getTime() - new Date().getTime();
        timeout -= 10 * 60 * 1000; // -10 minutes
        timeout = Math.max(timeout, 60 * 1000);
        self.refreshTokenIntervalId = setInterval(() => {
          self.tryToRefreshToken();
        }, timeout);
        console.debug('Обновление токена запущено');
      });
    },

    async tryToRefreshToken() {
      const appStore = useAppStore();
      await appStore.waitForBackendAvailable();
      try {
        console.debug('Попытка обновить токен');
        if (!this.refreshToken) return false;
        const response = await authModule.refreshToken(this.refreshToken);
        if (response.data?.access_token && response.data?.expires_in && response.data?.refresh_token) {
          console.debug('Учетные данные получены от сервера');
          const expiresAt = new Date();
          expiresAt.setSeconds(expiresAt.getSeconds() + response.data.expires_in);
          await this.setAuthToken(response.data.access_token);
          await this.setExpiresAt(expiresAt);
          await this.setRefreshToken(response.data.refresh_token);
          await this.setIsAuthenticated(true);
          console.debug('Токен обновлен');
          return true;
        }
        console.error(`Ошибка при обновлении токена ${response?.status} ${response.data}`);
        return false;
      } catch (error) {
        console.error(`Ошибка при обновлении токена: ${error}`);
        if (error.response?.status && [401, 400, 403].includes(error.response?.status)) {
          console.log('Ошибка при обновлении токена: ', error);
        }
        return false;
      }
    },

    async loadUser() {
      const appStore = useAppStore();
      await appStore.waitForBackendAvailable();
      if (await this.notHaveCredentials()) return;
      console.debug('Попытка загрузить пользователя');
      try {
        const response = await authModule.loadUser();
        if (response.data) {
          const user = mapApiUserToStoreUser(response.data);
          await this.setUser(user);
          console.debug('Пользователь загружен');
          appStore.loadFrontendUserSettings();
        } else {
          console.debug('Ошибка при загрузке пользователя, данные не получены');
        }
      } catch (error) {
        console.error(`Ошибка при загрузке пользователя: ${error}`);
        if(error.response?.status >= 500 || error.response?.status === 408) {
          setTimeout(() => this.loadUser(), 3000);
          console.debug('Повторная попытка загрузить пользователя через 3 секунды');
        }
      }
    },

    async logoutUser() {
      const appStore = useAppStore();
      await appStore.waitForBackendAvailable();
      console.debug('Попытка выхода из системы');
      await this.clearAuthState();
      await appStore.resetFrontendUserSettings();
      console.debug('Выход из системы завершен');
    }
  },
});