// src/store/app.ts
// Module for application-wide state (excluding authentication, authorization and interface).
import { defineStore } from 'pinia';
import type { SiteState } from '@/store/models/site-state.model'
import { OPERATION_MODE } from "@/helpers/enums/operation-mode";
import { ANONYMOUS_USER_FRONTEND_SETTINGS_ID } from '@/helpers/constants/application'
import { APP_STAGE } from "@/helpers/enums/app-stage";
import type { FrontendUserSettings } from '@/store/models/frontend-user-settings.model';
import { mapApiFrontendUserSettingsToStoreFrontendUserSettings } from '@/helpers/mappers/frontend-user-settings.mapper';
import { mapApiSiteStateToStoreSiteState } from '@/helpers/mappers/site-state.mapper';
import { applicationModule } from '@/api';
import { useCookies } from 'vue3-cookies';
import { useAuthStore } from '@/store/auth'


// @ts-ignore
import { registerSW } from 'virtual:pwa-register'
import {useMapStore} from "@/store/map";

const { cookies } = useCookies();

const basicFrontendUserSettings: FrontendUserSettings = {
  id: ANONYMOUS_USER_FRONTEND_SETTINGS_ID,
  modifiedAt: null,
  createdAt: new Date(),
  showInfoMessages: true,
  showWarningMessages: true,
  showErrorMessages: true,
  showDebugMessages: false,
};

const basicSiteState: SiteState = {
  publicMode: OPERATION_MODE.MODE_NORMAL,
  messageForPublic: null,
  moderationMode: OPERATION_MODE.MODE_NORMAL,
  messageForModerators: null,
  stage: APP_STAGE.PROD,
  frontendVer: '',
  tileLayerUrl: '',
  frontendVerBuild: ''
}

export const useAppStore = defineStore('app', {
  state: () => ({
    frontendUserSettings: basicFrontendUserSettings,
    siteState: basicSiteState as SiteState,
    backendAvailable: false,
    siteStateIntervalId: null as number | null,
  }),

  actions: {
    async setBackendAvailable(value: boolean) {
      this.backendAvailable = value;
    },

    async setFrontendUserSettings(settings: FrontendUserSettings) {
      this.frontendUserSettings = settings;
    },

    async loadFrontendUserSettings() {
      await this.waitForBackendAvailable();
      const authStore = useAuthStore();
      if(await authStore.notAuthenticated()) {
        await this.setFrontendUserSettings(basicFrontendUserSettings);
        return;
      }

      try {
        const response = await applicationModule.getFrontendUserSettings();
        const frontendUserSettings = mapApiFrontendUserSettingsToStoreFrontendUserSettings(response.data);
        await this.setFrontendUserSettings(frontendUserSettings);
      } catch (error) {
        if(error.response?.status >= 500 || error.response?.status == 408) {
          console.error(`Ошибка загрузки настроек пользователя: ${error}`);
          setTimeout(() => {
            this.loadFrontendUserSettings();
          }, 3000);
        } else {
          console.error(`Ошибка загрузки настроек пользователя: ${error}`);
        }
      }
    },

    async resetFrontendUserSettings() {
      await this.setFrontendUserSettings(basicFrontendUserSettings);
    },


    async updateSiteState() {
      try {
        const response = await applicationModule.getSiteState();
        this.siteState = mapApiSiteStateToStoreSiteState(response.data);
        const mapStore = useMapStore();
        if(this.siteState.tileLayerUrl && mapStore.tileLayerUrl != this.siteState.tileLayerUrl) {
          await mapStore.setTileLayerUrl(this.siteState.tileLayerUrl);
        }
        const currentVersion = cookies.get('oink-front-version');
        if (currentVersion !== this.siteState.frontendVer && this.siteState.frontendVer !== this.siteState.frontendVerBuild) {
          cookies.set('oink-front-version', this.siteState.frontendVer);
          if(currentVersion) {
            navigator.serviceWorker.controller.postMessage({ type: 'SKIP_WAITING' });
            const reloadCallback = registerSW({
              onRegistered(r) {
                r && r.update()
              },
              immediate: true
            })
            setTimeout(() => {
              reloadCallback(true).then(() => window.location.reload())
            }, 5000);
          }
        }
        if (!this.backendAvailable) {
          this.setBackendAvailable(true);
        }
      } catch (error) {
        console.error(`Ошибка загрузки состояния сайта: ${error}`);
        if (this.backendAvailable) {
          this.setBackendAvailable(false);
        }
      } finally {
        setTimeout(() => {
          this.updateSiteState();
        }, 10000);
      }
    },

    async waitForSiteLoaded() {
      return new Promise((resolve) => {
        const intervalId = setInterval(() => {
          //@ts-ignore
          if (site_loaded_global) { // global variable from index.html
            clearInterval(intervalId);
            this.setBackendAvailable(true);
            resolve(true);
          }
        }, 100);
      });
    },

    async waitForBackendAvailable() {
      return new Promise((resolve) => {
        const intervalId = setInterval(() => {
          if (this.backendAvailable) {
            clearInterval(intervalId);
            resolve(true);
          }
        }, 250);
      });
    },
  },
});
