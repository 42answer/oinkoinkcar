// src/store/map.ts
// Module for map state.
import { defineStore } from 'pinia'
import { convertCoordinates } from '@/helpers/functions'
import { CARPARKS_ENDPOINT_FOR_MAP } from '@/helpers/constants/api'

export const useMapStore = defineStore('map', {
  state: () => ({
    loadingInProcess: false as boolean,
    carParksUrl: CARPARKS_ENDPOINT_FOR_MAP,
    mapZoom: 12 as number,
    // TODO: get from user location
    mapCenter: convertCoordinates(47.791846, 52.005431) as [number, number], // Use coordinates in EPSG:3857 format or convertCoordinates() function to convert from EPSG:4326 (lat, lng) format
    mapExtent4326: [0, 0, 180, 180] as [number, number, number, number],
    tileLayerUrl: "https://tile3.maps.2gis.com/tiles?x={x}&y={y}&z={z}&v=1",
  }),

  actions: {
    async setTileLayerUrl(url: string) {
        this.tileLayerUrl = url;
    },
    async resetState() {
      this.loadingInProcess = false;
      const carParksUrl = this.carParksUrl
      this.carParksUrl = carParksUrl+`&reset_beacon=${Math.random()}`;
      setTimeout(() => {
        this.carParksUrl = carParksUrl;
      }, 250);
    },
    async updateUrl() {
      let carParksUrl = CARPARKS_ENDPOINT_FOR_MAP
      let carParksUrlDelimiter = '?';
      if (this.mapExtent4326) {
        carParksUrl += `${carParksUrlDelimiter}in_bbox=${this.mapExtent4326.slice(0, 2).join(',')},${this.mapExtent4326.slice(2).join(',')}`;
        carParksUrlDelimiter = '&';
      }
      this.carParksUrl = `${carParksUrl}&beacon=${Math.random()}`
    },
    async loadingStarted() {
        this.loadingInProcess = true;
    },
    async loadingEnded() {
        this.loadingInProcess = false;
    },
    async setMapZoom(value: number) {
      this.mapZoom = value;
      await this.updateUrl();
    },
    async setMapCenter(value: [number, number]) {
      this.mapCenter = value;
      await this.updateUrl();
    },
    async setMapExtent4326(value: [number, number, number, number]) {
      this.mapExtent4326 = value;
      await this.updateUrl();
    },
  },
});
