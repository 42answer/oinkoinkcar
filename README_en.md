<img src="frontend/public/ooc_logo_horisontal.png" alt="Лого"/>

# Hello world, this is OinkOinkCar!
**Normal guys say "Bla-Bla" and don’t park their cars on lawns!** - this is the motto of the project.

## About the Project
OinkOinkCar is a system aimed at combating violations by motorists related to parking or stopping in unauthorized places. 
OinkOinkCar allows you to record the fact of committing an administrative offense in one click. 
Moreover, nothing else is required from the user: neural networks themselves recognize the car number, and the geocoder determines the exact location. 
Then moderators verify all materials, and the permissions management system automatically determines the authorized body, generates an application in the required format, and sends it to this body for consideration.

## Licensing
The project is distributed under the [MIT license](LICENSE). The project depends on other products distributed under their own licenses. 
For example, the Nomeroff package is distributed under the GPL-3.0 license. There is also a current dependency on non-free software, see more in the [notification](PROPRIETARY_SOFTWARE_DEPENDENCY_NOTICE_en.MD).

The guide will be ready shortly. Questions can be asked in the [Issues](https://gitlab.com/theansweris42/oinkoinkcar/-/issues).

## Project Structure
...

## Getting Started
...

## Building the Project
...

## Deployment in Production
...

## Contacts
### Project Authors:

- [Igor Lepekhov](mailto:i@xxkap.app): Backend development, testing.
- [Stanislav Lepekhov](mailto:admin@xxkap.app): Project idea, architecture and business process development, backend, frontend development, CI/CD, code review, deployment, support.
