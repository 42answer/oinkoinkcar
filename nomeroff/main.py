#!/bin/python
from flask import Flask, jsonify, request
from wtforms import Form, validators, StringField
from app import read_number_plates

app = Flask(__name__)


@app.route('/status')
def status():
    return jsonify({'success': True, 'result': 'OK'})


class ReadForm(Form):
    path = StringField('Path to image', [])


@app.route('/read')
def read():
    form = ReadForm(request.args)

    if form.validate():
        path = form.path.data
        result = read_number_plates(path)

        return jsonify({
            'success': True,
            'result': result,
        })

    return jsonify({
        'success': False,
        'errors': form.errors
    })
