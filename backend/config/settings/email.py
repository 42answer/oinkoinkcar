import os
import logging

from .base import COLLECT_STATIC

logger = logging.getLogger(__name__)

EMAIL_HOST = os.environ.get("EMAIL_HOST")
EMAIL_PORT = int(os.environ.get("EMAIL_PORT", 465))
EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD")

if not all([EMAIL_HOST, EMAIL_PORT, EMAIL_HOST_USER, EMAIL_HOST_PASSWORD]) and not COLLECT_STATIC:
    logger.error("Email configuration is missing")
    raise ValueError("Email configuration is missing")

