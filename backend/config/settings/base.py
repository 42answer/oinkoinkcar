import os
import logging
import sys
from pathlib import Path
from django.conf.global_settings import LANGUAGES

logger = logging.getLogger(__name__)

DEBUG = int(os.environ.get("DEBUG", default=0))

STAGE = os.environ.get("STAGE", "").lower()
COLLECT_STATIC = 'collectstatic' in sys.argv
PRODUCTION = STAGE == "prod"
STAGING = STAGE == "staging"
DEV = STAGE == "dev"

FAKE_EMAIL_AUTHORITY_1 = os.environ.get("FAKE_EMAIL_AUTHORITY_1")
FAKE_EMAIL_AUTHORITY_2 = os.environ.get("FAKE_EMAIL_AUTHORITY_2")
if not PRODUCTION and not COLLECT_STATIC and (not FAKE_EMAIL_AUTHORITY_1 or not FAKE_EMAIL_AUTHORITY_2):
    logger.error('FAKE_EMAIL_AUTHORITY_1 and FAKE_EMAIL_AUTHORITY_2 must be set for non-production environment!')
    raise Exception("FAKE_EMAIL_AUTHORITY_1 and FAKE_EMAIL_AUTHORITY_2 must be set for non-production environment!")

BASE_DIR = Path(__file__).resolve().parent.parent.parent
LOCALE_PATHS = os.path.join(BASE_DIR, "locale"),

LANGUAGES = [language_info for language_info in LANGUAGES if language_info[0] in ('ru', 'en')]
SITE_ID = 1

SECRET_KEY = os.environ.get("SECRET_KEY")
if not SECRET_KEY and not COLLECT_STATIC:
    logger.error('SECRET_KEY must be set! Generate a random string and set it as env var.')
    raise Exception("SECRET_KEY must be set! Generate a random string and set it as env var.")
GOOGLE_MAPS_API_KEY = os.environ.get("GOOGLE_MAPS_API_KEY")
if not GOOGLE_MAPS_API_KEY and not COLLECT_STATIC:
    logger.warning('GOOGLE_MAPS_API_KEY not set! Pano views will not be available in the moderator console.')

WSGI_APPLICATION = 'config.wsgi.application'

USE_I18N = True
USE_L10N = True
LANGUAGE_CODE = 'en-us'

USE_TZ = True
TIME_ZONE = 'Europe/Saratov'
CELERY_TIMEZONE = 'Europe/Saratov'

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

FRONTEND_VER = os.environ.get("FRONTEND_VER", default="unknown")
