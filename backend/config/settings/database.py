import os
import logging

from .base import COLLECT_STATIC

logger = logging.getLogger(__name__)

if not all([os.environ.get("POSTGRES_DBNAME"), os.environ.get("POSTGRES_USER"), os.environ.get("POSTGRES_PASS"), os.environ.get("POSTGRES_HOST"), os.environ.get("POSTGRES_PORT")]) and not COLLECT_STATIC:
    logger.error("Postgres environment variables not set, please set them in the env file")
    raise Exception("Postgres environment variables not set")

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": os.environ.get("POSTGRES_DBNAME"),
        "USER": os.environ.get("POSTGRES_USER"),
        "PASSWORD": os.environ.get("POSTGRES_PASS"),
        "HOST": os.environ.get("POSTGRES_HOST"),
        "PORT": os.environ.get("POSTGRES_PORT"),
    }
}

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
