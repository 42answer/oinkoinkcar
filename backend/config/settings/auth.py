import os
import logging
from .base import PRODUCTION, DEV, COLLECT_STATIC

logger = logging.getLogger(__name__)

DJANGO_SUPERUSER_EMAIL = os.getenv('DJANGO_SUPERUSER_EMAIL')
if not DJANGO_SUPERUSER_EMAIL and not COLLECT_STATIC:
    logger.error('DJANGO_SUPERUSER_EMAIL must be set! Set it as env var. This email will be used for geocoding requests.')
    raise Exception("DJANGO_SUPERUSER_EMAIL must be set! Set it as env var.")

AUTH_USER_MODEL = "core.User"
SOCIAL_AUTH_USER_MODEL = "core.User"
AUTH_PASSWORD_VALIDATORS = []

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = os.getenv('GOOGLE_CLIENT_ID')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = os.getenv('GOOGLE_CLIENT_SECRET')
if not SOCIAL_AUTH_GOOGLE_OAUTH2_KEY or not SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET:
    logger.warning('GOOGLE_CLIENT_ID and GOOGLE_CLIENT_SECRET must be set for social auth! Set them as env vars.')

SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.googleapis.com/auth/userinfo.email',
]
SOCIAL_AUTH_VK_OAUTH2_KEY = os.getenv('SOCIAL_AUTH_VK_OAUTH2_KEY')
SOCIAL_AUTH_VK_OAUTH2_SECRET = os.getenv('SOCIAL_AUTH_VK_OAUTH2_SECRET')
if not SOCIAL_AUTH_VK_OAUTH2_KEY or not SOCIAL_AUTH_VK_OAUTH2_SECRET:
    logger.warning('SOCIAL_AUTH_VK_OAUTH2_KEY and SOCIAL_AUTH_VK_OAUTH2_SECRET must be set for social auth! Set them as env vars.')
SOCIAL_AUTH_VK_OAUTH2_SCOPE = [
    'email',
]

OAUTH_CLIENT_ID_KEY = os.getenv('OAUTH_CLIENT_ID_KEY')
OAUTH_CLIENT_SECRET_KEY = os.getenv('OAUTH_CLIENT_SECRET_KEY')
if (not OAUTH_CLIENT_ID_KEY or not OAUTH_CLIENT_SECRET_KEY) and not COLLECT_STATIC:
    logger.error('OAUTH_CLIENT_ID_KEY and OAUTH_CLIENT_SECRET_KEY must be set! Generate random strings and set them as env vars.')
    raise Exception("OAUTH_CLIENT_ID_KEY and OAUTH_CLIENT_SECRET_KEY must be set! Generate random strings and set them as env vars. OAUTH_CLIENT_ID_KEY - 40 symbols, OAUTH_CLIENT_SECRET_KEY - 128 symbols (A-Za-z0-9).")

# Pipelines must be set in the correct order
SOCIAL_AUTH_PIPELINE = [
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.social_auth.associate_by_email',
]

if PRODUCTION or DEV:
    SOCIAL_AUTH_PIPELINE += ['social_core.pipeline.user.create_user', ]

SOCIAL_AUTH_PIPELINE += [
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.user.user_details',
    'social_core.pipeline.social_auth.load_extra_data',
]

AUTHENTICATION_BACKENDS = [
    'social_core.backends.google.GoogleOAuth2',
    'social_core.backends.vk.VKOAuth2',
    'drf_social_oauth2.backends.DjangoOAuth2',
    'django.contrib.auth.backends.ModelBackend',
]

SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/api/auth/success/'
SOCIAL_AUTH_LOGIN_ERROR_URL = '/error-auth/login-error/'
SOCIAL_AUTH_LOGIN_URL = '/login/'
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/api/auth/success/'
SOCIAL_AUTH_NEW_ASSOCIATION_REDIRECT_URL = '/api/auth/success/'
SOCIAL_AUTH_INACTIVE_USER_URL = '/error-auth/inactive-user/'
SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True
SOCIAL_AUTH_REDIRECT_IS_HTTPS = True
SOCIAL_AUTH_JSONFIELD_ENABLED = True
SOCIAL_AUTH_URL_NAMESPACE = 'social'
SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ['email',]
REDIRECT_AFTER_LOGIN = '/'
REDIRECT_AFTER_LOGOUT = '/'
AUTH_PROVIDERS = ['google-oauth2', 'vk-oauth2']

ACTIVATE_JWT = True

SESSION_COOKIE_SECURE = True
