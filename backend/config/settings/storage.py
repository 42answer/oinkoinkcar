import os
import sys
import logging

from .base import COLLECT_STATIC

logger = logging.getLogger(__name__)

STATIC_URL = 'static/'
STATIC_ROOT = "/static"
STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'

AWS_REGION = "ru-moscow"
DEFAULT_FILE_STORAGE = 'django_s3_storage.storage.S3Storage'
AWS_ACCESS_KEY_ID = os.environ.get("S3_ACCESS_KEY")
AWS_SECRET_ACCESS_KEY = os.environ.get("S3_SECRET_KEY")
AWS_STORAGE_BUCKET_NAME = os.environ.get("S3_BUCKET")
AWS_S3_ENDPOINT_URL = f"https://{os.environ.get('S3_ENDPOINT')}"
AWS_S3_ACCESS_KEY_ID = os.environ.get("S3_ACCESS_KEY")
AWS_S3_SECRET_ACCESS_KEY = os.environ.get("S3_SECRET_KEY")
AWS_S3_BUCKET_AUTH = True
AWS_S3_MAX_AGE_SECONDS = 604800
AWS_S3_ADDRESSING_STYLE = "virtual" if "sbercloud.ru" in AWS_S3_ENDPOINT_URL else "path"
AWS_S3_BUCKET_AUTH_STATIC = True
AWS_S3_BUCKET_NAME = os.environ.get("S3_TEST_BUCKET_NAME") if 'test' in sys.argv else os.environ.get("S3_BUCKET")
AWS_S3_PRIVATE_BUCKET_NAME = os.environ.get("S3_TEST_BUCKET_NAME") if 'test' in sys.argv else os.environ.get("S3_PRIVATE_BUCKET")
if COLLECT_STATIC:
    AWS_S3_BUCKET_NAME = "test"
    AWS_S3_PRIVATE_BUCKET_NAME = "test-priv"
AWS_S3_MAX_AGE_SECONDS_CACHED_STATIC = 604800

if not all([AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_STORAGE_BUCKET_NAME, AWS_S3_ENDPOINT_URL]) and not COLLECT_STATIC:
    logger.error("S3 configuration is missing")
    raise ValueError("S3 configuration is missing")
