import os
from .base import DEBUG, PRODUCTION

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'DEBUG' if DEBUG else 'INFO',
            'class': 'logging.StreamHandler',
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/logs/django.log',
            'level': 'INFO',
            'formatter': 'verbose',
            'maxBytes': 10 * 1024 * 1024,
            'backupCount': 5,
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console', 'file'] if PRODUCTION else [],
            'level': 'DEBUG' if DEBUG else 'INFO',
        },
        'core': {
            'handlers': ['console', 'file'] if PRODUCTION else ['console'],
            'level': 'DEBUG' if DEBUG else 'INFO',
        },
        'api': {
            'handlers': ['console', 'file'] if PRODUCTION else ['console'],
            'level': 'DEBUG' if DEBUG else 'INFO',
        },
        'django.request': {
            'handlers': ['console', 'file'] if PRODUCTION else [],
            'level': 'DEBUG' if DEBUG else 'ERROR',
        },
        'celery': {
            'handlers': ['console', 'file'] if PRODUCTION else [],
            'level': 'DEBUG' if DEBUG else 'ERROR',
        },
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
}

if int(os.getenv('DB_SHOW_DEBUG', default=0)):
    LOGGING['loggers']['django.db.backends'] = {
        'level': 'DEBUG',
        'handlers': ['console'],
    }
