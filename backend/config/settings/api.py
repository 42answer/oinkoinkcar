from .base import DEV
import os
import re
import logging
from .base import COLLECT_STATIC

logger = logging.getLogger(__name__)

ROOT_URLCONF = 'config.urls'

APPEND_SLASH = False
DATA_UPLOAD_MAX_MEMORY_SIZE = 5242880 * 2  # 10MB

APP_DOMAIN = os.environ.get("APP_DOMAIN")
if not APP_DOMAIN and not COLLECT_STATIC:
    raise ValueError("APP_DOMAIN environment variable is not set")
LOGIN_DOMAIN = os.environ.get("LOGIN_DOMAIN", None)
if not LOGIN_DOMAIN:
    logger.warning("LOGIN_DOMAIN environment variable is not set. Authorization with PWA will not work.")

ALLOWED_HOSTS = os.environ.get("DJANGO_ALLOWED_HOSTS", "").split(" ")
if not ALLOWED_HOSTS and not COLLECT_STATIC:
    raise ValueError("DJANGO_ALLOWED_HOSTS environment variable is not set")

CONVERT_TOKEN_ENDPOINT = 'http://backend:8000/auth/convert-token'
REFRESH_TOKEN_ENDPOINT = 'http://backend:8000/auth/token'
REVOCATION_ENDPOINT = 'http://backend:8000/auth/revoke-token'

CORS_ALLOWED_ORIGINS = []
for h in ALLOWED_HOSTS:
    # check that it is a valid domain and not IP address
    if not re.match(r'^\d+\.\d+\.\d+\.\d+$', h):
        CORS_ALLOWED_ORIGINS.append(f'https://{h}')
    elif re.match(r'^\d+\.\d+\.\d+\.\d+$', h):
        CORS_ALLOWED_ORIGINS.append(f'http://{h}')
    else:
        logger.info(f"Host {h} is not a valid domain name. Skipping CORS setup for it.")

# For OAuth2 flow and balancer healthcheck
ALLOWED_HOSTS += ["backend", ]

COOKIE_EXPIRES_DAYS = 30
CSRF_TRUSTED_ORIGINS = []

if int(os.getenv('USE_SSL', 0)):
    CSRF_COOKIE_SECURE = True
    HTTP_PROTOCOL = 'https'
    for h in ALLOWED_HOSTS:
        CSRF_TRUSTED_ORIGINS.append(f'https://{h}')
else:
    HTTP_PROTOCOL = 'http'

USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

NOMEROFF_URL = os.environ.get("NOMEROFF_URL", "http://localhost:3116/read")

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.DjangoModelPermissions',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.SessionAuthentication',
        'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
        'drf_social_oauth2.authentication.SocialAuthentication',
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 50,
    'HTML_SELECT_CUTOFF': 5,
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
}

if not DEV:
    REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = (
        'rest_framework.renderers.JSONRenderer',
    )

if DEV:
    REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] += [
        'rest_framework.authentication.BasicAuthentication',
    ]

IDEMPOTENCY_KEY_LOCK_TIMEOUT_SECONDS = 60 * 5
IDEMPOTENCY_KEY_LIFETIME_HOURS = 24
