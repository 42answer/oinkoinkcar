from django.contrib import admin
from django.urls import path, include, re_path
from django.contrib.sitemaps.views import sitemap

from .sitemap import ViolationViewSitemap, StaticViewSitemap


urlpatterns = [
	path('admin/', admin.site.urls),
	path('api/', include('api.urls')),
	re_path(r'^auth/', include('drf_social_oauth2.urls', namespace='drf')),
	path('', include('social_django.urls', namespace='social')),

	path('sitemap.xml', sitemap, {'sitemaps': {'static': StaticViewSitemap, 'violation': ViolationViewSitemap}})
	]
