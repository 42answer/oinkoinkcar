from django_s3_storage.storage import S3Storage as DJS3Storage
from config import settings

S3Storage = DJS3Storage(aws_s3_bucket_name=settings.AWS_S3_BUCKET_NAME)
S3PrivateStorage = DJS3Storage(aws_s3_bucket_name=settings.AWS_S3_PRIVATE_BUCKET_NAME)
