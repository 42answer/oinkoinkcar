from django.contrib import sitemaps

from core.models import Violation


class ViolationViewSitemap(sitemaps.Sitemap):
	priority = 0.7
	changefreq = "never"

	def items(self):
		return Violation.objects.filter(is_published=True, car_plate__number_confirmed=True, car_plate__photo__address_confirmed=True)

	def lastmod(self, obj):
		return obj.modified_at

	def location(self, obj):
		return f"/?violation={obj.id}"
