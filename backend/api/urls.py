from django.urls import path

from .views import *


urlpatterns = [
	path("v1/site_state/", SiteStateAPIView.as_view(), name="site_state"),

	path("auth/success/", success),
	path("auth/token/", refresh_token),
	path("auth/logout/", logout),
	path("auth/login/", login),

	path("v1/account/me/", DetailsUserInfoView.as_view()),
	path("v1/account/settings/app/", DetailsFrontendUserSettingsView.as_view()),
	path("v1/account/stats/violations/", UserViolationStats.as_view()),

	path("v1/applicants/", ListedApplicantViewForStaff.as_view(), name="applicants_list"),

	path("v1/applications/", ListedApplicationViewForStaff.as_view(), name="applications_list"),
	path("v1/applications/<uuid:uuid>/",DetailsApplicationViewForStaff.as_view(), name="application"),

	path("v1/geo_photos/", ListedGeoPhotoView.as_view(), name="geo_photos_list"),
	path("v1/geo_photos/<uuid:uuid>/", DetailsGeoPhotoView.as_view(), name="geo_photo"),

	path("v1/violations/", ListedViolationViewForMap.as_view(), name="violations_list_for_map"),
	path("v1/violations_for_sitemap/", ListedViolationViewForSiteMap.as_view(), name="violations_list_for_sitemap"),
	path("v1/violations/<uuid:uuid>/", DetailsViolationView.as_view(), name="violation"),

	path("v1/car_plates/", ListedCarPlateView.as_view(), name="car_plates_list"),
	path("v1/car_plates/<uuid:uuid>/", DetailsCarPlateView.as_view(), name="car_plate"),

	path("v1/types_of_violation/", ListedTypeOfViolationView.as_view(), name="types_of_violation_list"),

	path("v1/car_parks/", ListedCarParkView.as_view(), name="car_park_list"),
	path("v1/car_parks/<uuid:uuid>/", DetailsCarParkView.as_view(), name="car_park"),

	path("v1/moderation/geo_photos/<uuid:uuid>/", DetailsGeoPhotoViewForStaff.as_view(), name="geo_photo_for_staff"),
	path("v1/moderation/car_plates/<uuid:uuid>/", DetailsCarPlateViewForStaff.as_view(), name="car_plate_for_staff"),
	path("v1/moderation/violations/", ListedViolationViewForStaff.as_view(), name="violations_list_for_staff"),
	path("v1/moderation/violations/<uuid:uuid>/", DetailsViolationViewForStaff.as_view(), name="violation_for_staff"),

	path("v1/healthcheck/email_sending_health_check/", email_sending_health_check, name="email_sending_health_check"),
	path("v1/healthcheck/moderation_timeout_health_check/", moderation_timeout_health_check, name="moderation_timeout_health_check"),
	path("v1/healthcheck/user_quality_health_check/", user_quality_health_check, name="user_quality_health_check"),
	path("v1/healthcheck/moderator_quality_health_check/", moderator_quality_health_check, name="moderator_quality_health_check"),
	path("v1/healthcheck/disk_space_health_check/", disk_space_health_check, name="disk_space_health_check"),
	path("v1/healthcheck/memory_usage_health_check/", memory_usage_health_check, name="memory_usage_health_check"),
	path("v1/healthcheck/cpu_load_health_check/", cpu_load_health_check, name="cpu_load_health_check"),
	path("v1/healthcheck/basic_health_check/", basic_health_check, name="basic_health_check"),
]
