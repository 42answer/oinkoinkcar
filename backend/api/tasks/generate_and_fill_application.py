import datetime
import io
from config import settings
from django.template import Template

from config.celery import app as celery_app
from core.models import Application, Violation, Powers
from weasyprint import HTML
from django.template.loader import get_template


@celery_app.task
def generate_and_fill_application(application_id: str):
    application = Application.objects.get(id=application_id)
    violation: Violation = application.violation
    power: Powers = application.powers
    # Fixme: Get timezone/utc_offset from places
    utc_offset = datetime.timedelta(hours=4)
    local_dt = violation.car_plate.photo.captured_at + utc_offset
    date = local_dt.strftime("%d.%m.%Y")
    time = local_dt.strftime("%H:%M")
    y, x = violation.car_plate.photo.geom.coords
    applicant_email = f"{application.applicant.user.username}@{settings.EMAIL_HOST}"
    x = round(x, 6)
    y = round(y, 6)
    application_text = (f"{power.liability.request} {violation.car_plate.plate_number}, который {date} в {time} {power.rule.statement}, "
                        f"адресный ориентир: {violation.car_plate.photo.address}, координаты: {x, y}. \nК настоящему заявлению прикладываю материалы фотофиксации правонарушения.")
    if violation.car_plate.photo.panorama_shot.name:
        application_text += f" Помимо фотографии нарушения, к заявлению прилагается панорамный снимок места совершения правонарушения, для уточнения местоположения и иных деталей, имеющих значение при принятии решения о привлечении к ответственности."

    application.text = application_text
    application_date = application.created_at + utc_offset
    application_date = application_date.strftime("%d.%m.%Y")
    applicant_name = f"{application.applicant.first_name[0]}. {application.applicant.patronymic[0]}. {application.applicant.last_name}"
    context = {
                'application': application,
                'application_date': application_date,
                'application_text': application_text,
                'applicant_email': applicant_email,
                'applicant_name': applicant_name,
                'captured_time': time,
                'captured_date': date,
                'coords_x': x,
                'coords_y': y,
               }
    if not power.template:
        template = get_template('application.html')
        html_str = template.render(context)
        result = io.BytesIO()
    else:
        template = Template(power.template.template)
        html_str = template.render(context)
        result = io.BytesIO()
    HTML(string=html_str).write_pdf(result)
    application.pdf_file.save(f'{str(application_date)}_{str(application.violation.car_plate.plate_number)}_{str(application.violation.violation_type.name)}.pdf', result)
    application.save(update_fields=['text', 'pdf_file'])
