import os
import requests
import logging
from config.celery import app as celery_app
from config.settings import NOMEROFF_URL
from django.core.files import File

from datetime import timedelta
from django.utils import timezone
from core.models.geo_photo import GeoPhoto
from core.models.car_plate import CarPlate
from core.models.violation import Violation
from PIL import Image
from io import BytesIO
from config.settings import COMPRESSED_IMG_WIDTH

logger = logging.getLogger(__name__)


def resize_image(image_path):
    with Image.open(image_path) as img:
        width, height = img.size

        scale = COMPRESSED_IMG_WIDTH / min(width, height)

        new_width = int(width * scale)
        new_height = int(height * scale)
        img = img.resize((new_width, new_height), Image.LANCZOS)

        left = (new_width - COMPRESSED_IMG_WIDTH) / 2
        top = (new_height - COMPRESSED_IMG_WIDTH) / 2
        right = (new_width + COMPRESSED_IMG_WIDTH) / 2
        bottom = (new_height + COMPRESSED_IMG_WIDTH) / 2

        img = img.crop((left, top, right, bottom))

        return img, new_width, new_height


def convert_coordinates_to_relative(b_box, img_width, img_height):
    print(b_box, img_width, img_height)
    img_width_relative = 1.0 / img_width
    img_height_relative = 1.0 / img_height
    res = [[x * img_width_relative, y * img_height_relative] for x, y in b_box]
    return res


@celery_app.task
def car_plate_recognition(photo_id: str):
    photo = GeoPhoto.objects.get(id=photo_id)
    delta = photo.created_at - photo.captured_at
    if delta.total_seconds() > 60 * 29:
        logger.warning(f'Photo {photo_id} was not recognized because of timeout')
        if not photo.author.is_superuser:
            photo.moderation_status = GeoPhoto.REJECTED_REASON_TIMEOUT
            photo.save(update_fields=["moderation_status"])
            return
    ext = os.path.splitext(photo.src_photo.name)[-1].lower()
    file_path = f'/container_image_exchange/{photo_id}_src_photo{ext}'
    update_fields = []
    img_width: int = None
    img_height: int = None
    try:
        img, img_width, img_height = resize_image(file_path)
        buffer = BytesIO()
        img_format = img.format
        if img_format != 'JPEG':
            img = img.convert('RGB')
            img_format = 'JPEG'
            ext = '.jpg'
        img.save(buffer, format=img_format, quality=70)
        buffer.seek(0)
        photo.compressed_photo = File(buffer, name=f'{photo_id}_compressed_photo{ext}')
        update_fields.append("compressed_photo")
    except Exception as e:
        logger.error(f'Error resize image {e}')

    try:
        response = requests.get(f'{NOMEROFF_URL}?path={file_path}')
        response_data = response.json()
        if img_width is None or img_height is None:
            with Image.open(file_path) as img:
                img_width, img_height = img.size
        plates_count = 0
        if response.status_code == 200 and "result" in response_data:
            for plate_number, b_box in response_data.get("result").items():
                car_plate = CarPlate.objects.create(
                    plate_number=plate_number,
                    b_box=convert_coordinates_to_relative(b_box, img_width, img_height),
                    photo=photo
                )
                plates_count += 1
                if photo.initial_type_of_violation is not None:
                    yesterday = timezone.now() - timedelta(days=1)
                    already_exists = Violation.objects.filter(
                        car_plate__plate_number__iexact=plate_number,
                        car_plate__photo__captured_at__gte=yesterday,
                        violation_type=photo.initial_type_of_violation,
                    ).exists() if plate_number != "Не распознан" else False
                    Violation.objects.create(
                        violation_type=photo.initial_type_of_violation,
                        car_plate=car_plate,
                        moderation_status=Violation.REJECTED_REASON_VIOLATION_ALREADY_EXISTS if already_exists else Violation.NOT_MODERATED
                    )
                    if already_exists:
                        logger.warning(f'Violation with car plate {plate_number} already exists')
                        photo.moderation_status = GeoPhoto.REJECTED_REASON_VIOLATION_ALREADY_EXISTS
                        update_fields.append("moderation_status")
                        car_plate.moderation_status = GeoPhoto.REJECTED_REASON_VIOLATION_ALREADY_EXISTS
                        car_plate.save(update_fields=["moderation_status"])
        else:
            logger.error(f'Error recognition car plates. Nomeroff get response with code {response.status_code}')
        if plates_count == 0:
            car_plate = CarPlate.objects.create(
                plate_number="Не распознан",
                b_box=[[0, 0], [0, 0], [0, 0], [0, 0]],
                photo=photo
            )
            logger.warning(f'Car plate was not recognized')
            if photo.initial_type_of_violation is not None:
                Violation.objects.create(
                    violation_type=photo.initial_type_of_violation,
                    car_plate=car_plate
                )
        update_fields.append("is_recognized")
        photo.is_recognized = True
        if photo.initial_type_of_violation is not None:
            photo.is_violation_selected = True
            update_fields.append("is_violation_selected")

    except Exception as e:
        logger.error(f'Error recognition car plates {e}')

    photo.save(update_fields=update_fields)

    if os.path.exists(file_path):
        os.remove(file_path)
