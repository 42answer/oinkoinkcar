import requests
import logging
from io import BytesIO

from django.core import mail
from django.core.mail.backends.smtp import EmailBackend
from django.utils import timezone

from config.celery import app as celery_app
from config import settings
from core.models import Application

logger = logging.getLogger(__name__)


@celery_app.task
def send_application(application_id: str):
    application: Application = Application.objects.get(id=application_id)
    if application.sent_at:
        logger.warning(f'Application {application.id} has already been sent')
        return
    applicant_email = f"{application.applicant.user.username}@{settings.EMAIL_HOST}"
    recipient = application.powers.authority.email
    pdf_bytes = BytesIO(requests.get(application.pdf_file.url).content)
    file_name = f'{application.violation.car_plate.plate_number}_{application.created_at.strftime("%d.%m.%Y")}_{application.violation.violation_type.name}.pdf'
    application.task_id = send_application.request.id
    application.save(update_fields=['task_id'])

    subject = f'Заявление об адм. правонарушении ({application.violation.violation_type.name}), а/м г/н {application.violation.car_plate.plate_number}, от {application.created_at.strftime("%d.%m.%Y")}.'
    message = f'Добрый день! Направляю заявление от {application.created_at.strftime("%d.%m.%Y")} (во вложении) о привлечении к административной ответственности собственника а/м {application.violation.car_plate.plate_number}. Ответ прошу направить на этот адрес электронной почты в установленный законом срок.\n______________________________\n{application.applicant.last_name} {application.applicant.first_name[0]}. {application.applicant.patronymic[0]}. '
    success = False
    try:
        connection = EmailBackend(
            host=settings.EMAIL_HOST,
            port=settings.EMAIL_PORT,
            username=settings.EMAIL_HOST_USER,
            password=settings.EMAIL_HOST_PASSWORD,
            use_ssl=True,
            fail_silently=False
        )
        connection.open()
        email = mail.EmailMessage(subject, message, applicant_email, [recipient], connection=connection, bcc=[application.applicant.email])
        email.attach(file_name, pdf_bytes.read(), 'application/pdf')
        success = email.send()
        connection.close()
    except Exception as e:
        logger.error(f'Error occurred while sending email: {str(e)}')

    if success:
        application.sent_at = timezone.now()
        application.task_id = None
        application.save(update_fields=['sent_at', 'task_id'])
    else:
        application.failed_attempts += 1
        application.last_failure_attempt_at = timezone.now()
        application.task_id = None
        application.save(update_fields=['failed_attempts', 'last_failure_attempt_at', 'task_id'])
