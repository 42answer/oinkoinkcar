import requests
import logging

from config import settings
from django.contrib.gis.geos import Point
from django.utils.timezone import now
from django.contrib.gis.db.models.functions import Distance
from config.celery import app as celery_app
from core.models import GeoPhoto, CarPark

logger = logging.getLogger(__name__)


@celery_app.task
def photo_geocoding(geo_photo_id: str):
    geo_photo = GeoPhoto.objects.get(id=geo_photo_id)

    lon, lat = geo_photo.geom.coords
    url = f"https://nominatim.openstreetmap.org/reverse?lat={lat}&lon={lon}&format=json&accept-language=ru&email={settings.DJANGO_SUPERUSER_EMAIL}"

    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        address = None
        try:
            address = f"д.{data['address'].get('house_number')}, {data['address'].get('road')}, {data['address'].get('city') or data['address'].get('municipality')}, {data['address'].get('county')}, {data['address'].get('state') or data['address'].get('region')}"
            if "None" in address:
                address = None
        except AttributeError:
            pass
        if address is None:
            address_lst = data['display_name'].split(', ')
            address_lst.reverse()
            address = ', '.join(address_lst)

        geo_photo.address = address
        geo_photo.is_geocoded = True
        geo_photo.modified_at = now()
        updated_fields = ['address', 'is_geocoded', 'modified_at']
        closest_car_park = CarPark.objects.filter(is_active=True).annotate(distance=Distance('geom', Point(geo_photo.geom.x, geo_photo.geom.y, srid=4326))).order_by('distance').first()
        if closest_car_park is not None and closest_car_park.distance.m < 1000:
            geo_photo.nearest_parking_distance = closest_car_park.distance.m
            updated_fields.append('nearest_parking_distance')
        geo_photo.save(update_fields=updated_fields)
        return
    logger.error(f'Error occurred while geocoding photo: {response.status_code} {response.text}')
    return
