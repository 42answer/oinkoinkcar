from datetime import timedelta
import logging
from celery import shared_task
from django.db import transaction
from django.db.models import Q
from django.utils import timezone

from config import settings
from core.models import Application
from .send_application import send_application

logger = logging.getLogger(__name__)


@shared_task
def send_unsent_emails():
    success = False
    try:
        with transaction.atomic():
            unsent_applications = Application.objects.filter(Q(last_failure_attempt_at__isnull=True) | Q(last_failure_attempt_at__lte=timezone.now()-timedelta(minutes=settings.DELAY_BETWEEN_APPLICATION_SENDING_ATTEMPTS_MINUTES)),
                                                             is_confirmed=True,
                                                             sent_at__isnull=True,
                                                             created_at__gte=timezone.now()-timedelta(hours=settings.TIMEDELTA_FOR_APPLICATION_SENDING_HOURS),
                                                             task_id__isnull=True,
                                                             failed_attempts__lte=5,
                                                             failed_attempts__gt=0)

            for a in unsent_applications:
                a.task_id = send_unsent_emails.request.id
                a.save(update_fields=['task_id'])
        transaction.commit()
        success = True
    except Exception as e:
        logger.error(f'Error occurred while committing transaction: {str(e)}')
        transaction.rollback()
    if success:
        for unsent_application in Application.objects.filter(task_id=send_unsent_emails.request.id):
            send_application.delay(unsent_application.id)
