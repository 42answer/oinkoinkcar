from datetime import timedelta
import logging
from celery import shared_task
from django.utils import timezone

from config.settings import IDEMPOTENCY_KEY_LIFETIME_HOURS
from core.models import IdempotencyKey


logger = logging.getLogger(__name__)


@shared_task
def delete_idempotency_keys():
    try:
        IdempotencyKey.objects.filter(created_at__lte=timezone.now()-timedelta(hours=IDEMPOTENCY_KEY_LIFETIME_HOURS)).delete()
    except Exception as e:
        logger.error(f'Error occurred while deleting idempotency keys: {str(e)}')
