from rest_framework import serializers

from core.models import User
from .base import BaseSerializer
from core.models.applicant import Applicant


class ListedApplicantSerializerForStaff(BaseSerializer):
    first_name = serializers.CharField(required=True, max_length=64)
    patronymic = serializers.CharField(required=True, max_length=64)
    last_name = serializers.CharField(required=True, max_length=64)
    registration_address = serializers.CharField(required=True, max_length=1024)
    identity_document = serializers.CharField(required=True, max_length=1024)
    email = serializers.CharField(required=False, allow_null=True)
    phone_number = serializers.CharField(required=False, allow_null=True, max_length=15)
    user = serializers.PrimaryKeyRelatedField(required=True, queryset=User.objects.all())
    sign_scan = serializers.ImageField(required=False, allow_null=False)

    class Meta(BaseSerializer.Meta):
        model = Applicant
        fields = BaseSerializer.Meta.fields + ["first_name", "patronymic", "last_name", "registration_address", "identity_document", "email", "phone_number", "user", "sign_scan"]
        read_only_fields = BaseSerializer.Meta.read_only_fields
        extra_kwargs = {
            "sign_scan": {"write_only": True},
            "registration_address": {"write_only": True},
            "identity_document": {"write_only": True},
        }
