from rest_framework import serializers

from core.models import TypeOfViolation


class TypeOfViolationSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True, max_length=256)
    description = serializers.CharField(required=False, max_length=1024)
    image = serializers.ImageField(required=False, allow_null=True)
    icon = serializers.ImageField(required=False, allow_null=True)

    class Meta:
        model = TypeOfViolation
        field_id = 'id'
        fields = ["id", "name", "description", "image", "icon", "created_at", "modified_at"]


class ListedTypeOfViolationSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True, max_length=256)
    icon = serializers.ImageField(required=False, allow_null=True)

    class Meta:
        model = TypeOfViolation
        field_id = 'id'
        fields = ["id", "name", "icon", "description"]
