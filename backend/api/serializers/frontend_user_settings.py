from django.utils.timezone import now
from rest_framework import serializers
from core.models.frontend_user_settings import FrontendUserSettings


class FrontendUserSettingsSerializer(serializers.ModelSerializer):

    class Meta:
        model = FrontendUserSettings
        id_field = 'id'
        fields = [
            'modified_at',
            'created_at',
            'theme',
            'show_info_messages',
            'show_warning_messages',
            'show_error_messages',
            'show_debug_messages'
        ]
        read_only_fields = ['modified_at', 'created_at']

    def update(self, instance, validated_data):
        validated_data["modified_at"] = now()
        return super().update(instance, validated_data)
