from rest_framework import serializers
from drf_extra_fields.fields import HybridImageField
from django.utils.timezone import now
from config import storages
from core.models import Violation, CarPlate
from .base import BaseSerializer
from core.models.geo_photo import GeoPhoto
from core.models.type_of_violation import TypeOfViolation


class ListedGeoPhotoSerializer(BaseSerializer):

    class Meta(BaseSerializer.Meta):
        model = GeoPhoto
        fields = BaseSerializer.Meta.fields + ['geom', 'compressed_photo', 'address', 'address_confirmed', 'src_photo', 'moderation_status']
        read_only_fields = BaseSerializer.Meta.read_only_fields + ['geom', 'compressed_photo', 'address', 'address_confirmed', 'src_photo', 'moderation_status']


class ListedGeoPhotoSerializerForMap(BaseSerializer):
    captured_at = serializers.DateTimeField(read_only=True, format="%Y-%m-%d")

    class Meta(BaseSerializer.Meta):
        model = GeoPhoto
        fields = BaseSerializer.Meta.fields + ['geom', 'compressed_photo', 'address', 'src_photo', 'moderation_status', 'panorama_shot', 'captured_at']
        read_only_fields = BaseSerializer.Meta.read_only_fields + ['geom', 'compressed_photo', 'address', 'src_photo', 'moderation_status', 'panorama_shot', 'captured_at']


class ListedGeoPhotoSerializerForStaff(ListedGeoPhotoSerializer):

    class Meta(ListedGeoPhotoSerializer.Meta):
        fields = ListedGeoPhotoSerializer.Meta.fields + ["is_recognized", "is_violation_selected", "is_geocoded", "accuracy_horizontal", "compass_angle", "is_rejected", "panorama_shot"]
        read_only_fields = BaseSerializer.Meta.read_only_fields + ['compressed_photo', 'src_photo'] + ["is_recognized", "is_violation_selected", "is_geocoded", "accuracy_horizontal", "compass_angle"]


class DetailsGeoPhotoSerializer(ListedGeoPhotoSerializer):

    class Meta(ListedGeoPhotoSerializer.Meta):
        model = GeoPhoto
        fields = ListedGeoPhotoSerializer.Meta.fields + ['accuracy_horizontal', 'compass_angle', 'is_recognized', 'is_violation_selected', 'is_geocoded', 'panorama_shot', 'moderation_status']
        read_only_fields = ListedGeoPhotoSerializer.Meta.read_only_fields + ['accuracy_horizontal', 'compass_angle', 'is_recognized', 'is_violation_selected', 'is_geocoded', 'panorama_shot', 'moderation_status']


class DetailsGeoPhotoSerializerForStaff(DetailsGeoPhotoSerializer):

    class Meta(DetailsGeoPhotoSerializer.Meta):
        fields = ListedGeoPhotoSerializerForStaff.Meta.fields + []
        read_only_fields = ListedGeoPhotoSerializerForStaff.Meta.read_only_fields + []


class CreateGeoPhotoSerializer(serializers.ModelSerializer):
    src_photo = HybridImageField(required=True)  # We can use ImageField here, but HybridImageField is more flexible
    accuracy_horizontal = serializers.FloatField(required=False, allow_null=True)
    compass_angle = serializers.FloatField(required=False, allow_null=True)
    captured_at = serializers.DateTimeField(required=True)
    initial_type_of_violation = serializers.PrimaryKeyRelatedField(queryset=TypeOfViolation.objects.all(), required=False, allow_null=True)

    class Meta:
        model = GeoPhoto
        fields = ['geom', 'src_photo', 'accuracy_horizontal', 'compass_angle', 'captured_at', 'initial_type_of_violation', 'id', 'created_at', 'modified_at', 'compressed_photo', 'address_confirmed', 'author', 'is_recognized', 'is_violation_selected', 'is_geocoded', 'address', 'address_confirmed']
        read_only_fields = ['id', 'created_at', 'modified_at', 'compressed_photo', 'address_confirmed', 'author', 'is_recognized', 'is_violation_selected', 'is_geocoded', 'address', 'address_confirmed']
        extra_kwargs = {
            'initial_type_of_violation': {'required': False, 'allow_null': True, 'write_only': True},
        }


class UpdateGeoPhotoSerializer(serializers.ModelSerializer):
    address = serializers.CharField(required=True, read_only=False)

    class Meta:
        model = GeoPhoto
        fields = ['address',]

    def update(self, instance, validated_data):
        instance.address = validated_data.get('address', instance.address)
        instance.modified_at = now()
        instance.save(update_fields=['address', 'modified_at', ])
        return instance


class UpdateGeoPhotoSerializerForStaff(serializers.ModelSerializer):
    address = serializers.CharField(required=False)
    address_confirmed = serializers.BooleanField(required=False, allow_null=True)
    panorama_shot = HybridImageField(required=False, allow_null=True)
    moderation_status = serializers.CharField(required=False, allow_null=False, read_only=False)

    class Meta:
        model = GeoPhoto
        fields = ['address', 'address_confirmed', 'is_rejected', 'panorama_shot', 'moderation_status', 'geom']

    def update(self, instance, validated_data):
        instance.address = validated_data.get('address', instance.address)
        instance.address_confirmed = validated_data.get('address_confirmed', instance.address_confirmed)
        instance.is_rejected = validated_data.get('is_rejected', instance.is_rejected)
        if 'panorama_shot' in validated_data:
            if instance.panorama_shot and storages.S3Storage.exists(instance.panorama_shot.name):
                storages.S3Storage.delete(instance.panorama_shot.name)
            instance.panorama_shot = validated_data.get('panorama_shot')
        instance.moderation_status = validated_data.get('moderation_status', instance.moderation_status)

        if instance.moderation_status not in [GeoPhoto.CONFIRMED, GeoPhoto.NOT_MODERATED]:
            instance.is_rejected = True

        instance.geom = validated_data.get('geom', instance.geom)
        instance.moderator = self.context['request'].user
        instance.modified_at = now()
        instance.save(update_fields=['address', 'address_confirmed', 'modified_at', 'is_rejected', 'panorama_shot', 'moderation_status', 'moderator', 'geom'])

        if instance.moderation_status not in [GeoPhoto.CONFIRMED, GeoPhoto.NOT_MODERATED]:
            vq = Violation.objects.filter(car_plate__photo__id=instance.id)
            for v in vq:
                v.moderation_status = instance.moderation_status
                v.is_published = False
                v.moderator = self.context["request"].user
                v.save(update_fields=['moderation_status', 'moderator', 'is_published'])
            cq = CarPlate.objects.filter(photo__id=instance.id)
            for c in cq:
                c.moderation_status = instance.moderation_status
                c.moderator = self.context["request"].user
                c.number_confirmed = False
                c.save(update_fields=['moderation_status', 'moderator', 'number_confirmed'])

        return instance
