from rest_framework import serializers

from django.utils.timezone import now

from core.models import Violation
from .geo_photo import ListedGeoPhotoSerializerForStaff, ListedGeoPhotoSerializerForMap
from .base import BaseSerializer
from core.models.car_plate import CarPlate


class ListedCarPlateSerializer(BaseSerializer):
    plate_number = serializers.CharField(read_only=True)
    b_box = serializers.JSONField(read_only=True)
    number_confirmed = serializers.BooleanField(read_only=True)
    moderation_status = serializers.CharField(read_only=True)

    class Meta(BaseSerializer.Meta):
        model = CarPlate
        fields = BaseSerializer.Meta.fields + ["plate_number", "number_confirmed", "b_box", "moderation_status"]
        read_only_fields = BaseSerializer.Meta.read_only_fields + ["plate_number", "number_confirmed", "b_box", "moderation_status"]


class DetailsCarPlateSerializer(ListedCarPlateSerializer):
    photo = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta(ListedCarPlateSerializer.Meta):
        model = CarPlate
        fields = ListedCarPlateSerializer.Meta.fields + ["photo"]
        read_only_fields = ListedCarPlateSerializer.Meta.read_only_fields + ["photo"]


class ListedCarPlateSerializerForMap(BaseSerializer):
    plate_number = serializers.CharField(read_only=True)
    moderation_status = serializers.CharField(read_only=True)
    photo = ListedGeoPhotoSerializerForMap(read_only=True)

    class Meta(BaseSerializer.Meta):
        model = CarPlate
        fields = BaseSerializer.Meta.fields + ["plate_number", "moderation_status", "photo"]
        read_only_fields = BaseSerializer.Meta.read_only_fields + ["plate_number", "moderation_status", "photo"]


class UpdateCarPlateSerializer(serializers.ModelSerializer):
    plate_number = serializers.CharField(required=True, max_length=16)

    class Meta:
        model = CarPlate
        fields = ["plate_number"]

    def update(self, instance, validated_data):
        instance.plate_number = validated_data.get("plate_number", instance.plate_number)
        instance.modified_at = now()
        instance.save(update_fields=["plate_number", "modified_at"])
        return instance


class UpdateCarPlateSerializerForStaff(serializers.ModelSerializer):
    number_confirmed = serializers.BooleanField(required=False, allow_null=True)
    plate_number = serializers.CharField(required=False, max_length=16)
    moderation_status = serializers.CharField(required=False, allow_null=False, read_only=False)

    class Meta:
        model = CarPlate
        fields = ["number_confirmed", "plate_number", "moderation_status"]

    def update(self, instance, validated_data):
        instance.plate_number = validated_data.get("plate_number", instance.plate_number)
        instance.number_confirmed = validated_data.get("number_confirmed", instance.number_confirmed)
        instance.moderation_status = validated_data.get("moderation_status", instance.moderation_status)

        if instance.moderation_status not in [CarPlate.CONFIRMED, CarPlate.NOT_MODERATED]:
            instance.number_confirmed = False

        instance.modified_at = now()
        instance.moderator = self.context["request"].user
        instance.save(update_fields=["plate_number", "modified_at", "number_confirmed", "moderation_status", "moderator"])

        if instance.moderation_status not in [CarPlate.CONFIRMED, CarPlate.NOT_MODERATED]:
            vq = Violation.objects.filter(car_plate__id=instance.id)
            for v in vq:
                v.moderation_status = instance.moderation_status
                v.moderator = self.context["request"].user
                v.save(update_fields=['moderation_status', 'moderator'])

        return instance


class ListedCarPlateSerializerForStaff(BaseSerializer):
    plate_number = serializers.CharField(read_only=False)
    b_box = serializers.JSONField(read_only=True)
    photo = ListedGeoPhotoSerializerForStaff(read_only=True)
    number_confirmed = serializers.BooleanField(read_only=False)
    moderation_status = serializers.CharField(read_only=False)

    class Meta(BaseSerializer.Meta):
        model = CarPlate
        fields = BaseSerializer.Meta.fields + ["plate_number", "number_confirmed", "photo", "b_box", "moderation_status"]
        read_only_fields = BaseSerializer.Meta.read_only_fields + ["photo", "b_box", "plate_number", "number_confirmed"]


class DetailsCarPlateSerializerForStaff(ListedCarPlateSerializerForStaff):

    class Meta(ListedCarPlateSerializerForStaff.Meta):
        model = CarPlate
        fields = ListedCarPlateSerializerForStaff.Meta.fields
        read_only_fields = ListedCarPlateSerializerForStaff.Meta.read_only_fields
