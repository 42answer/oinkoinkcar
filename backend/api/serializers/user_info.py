from django.utils.timezone import now
from rest_framework import serializers
from core.models.user import User


class UserInfoSerializer(serializers.ModelSerializer):
    groups = serializers.SerializerMethodField("get_groups")

    class Meta:
        model = User
        id_field = 'id'
        fields = [
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'is_active',
            'is_staff',
            'is_superuser',
            'date_joined',
            'created_at',
            'modified_at',
            'provider_id',
            'initialized',
            'groups'
        ]
        read_only_fields = [
            'id',
            'username',
            'email',
            'is_active',
            'is_staff',
            'is_superuser',
            'date_joined',
            'created_at',
            'modified_at',
            'provider_id',
            'initialized'
        ]

    def get_groups(self, obj):
        return obj.groups.values_list('name', flat=True)

    def update(self, instance, validated_data):
        validated_data["modified_at"] = now()
        return super().update(instance, validated_data)
