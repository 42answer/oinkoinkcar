from rest_framework import serializers

from core.models.base_model import BaseModel


class BaseSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(read_only=True, format='%Y-%m-%d')
    modified_at = serializers.DateTimeField(read_only=True)

    class Meta:
        model = BaseModel
        field_id = 'id'
        fields = ['id', 'created_at', 'modified_at']
        read_only_fields = ['id', 'created_at', 'modified_at']
