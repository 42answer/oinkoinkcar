from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer, GeometrySerializerMethodField
from django.utils.timezone import now

from core.models import TypeOfViolation, CarPlate
from .base import BaseSerializer
from .car_plate import ListedCarPlateSerializerForStaff, ListedCarPlateSerializerForMap
from .type_of_violation import TypeOfViolationSerializer, ListedTypeOfViolationSerializer
from core.models.violation import Violation


class ListedViolationSerializerForSiteMap(BaseSerializer):
    violation_type = ListedTypeOfViolationSerializer(read_only=True)
    car_plate = ListedCarPlateSerializerForMap(read_only=True)
    is_author = serializers.SerializerMethodField(method_name="get_is_author", read_only=True)
    nearest_parking_distance = serializers.IntegerField(read_only=True, source="car_plate.photo.nearest_parking_distance")

    class Meta(BaseSerializer.Meta):
        model = Violation
        fields = BaseSerializer.Meta.fields + ["is_published", "is_violator_fined", "violation_type", "car_plate", "moderation_status", "is_author", "nearest_parking_distance"]
        read_only_fields = BaseSerializer.Meta.fields + ["is_published", "is_violator_fined", "violation_type", "car_plate", "moderation_status", "is_author", "nearest_parking_distance"]

    def get_is_author(self, obj):
        return "request" in self.context and obj.car_plate.photo.author == self.context["request"].user


class ListedViolationSerializerForMap(GeoFeatureModelSerializer):
    plate_number = serializers.SerializerMethodField(method_name="get_plate_number", read_only=True)
    is_author = serializers.SerializerMethodField(method_name="get_is_author", read_only=True)
    point = GeometrySerializerMethodField(method_name="get_point", read_only=True)
    nearest_parking_distance = serializers.IntegerField(read_only=True, source="car_plate.photo.nearest_parking_distance")

    class Meta(BaseSerializer.Meta):
        model = Violation
        geo_field = "point"
        id_field = "id"
        fields = ["id", "moderation_status", "is_author", "point", "plate_number", "nearest_parking_distance"]
        read_only_fields = ["id", "moderation_status", "is_author", "point", "plate_number", "nearest_parking_distance"]

    def get_is_author(self, obj):
        return "request" in self.context and obj.car_plate.photo.author == self.context["request"].user

    def get_point(self, obj):
        return obj.car_plate.photo.geom

    def get_plate_number(self, obj):
        return obj.car_plate.plate_number


class ListedViolationSerializer(BaseSerializer):
    nearest_parking_distance = serializers.IntegerField(read_only=True, source="car_plate.photo.nearest_parking_distance")

    class Meta(BaseSerializer.Meta):
        model = Violation
        fields = BaseSerializer.Meta.fields + ["type_confirmed", "is_published", "violation_type", "car_plate", "moderation_status", "nearest_parking_distance"]
        read_only_fields = BaseSerializer.Meta.fields + ["type_confirmed", "is_published", "violation_type", "car_plate", "moderation_status", "nearest_parking_distance"]


class ListedViolationSerializerForStaff(ListedViolationSerializer):
    car_plate = ListedCarPlateSerializerForStaff(read_only=True)
    violation_type = TypeOfViolationSerializer(read_only=True)

    class Meta(ListedViolationSerializer.Meta):
        model = Violation
        fields = ListedViolationSerializer.Meta.fields + ["is_violator_fined"]
        read_only_fields = ListedViolationSerializer.Meta.read_only_fields + ["is_violator_fined"]


class DetailsViolationSerializer(ListedViolationSerializer):
    class Meta(ListedViolationSerializer.Meta):
        model = Violation
        fields = ListedViolationSerializer.Meta.fields + ["is_violator_fined"]
        read_only_fields = ListedViolationSerializer.Meta.fields + ["is_violator_fined"]


class DetailsViolationSerializerForStaff(ListedViolationSerializerForStaff):
    class Meta(DetailsViolationSerializer.Meta):
        model = Violation
        fields = ListedViolationSerializerForStaff.Meta.fields
        read_only_fields = ListedViolationSerializerForStaff.Meta.read_only_fields


class CreateViolationSerializer(BaseSerializer):
    violation_type = serializers.PrimaryKeyRelatedField(required=True, queryset=TypeOfViolation.objects.all())
    car_plate = serializers.PrimaryKeyRelatedField(required=True, queryset=CarPlate.objects.all())

    class Meta(BaseSerializer.Meta):
        model = Violation
        fields = BaseSerializer.Meta.fields + ["violation_type", "car_plate"]
        read_only_fields = BaseSerializer.Meta.read_only_fields


class UpdateViolationSerializer(serializers.ModelSerializer):
    violation_type = serializers.PrimaryKeyRelatedField(required=True, queryset=TypeOfViolation.objects.all())

    class Meta:
        model = Violation
        fields = ["violation_type"]


class UpdateViolationSerializerForStaff(serializers.ModelSerializer):
    type_confirmed = serializers.BooleanField(required=False, allow_null=True)
    violation_type = serializers.PrimaryKeyRelatedField(required=False, queryset=TypeOfViolation.objects.all())
    moderation_status = serializers.CharField(required=False, allow_null=False, read_only=False)

    class Meta:
        model = Violation
        fields = ["type_confirmed", "violation_type", "moderation_status"]

    def update(self, instance, validated_data):
        instance.type_confirmed = validated_data.get("type_confirmed", instance.type_confirmed)
        instance.violation_type = validated_data.get("violation_type", instance.violation_type)
        instance.moderation_status = validated_data.get("moderation_status", instance.moderation_status)
        instance.moderator = self.context["request"].user
        instance.modified_at = now()
        instance.save(update_fields=["type_confirmed", "violation_type", "moderator", "modified_at", "moderation_status"])
        return instance
