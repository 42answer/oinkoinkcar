from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer, GeometrySerializerMethodField

from core.models import CarPark, ParkState, Organization
from .base import BaseSerializer


class ParkStateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParkState
        fields = ['created_at', 'total_spaces', 'free_spaces', 'monthly_rent']
        read_only_fields = ['created_at', 'total_spaces', 'free_spaces', 'monthly_rent']


class ListedCarParkSerializer(GeoFeatureModelSerializer):
    is_active = serializers.BooleanField(read_only=True)
    address = serializers.CharField(read_only=True)
    type = serializers.ChoiceField(choices=CarPark.PARKING_TYPES, read_only=True)
    last_state = serializers.SerializerMethodField(method_name="get_last_state", read_only=True)
    public_info = serializers.SerializerMethodField(method_name="get_public_info", read_only=True)

    class Meta(BaseSerializer.Meta):
        model = CarPark
        geo_field = "geom"
        id_field = "id"
        fields = BaseSerializer.Meta.fields + ["is_active", "geom", "address", "type", "last_state", "photo", "public_info"]
        read_only_fields = BaseSerializer.Meta.read_only_fields + ["is_active", "geom", "address", "type", "last_state", "photo", "public_info"]

    def get_last_state(self, obj):
        last_state = obj.parkstate_set.last()
        if last_state:
            return ParkStateSerializer(last_state).data
        return None

    def get_public_info(self, obj):
        if obj.organization and obj.organization.pub_contact_person:
            return {
                "name": obj.organization.pub_contact_person.name,
                "phone_number": obj.organization.pub_contact_person.phone_number,
                "notes": obj.organization.pub_contact_person.notes
            }
        return None


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ['id', 'name']
        read_only_fields = ['id', 'name']


class DetailedCarParkSerializer(ListedCarParkSerializer):
    organization = OrganizationSerializer(read_only=True)

    class Meta(ListedCarParkSerializer.Meta):
        model = CarPark
        fields = ListedCarParkSerializer.Meta.fields + ["organization"]
        read_only_fields = ListedCarParkSerializer.Meta.read_only_fields + ["organization"]
