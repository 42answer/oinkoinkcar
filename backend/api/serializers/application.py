import logging

from django.utils.timezone import now
from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied, ValidationError

from core.models import Violation, Place, Powers, Applicant
from .base import BaseSerializer
from core.models.application import Application
from api.tasks.send_application import send_application

logger = logging.getLogger(__name__)


class ListedApplicationSerializerForStaff(BaseSerializer):
    applicant = serializers.PrimaryKeyRelatedField(read_only=True)
    violation = serializers.PrimaryKeyRelatedField(read_only=True)
    powers = serializers.PrimaryKeyRelatedField(read_only=True)
    is_confirmed = serializers.BooleanField(read_only=True)
    sent_at = serializers.DateTimeField(read_only=True)
    pdf_file = serializers.FileField(read_only=True)
    rejected_reason = serializers.CharField(read_only=True)

    class Meta(BaseSerializer.Meta):
        model = Application
        fields = BaseSerializer.Meta.fields + ["applicant", "violation", "powers", "is_confirmed", "sent_at", "pdf_file", "rejected_reason"]
        read_only_fields = BaseSerializer.Meta.read_only_fields = ["applicant", "violation", "powers", "is_confirmed", "sent_at", "pdf_file", "rejected_reason"]


class DetailsApplicationSerializerForStaff(BaseSerializer):
    text = serializers.CharField(read_only=True)
    applicant = serializers.PrimaryKeyRelatedField(read_only=True)
    violation = serializers.PrimaryKeyRelatedField(read_only=True)
    powers = serializers.PrimaryKeyRelatedField(read_only=True)
    is_confirmed = serializers.BooleanField(read_only=True)
    sent_at = serializers.DateTimeField(read_only=True)
    pdf_file = serializers.FileField(read_only=True)
    rejected_reason = serializers.CharField(read_only=True)

    class Meta(BaseSerializer.Meta):
        model = Application
        fields = BaseSerializer.Meta.fields + ["text", "applicant", "violation", "powers", "is_confirmed", "sent_at", "pdf_file", "rejected_reason"]
        read_only_fields = BaseSerializer.Meta.read_only_fields + ["text", "applicant", "violation", "powers", "is_confirmed", "sent_at", "pdf_file", "rejected_reason"]


class ListedApplicationSerializerForCreate(serializers.ModelSerializer):
    applicant = serializers.PrimaryKeyRelatedField(required=True, queryset=Applicant.objects.all())
    violation = serializers.PrimaryKeyRelatedField(required=True, queryset=Violation.objects.all().filter(type_confirmed=True))

    class Meta:
        model = Application
        fields = ["applicant", "violation"]

    def create(self, validated_data):
        applicant: Applicant = validated_data.pop("applicant")
        violation: Violation = validated_data.pop("violation")
        point = violation.car_plate.photo.geom
        places = Place.objects.filter(geom__contains=point)
        if not places.exists():
            logger.error(f'No place found for violation {violation.id}')
            raise serializers.ValidationError("No place found for this violation")
        powers = Powers.objects.filter(place__in=places, type_of_violation=violation.violation_type, is_active=True)
        if not powers.exists():
            logger.error(f'No powers found for violation {violation.id}')
            raise serializers.ValidationError("No powers found for this violation")
        if powers.count() > 1:
            logger.error(f'More than one power found for violation {violation.id}')
            raise serializers.ValidationError("More than one power found for this violation")
        return Application.objects.create(applicant=applicant, violation=violation, powers=powers.first())

    def validate(self, attrs):
        applicant: Applicant = attrs["applicant"]
        violation: Violation = attrs["violation"]
        if "request" in self.context and applicant.user != self.context["request"].user:
            logger.critical(f'User {self.context["request"].user.id} tried to create application for another user {applicant.user.id}')
            raise PermissionDenied()
        # FIXME: For idempotency we must check if application already exists and return it
        if Application.objects.filter(violation=violation).exists():
            logger.error(f'Application for violation {violation.id} already exists')
            raise ValidationError("Application already exists")
        return attrs


class ApplicationSerializerForPatch(serializers.ModelSerializer):
    is_confirmed = serializers.BooleanField(required=False, read_only=False)
    text = serializers.CharField(required=False, read_only=False)
    rejected_reason = serializers.CharField(required=False, read_only=False)

    class Meta:
        model = Application
        fields = ["is_confirmed", "text", "rejected_reason"]

    def update(self, instance, validated_data):
        instance.is_confirmed = validated_data.get("is_confirmed", instance.is_confirmed)
        instance.text = validated_data.get("text", instance.text)
        instance.rejected_reason = validated_data.get("rejected_reason", instance.rejected_reason)
        instance.modified_at = now()
        instance.save(update_fields=["is_confirmed", "text", "rejected_reason", "modified_at"])
        if instance.is_confirmed:
            instance.violation.is_published = True
            instance.violation.save(update_fields=["is_published"])

            def run_tasks():
                send_application.delay(instance.id)
            transaction.on_commit(run_tasks)

        return instance
