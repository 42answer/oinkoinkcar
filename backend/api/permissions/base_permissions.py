from rest_framework import permissions
from core.models.site_state import SiteState


class ReadOnlyPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        if request.user.is_superuser:
            return True
        return False


class MaintenanceModeModeratorPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        site_state = SiteState.objects.all().first()
        if request.method in permissions.SAFE_METHODS or (site_state.moderation_mode == SiteState.MODE_NORMAL and site_state.public_mode == SiteState.MODE_NORMAL):
            return True
        if request.user.is_superuser:
            return True
        return False


class MaintenanceModePublicPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS or SiteState.objects.all().first().public_mode == SiteState.MODE_NORMAL:
            return True
        if request.user.is_superuser:
            return True
        return False
