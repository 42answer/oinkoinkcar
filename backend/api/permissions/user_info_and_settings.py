from rest_framework import permissions


def _has_permission_to_view(request):
    return (request.method == 'GET' and request.user.is_authenticated) or request.user.is_superuser


class UserInfoPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return _has_permission_to_view(request)

    def has_object_permission(self, request, view, obj):
        return (obj == request.user and request.method == 'GET') or request.user.is_superuser


class FrontendUserSettingsPermission(UserInfoPermission):

    def has_permission(self, request, view):
        return _has_permission_to_view(request)

    def has_object_permission(self, request, view, obj):
        return (obj.user == request.user and request.method == 'GET') or request.user.is_superuser
