from django.utils import timezone
from datetime import timedelta
from rest_framework import permissions

from config import settings
from core.models import Violation
from .utils import is_authenticated_and_not_banned, is_not_banned_moderator


class ViolationPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.method in ['GET', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS']

    def has_object_permission(self, request, view, obj: Violation):
        if request.method in permissions.SAFE_METHODS and (obj.is_published or obj.car_plate.photo.author == request.user):
            return True
        if request.method in ['PATCH', 'DELETE'] and is_authenticated_and_not_banned(request.user) and \
                obj.car_plate.photo.author == request.user and obj.type_confirmed is None and \
                timezone.now() - obj.created_at < timedelta(hours=settings.TIMEDELTA_TO_DELETE_HOURS):
            return True
        if request.user.is_superuser:
            return True
        return False


class ViolationPermissionForStaff(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in ['GET', 'PATCH', 'DELETE'] and is_not_banned_moderator(request.user):
            return True
        if request.user.is_superuser:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.method == 'GET' and is_not_banned_moderator(request.user):
            return True
        if (request.method == "PATCH" and is_not_banned_moderator(request.user) and
                request.user != obj.car_plate.photo.author and obj.type_confirmed is None and
                obj.moderation_status == Violation.NOT_MODERATED and
                timezone.now() - obj.created_at < timedelta(hours=settings.TIMEDELTA_TO_MODERATE_HOURS)):
            return True
        if request.user.is_superuser:
            return True
        return False
