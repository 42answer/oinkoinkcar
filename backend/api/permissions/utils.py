from core.models import User


def is_not_banned_moderator(user: User):
    return user.is_authenticated and not user.is_superuser and user.groups.filter(name='Moderator').exists() and not user.groups.filter(name='Banned').exists()


def is_authenticated_and_not_banned(user: User):
    return user.is_authenticated and not user.groups.filter(name='Banned').exists()


def is_authenticated_and_banned(user: User):
    return user.is_authenticated and user.groups.filter(name='Banned').exists()
