from datetime import timedelta

from django.utils import timezone
from rest_framework import permissions

from config import settings
from core.models import CarPlate
from .utils import is_not_banned_moderator


class CarPlatePermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.groups.filter(name='Banned').exists():
            return False
        if request.method == 'GET':
            return True
        if request.method in ['DELETE', 'PATCH'] and request.user.is_authenticated:
            return True
        if request.user.is_superuser:
            return True
        return False

    def has_object_permission(self, request, view, obj: CarPlate):
        if request.user.groups.filter(name='Banned').exists():
            return False
        if request.method in permissions.SAFE_METHODS:
            return True
        if (request.method in ['DELETE', 'PATCH'] and obj.photo.author == request.user and timezone.now() -
                obj.created_at < timedelta(hours=settings.TIMEDELTA_TO_DELETE_HOURS) and obj.number_confirmed is None):
            return True
        if request.user.is_superuser:
            return True
        return False


class CarPlatePermissionForStaff(permissions.BasePermission):

    def has_permission(self, request, view):
        if is_not_banned_moderator(request.user) and request.method in ['GET', 'PATCH', 'DELETE']:
            return True
        if request.user.is_superuser:
            return True
        return False

    def has_object_permission(self, request, view, obj: CarPlate):
        if request.method == 'GET':
            return True
        if request.method == "PATCH" and is_not_banned_moderator(request.user) and \
                request.user != obj.photo.author and obj.number_confirmed is None and obj.moderation_status == CarPlate.NOT_MODERATED and \
                timezone.now() - obj.created_at < timedelta(hours=settings.TIMEDELTA_TO_MODERATE_HOURS):
            return True
        if request.user.is_superuser:
            return True
        return False
