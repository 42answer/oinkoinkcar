from datetime import timedelta

from django.utils.timezone import now
from rest_framework import permissions

from config import settings
from core.models import GeoPhoto
from .utils import is_authenticated_and_not_banned, is_not_banned_moderator


class GeoPhotoPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method != 'PATCH' and is_authenticated_and_not_banned(request.user):
            return True
        if request.user.is_superuser:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return True
        if request.method in permissions.SAFE_METHODS and is_authenticated_and_not_banned(request.user):
            return True
        if request.method == 'DELETE' and is_authenticated_and_not_banned(request.user) and obj.author == request.user and obj.address_confirmed is None and obj.created_at - now() < timedelta(hours=settings.TIMEDELTA_TO_DELETE_HOURS):
            return True
        return False


class GeoPhotoPermissionForStaff(permissions.BasePermission):

    def has_permission(self, request, view):
        if is_not_banned_moderator(request.user):
            return True
        if request.user.is_superuser:
            return True
        return False

    def has_object_permission(self, request, view, obj: GeoPhoto):
        if request.method == 'GET' and is_not_banned_moderator(request.user):
            return True
        if request.method == "PATCH" and is_not_banned_moderator(request.user) and \
                request.user != obj.author and obj.address_confirmed is None and obj.moderation_status == GeoPhoto.NOT_MODERATED and \
                now() - obj.created_at < timedelta(hours=settings.TIMEDELTA_TO_MODERATE_HOURS):
            return True
        if request.user.is_superuser:
            return True
        return False
