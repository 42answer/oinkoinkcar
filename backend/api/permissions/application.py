from rest_framework import permissions
from core.models import Application
from .utils import is_not_banned_moderator


class ApplicationPermissionForStaff(permissions.BasePermission):
    allowed_methods = ['GET', 'PATCH', 'POST']

    def has_permission(self, request, view):
        if request.method in self.allowed_methods and is_not_banned_moderator(request.user):
            return True
        if request.user.is_superuser:
            return True
        return False

    def has_object_permission(self, request, view, obj: Application):
        if request.method in self.allowed_methods and is_not_banned_moderator(request.user) and obj.applicant.user == request.user:
            return True
        if request.user.is_superuser:
            return True
        return False
