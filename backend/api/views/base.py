from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import GenericAPIView
from django.db import models
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache

from api.paginators import ListedPagination
from api.decorators import idempotency_method


class ListedBaseView(GenericAPIView, mixins.CreateModelMixin, mixins.ListModelMixin):
    permission_classes = []
    model: models.Model = None
    filter_backends = [SearchFilter, OrderingFilter, DjangoFilterBackend]
    pagination_class = ListedPagination
    lookup_url_kwarg = "uuid"
    lookup_field = "id"
    serializer_class = None
    serializer_class_for_create = None
    filterset_class = None

    @method_decorator(never_cache, name='dispatch')
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    @idempotency_method
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get_serializer_class(self):
        if self.request.method == "POST":
            return self.serializer_class_for_create or self.serializer_class
        return self.serializer_class


class DetailsBaseView(GenericAPIView, mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin):
    permission_classes = []
    filter_backends = []
    model: models.Model = None
    serializer_class = None
    serializer_class_for_edit = None
    lookup_url_kwarg = "uuid"
    lookup_field = "id"

    @method_decorator(never_cache, name='dispatch')
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.http_method_not_allowed(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.http_method_not_allowed(request, *args, **kwargs)

    @idempotency_method
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    @idempotency_method
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def get_serializer_class(self):
        if self.request.method == "PATCH":
            return self.serializer_class_for_edit or self.serializer_class
        return self.serializer_class
