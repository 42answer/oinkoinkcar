from ..base import DetailsBaseView
from core.models.geo_photo import GeoPhoto
from api.serializers.geo_photo import DetailsGeoPhotoSerializer, DetailsGeoPhotoSerializerForStaff, UpdateGeoPhotoSerializer, UpdateGeoPhotoSerializerForStaff
from api.permissions.geo_photo import GeoPhotoPermission, GeoPhotoPermissionForStaff
from api.permissions.base_permissions import MaintenanceModeModeratorPermission, MaintenanceModePublicPermission


class DetailsGeoPhotoView(DetailsBaseView):
    model = GeoPhoto
    serializer_class = DetailsGeoPhotoSerializer
    serializer_class_for_edit = UpdateGeoPhotoSerializer
    queryset = GeoPhoto.objects.all()
    permission_classes = [GeoPhotoPermission, MaintenanceModePublicPermission]


class DetailsGeoPhotoViewForStaff(DetailsGeoPhotoView):
    serializer_class = DetailsGeoPhotoSerializerForStaff
    serializer_class_for_edit = UpdateGeoPhotoSerializerForStaff
    permission_classes = [GeoPhotoPermissionForStaff, MaintenanceModeModeratorPermission]
