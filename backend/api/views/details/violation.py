from ..base import DetailsBaseView
from core.models import Violation
from api.serializers.violation import DetailsViolationSerializerForStaff, UpdateViolationSerializer, UpdateViolationSerializerForStaff, ListedViolationSerializerForSiteMap
from api.permissions.violation import ViolationPermission, ViolationPermissionForStaff
from api.permissions.base_permissions import MaintenanceModeModeratorPermission, MaintenanceModePublicPermission


class DetailsViolationView(DetailsBaseView):
    model = Violation
    serializer_class = ListedViolationSerializerForSiteMap
    serializer_class_for_edit = UpdateViolationSerializer
    queryset = Violation.objects.all()
    permission_classes = [ViolationPermission, MaintenanceModePublicPermission]


class DetailsViolationViewForStaff(DetailsViolationView):
    serializer_class = DetailsViolationSerializerForStaff
    serializer_class_for_edit = UpdateViolationSerializerForStaff
    permission_classes = [ViolationPermissionForStaff, MaintenanceModeModeratorPermission]
