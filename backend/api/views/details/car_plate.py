from ..base import DetailsBaseView
from core.models.car_plate import CarPlate
from api.serializers.car_plate import DetailsCarPlateSerializer, UpdateCarPlateSerializer, UpdateCarPlateSerializerForStaff, DetailsCarPlateSerializerForStaff
from api.permissions.car_plate import CarPlatePermission, CarPlatePermissionForStaff
from api.permissions.base_permissions import MaintenanceModeModeratorPermission, MaintenanceModePublicPermission


class DetailsCarPlateView(DetailsBaseView):
    model = CarPlate
    queryset = CarPlate.objects.all()
    serializer_class = DetailsCarPlateSerializer
    serializer_class_for_edit = UpdateCarPlateSerializer
    permission_classes = [CarPlatePermission, MaintenanceModePublicPermission]


class DetailsCarPlateViewForStaff(DetailsCarPlateView):
    model = CarPlate
    queryset = CarPlate.objects.all()
    serializer_class = DetailsCarPlateSerializerForStaff
    serializer_class_for_edit = UpdateCarPlateSerializerForStaff
    permission_classes = [CarPlatePermissionForStaff, MaintenanceModeModeratorPermission]
