from ..base import DetailsBaseView
from core.models.application import Application
from api.serializers.application import DetailsApplicationSerializerForStaff, ApplicationSerializerForPatch
from api.permissions.base_permissions import MaintenanceModeModeratorPermission
from api.permissions.application import ApplicationPermissionForStaff


class DetailsApplicationViewForStaff(DetailsBaseView):
    model = Application
    serializer_class = DetailsApplicationSerializerForStaff
    permission_classes = [ApplicationPermissionForStaff, MaintenanceModeModeratorPermission]

    def get_object(self):
        if self.request.user.is_superuser:
            return Application.objects.get(id=self.kwargs['uuid'])
        return Application.objects.get(id=self.kwargs['uuid'], applicant__user=self.request.user)

    def get_serializer_class(self):
        if self.request.method == 'PATCH':
            return ApplicationSerializerForPatch
        return DetailsApplicationSerializerForStaff
