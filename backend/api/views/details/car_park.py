from ..base import DetailsBaseView
from core.models import CarPark
from api.serializers.car_park import DetailedCarParkSerializer
from api.permissions.base_permissions import ReadOnlyPermission


class DetailsCarParkView(DetailsBaseView):
    model = CarPark
    queryset = CarPark.objects.all()
    serializer_class = DetailedCarParkSerializer
    permission_classes = [ReadOnlyPermission]
