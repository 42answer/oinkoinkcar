from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework import status
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache

from core.models import SiteState
from config.settings.base import STAGE, FRONTEND_VER
from ..permissions.base_permissions import ReadOnlyPermission


class SiteStateAPIView(GenericAPIView):
    http_method_names = ['get', 'head', ]
    permission_classes = [ReadOnlyPermission]

    @method_decorator(never_cache, name='dispatch')
    def get(self, request):
        state = SiteState.objects.first()
        context = {
            'public_mode': state.public_mode,
            'message_for_public': state.message_for_public,
            'moderation_mode': state.moderation_mode,
            'message_for_moderators': state.message_for_moderators,
            'stage': STAGE,
            'commit_sha': FRONTEND_VER,   # for backward compatibility
            'frontend_ver': FRONTEND_VER,
            'tile_layer_url': state.tile_layer.layer_url if state.tile_layer else '',
        }
        return Response(context, status=status.HTTP_200_OK)
