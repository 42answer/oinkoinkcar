from datetime import datetime, timedelta

from django.http import JsonResponse
from rest_framework import mixins
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache

from core.models import Violation
from ..permissions.user_info_and_settings import UserInfoPermission
from core.models.user import User
from ..serializers.user_info import UserInfoSerializer


class DetailsUserInfoView(mixins.DestroyModelMixin, mixins.UpdateModelMixin, mixins.RetrieveModelMixin, GenericAPIView):
    model = User
    serializer_class = UserInfoSerializer
    queryset = User.objects.all()
    permission_classes = [UserInfoPermission]
    filter_backends = []

    def get_object(self):
        return self.request.user

    @method_decorator(never_cache, name='dispatch')
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        # TODO: Implement user deletion
        return Response(status=status.HTTP_501_NOT_IMPLEMENTED, data={'detail': 'Method "DELETE" not implemented yet.'})


class UserViolationStats(APIView):
    permission_classes = [IsAuthenticated]

    @method_decorator(never_cache, name='dispatch')
    def get(self, request):
        user = request.user
        thirty_days_ago = datetime.now() - timedelta(days=30)

        total_violations = Violation.objects.filter(created_at__gte=thirty_days_ago, car_plate__photo__author=user).count()
        total_confirmed = Violation.objects.filter(created_at__gte=thirty_days_ago, car_plate__photo__author=user, moderation_status=Violation.CONFIRMED).count()
        total_not_confirmed = Violation.objects.filter(created_at__gte=thirty_days_ago, car_plate__photo__author=user).exclude(moderation_status=Violation.CONFIRMED).exclude(moderation_status=Violation.NOT_MODERATED).count()
        # Calculate the percentages
        previous_thirty_days = thirty_days_ago - timedelta(days=30)
        previous_total_violations = Violation.objects.filter(created_at__gte=previous_thirty_days, created_at__lt=thirty_days_ago, car_plate__photo__author=user).count()
        previous_total_type_confirmed = Violation.objects.filter(created_at__gte=previous_thirty_days, created_at__lt=thirty_days_ago, car_plate__photo__author=user, moderation_status=Violation.CONFIRMED).count()
        previous_total_not_confirmed = Violation.objects.filter(created_at__gte=previous_thirty_days, created_at__lt=thirty_days_ago, car_plate__photo__author=user).exclude(moderation_status=Violation.CONFIRMED).exclude(moderation_status=Violation.NOT_MODERATED).count()

        percentage_total_violations_change = ((total_violations - previous_total_violations) / previous_total_violations) * 100 if previous_total_violations != 0 else 0
        percentage_confirmed_change = ((total_confirmed - previous_total_type_confirmed) / previous_total_type_confirmed) * 100 if previous_total_type_confirmed != 0 else 0
        percentage_not_confirmed_change = ((total_not_confirmed - previous_total_not_confirmed) / previous_total_not_confirmed) * 100 if previous_total_not_confirmed != 0 else 0

        # format percentage to 2 decimal places
        response_data = {
            "total_violations_last_30_days": total_violations,
            "percentage_total_violations_change": round(percentage_total_violations_change, 2),
            "total_type_confirmed_last_30_days": total_confirmed,
            "percentage_type_confirmed_change": round(percentage_confirmed_change, 2),
            "total_not_confirmed_last_30_days": total_not_confirmed,
            "percentage_not_confirmed_change": round(percentage_not_confirmed_change, 2),
        }

        return JsonResponse(response_data)
