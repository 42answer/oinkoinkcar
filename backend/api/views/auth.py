from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.core.handlers.wsgi import WSGIRequest
from django.core.handlers.asgi import ASGIRequest
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as auth_logout
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from social_django.models import UserSocialAuth
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
import datetime
import requests
import logging

from config import settings

logger = logging.getLogger(__name__)


@csrf_exempt
@require_http_methods(['POST'])
@api_view(['POST'])
@permission_classes([AllowAny])
def refresh_token(request: WSGIRequest | ASGIRequest) -> HttpResponse:
    token = request.data.get('refresh_token', None)
    if not token:
        logger.error('Invalid request, no refresh token provided.')
        return JsonResponse({'error': 'Invalid request'}, status=400)

    refresh_token_request = {
        "grant_type": "refresh_token",
        "client_id": settings.OAUTH_CLIENT_ID_KEY,
        "client_secret": settings.OAUTH_CLIENT_SECRET_KEY,
        "refresh_token": token
    }

    r = requests.post(settings.REFRESH_TOKEN_ENDPOINT, data=refresh_token_request, verify=False, headers={'X-CSRFToken': request.COOKIES.get('csrftoken', '')})
    if r.status_code != 200:
        logger.error(f'Error while refreshing token: {r.text}')
        return JsonResponse({'error': r.text}, status=r.status_code)

    response_data = r.json()
    access_token = response_data.get('access_token', None)
    expires_in = response_data.get('expires_in', None)
    r_token = response_data.get('refresh_token', None)

    if not access_token or not expires_in or not r_token:
        logger.error('Invalid response from oauth server, missing access token, expires in or refresh token.')
        return JsonResponse({'error': 'Invalid response from oauth server'}, status=500)

    cookie_expires = datetime.datetime.now() + datetime.timedelta(days=settings.COOKIE_EXPIRES_DAYS)
    expires_date_time = datetime.datetime.now() + datetime.timedelta(seconds=expires_in)
    response = JsonResponse({'access_token': access_token, 'expires_in': expires_in, 'refresh_token': r_token}, status=200)

    response.set_cookie('authToken', access_token, expires=cookie_expires, secure=True, samesite='Strict')
    response.set_cookie('expiresAt', expires_date_time.strftime('%Y-%m-%dT%H:%M:%S.%fZ'), expires=cookie_expires, secure=True, samesite='Strict')
    response.set_cookie('refreshToken', r_token, expires=cookie_expires, secure=True, samesite='Strict')
    return response


@never_cache
@require_http_methods(['GET'])
@login_required
def success(request: WSGIRequest | ASGIRequest) -> HttpResponse:
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/')
    if not request.user.is_active:
        return HttpResponseRedirect(settings.SOCIAL_AUTH_INACTIVE_USER_URL)

    response = HttpResponseRedirect(settings.REDIRECT_AFTER_LOGIN)

    social_user = UserSocialAuth.objects.filter(user=request.user).order_by('-modified').first()
    provider = social_user.provider
    extra = social_user.extra_data
    access_token = extra.get('access_token', None)

    payload = {
        "grant_type": "convert_token",
        "client_id": settings.OAUTH_CLIENT_ID_KEY,
        "client_secret": settings.OAUTH_CLIENT_SECRET_KEY,
        "backend": provider,
        "token": access_token
    }

    r = requests.post(settings.CONVERT_TOKEN_ENDPOINT, data=payload, verify=False, headers={'X-CSRFToken': request.COOKIES.get('csrftoken', '')})

    if r.status_code != 200:
        logger.error(f'Error while converting token: {r.text}')
        return HttpResponse(r.text, status=r.status_code)
    response_data = r.json()

    access_token = response_data.get('access_token', None)
    expires_in = response_data.get('expires_in', None)
    r_token = response_data.get('refresh_token', None)
    if not access_token or not expires_in or not r_token:
        logger.error('Invalid response from oauth server, missing access token, expires in or refresh token.')
        return HttpResponse('Invalid response from oauth server', status=500)

    expires = datetime.datetime.now() + datetime.timedelta(seconds=expires_in)

    # TODO: check if user is banned. Check cookie expiration time and other stuff
    cookie_expires = datetime.datetime.now() + datetime.timedelta(days=settings.COOKIE_EXPIRES_DAYS)
    response.set_cookie('authToken', access_token, expires=cookie_expires, secure=True, samesite='Strict')
    response.set_cookie('expiresAt', expires.strftime('%Y-%m-%dT%H:%M:%S.%fZ'), expires=cookie_expires, secure=True, samesite='Strict')
    response.set_cookie('refreshToken', r_token, expires=cookie_expires, secure=True, samesite='Strict')

    if request.user.is_superuser or (request.user.groups.filter(name='Moderator').exists() and not request.user.groups.filter(name='Banned').exists()):
        response.set_cookie('GOOGLE_MAPS_API_KEY', settings.GOOGLE_MAPS_API_KEY, expires=cookie_expires, secure=True, samesite='Strict')
    logger.info(f'User {request.user.id} logged in successfully.')
    return response


@csrf_exempt
@require_http_methods(['POST'])
@login_required
def logout(request: WSGIRequest | ASGIRequest) -> HttpResponse:

    # TODO: remove all tokens from database when user clicks logout from all devices
    # from oauth2_provider.models import AccessToken, RefreshToken
    # UserSocialAuth.objects.filter(user=request.user).delete()
    # AccessToken.objects.filter(user=request.user).delete()
    # RefreshToken.objects.filter(user=request.user).delete()

    uid = request.user.id
    auth_logout(request)

    response = HttpResponseRedirect(settings.REDIRECT_AFTER_LOGOUT)
    response.delete_cookie('authToken')
    response.delete_cookie('expiresAt')
    response.delete_cookie('refreshToken')
    response.delete_cookie('GOOGLE_MAPS_API_KEY')
    response.delete_cookie('csrftoken')
    response.delete_cookie('sessionid')
    logger.info(f'User {uid} logged out successfully.')
    return response


@never_cache
@require_http_methods(['GET'])
@api_view(['GET'])
@permission_classes([AllowAny])
def login(request: WSGIRequest | ASGIRequest) -> HttpResponse:
    # This life hack is necessary to address the following bug: when the user has PWA installed but attempts to authenticate via the browser, after selecting the account on the OAuth2 server side,
    # there is a reverse redirect to the site at the path /complete/google-oauth2/, and instead of opening the authorization result page, the browser tries to open the page in the PWA, where the state is no longer accessible,
    # resulting in an error. To avoid this, we redirect to a second domain, which already redirects to the required path on the main domain (before auth), and this redirect is intercepted by the PWA.

    provider = request.GET.get('provider', None)
    if provider is None or provider not in settings.AUTH_PROVIDERS:
        return HttpResponseRedirect(f'https://{settings.APP_DOMAIN}{settings.SOCIAL_AUTH_LOGIN_ERROR_URL}')

    if not settings.DEV and settings.LOGIN_DOMAIN:
        if request.get_host() == settings.APP_DOMAIN:
            return HttpResponseRedirect(f'https://{settings.LOGIN_DOMAIN}/api/auth/login/?provider={provider}')
        context = {
            'redirect_url': f'https://{settings.APP_DOMAIN}/login/{provider}/',
            'APP_DOMAIN': settings.APP_DOMAIN
        }
        return render(request, 'login_redirect_page.html', context)
    return HttpResponseRedirect(f'https://{settings.APP_DOMAIN}/login/{provider}/')
