from datetime import timedelta
import logging
import psutil

from django.http import JsonResponse
from django.utils import timezone
from django.views.decorators.http import require_GET
from rest_framework import status
from django.views.decorators.cache import never_cache

from core.models import Application, GeoPhoto, CarPlate, Violation, User
from config import settings

logger = logging.getLogger(__name__)


@never_cache
@require_GET
def email_sending_health_check(request):
    unsent_applications = Application.objects.filter(failed_attempts=5, sent_at__isnull=True)
    if unsent_applications.exists():
        unsent = [str(x.id) for x in unsent_applications]
        logger.error(f'System unhealthy, unsent applications: {unsent}')
        return JsonResponse({"unsent_applications": unsent}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return JsonResponse({"unsent_applications": []}, status=status.HTTP_200_OK)


@never_cache
@require_GET
def moderation_timeout_health_check(request):
    photos = GeoPhoto.objects.filter(created_at__lt=timezone.now() - timedelta(hours=settings.MODERATION_TIMEOUT_WARNING_HOURS), moderation_status=GeoPhoto.NOT_MODERATED)
    violations = Violation.objects.filter(created_at__lt=timezone.now() - timedelta(hours=settings.MODERATION_TIMEOUT_WARNING_HOURS), moderation_status=Violation.NOT_MODERATED)
    plates = CarPlate.objects.filter(created_at__lt=timezone.now() - timedelta(hours=settings.MODERATION_TIMEOUT_WARNING_HOURS), moderation_status=CarPlate.NOT_MODERATED)
    response = {}
    ok = True
    if photos.exists():
        response["not_moderated_photos"] = [str(x.id) for x in photos]
        ok = False
    if violations.exists():
        response["not_moderated_violations"] = [str(x.id) for x in violations]
        ok = False
    if plates.exists():
        response["not_moderated_plates"] = [str(x.id) for x in plates]
        ok = False
    if ok:
        return JsonResponse({"not_moderated_photos": [], "not_moderated_violations": [], "not_moderated_plates": []}, status=status.HTTP_200_OK)
    logger.error(f'System unhealthy, not moderated: {response}')
    return JsonResponse(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@never_cache
@require_GET
def user_quality_health_check(request):
    users = User.objects.exclude(groups__name__in=["Moderator", "Banned"], is_superuser=True, is_active=False)
    bad_users = []
    for user in users:
        rejected_photos_count = GeoPhoto.objects.filter(author=user, is_rejected=True).count()
        total_photos_count = GeoPhoto.objects.filter(author=user).count()
        if not total_photos_count or not rejected_photos_count:
            continue
        if rejected_photos_count / total_photos_count >= settings.MAX_PERCENTAGE_OF_DEFECTS:
            bad_users.append(str(user.id))
    if len(bad_users) > 0:
        logger.error(f'System unhealthy, bad users: {bad_users}')
        return JsonResponse({"bad_users": bad_users}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return JsonResponse({"bad_users": []}, status=status.HTTP_200_OK)


@never_cache
@require_GET
def moderator_quality_health_check(request):
    moderators = User.objects.filter(groups__name="Moderator").exclude(is_superuser=True, is_active=False, groups__name="Banned")
    bad_moderators = []
    for moderator in moderators:
        rejected_photos_count = GeoPhoto.objects.filter(moderator=moderator, is_rejected=True).count()
        moderated_photos_count = GeoPhoto.objects.filter(moderator=moderator).count()
        if not moderated_photos_count or not rejected_photos_count:
            continue
        if rejected_photos_count / moderated_photos_count >= settings.MAX_PERCENTAGE_OF_DEFECT_MODERATOR:
            bad_moderators.append(str(moderator.id))
    if len(bad_moderators):
        logger.error(f'System unhealthy, bad moderators: {bad_moderators}')
        return JsonResponse({"bad_moderators": bad_moderators}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return JsonResponse({"bad_moderators": []}, status=status.HTTP_200_OK)


@never_cache
@require_GET
def disk_space_health_check(request):
    free_space_gb = psutil.disk_usage('/').free / (2**30)
    if free_space_gb < float(request.GET.get('min_space_gb', 2)):
        return JsonResponse({"message": "Low disk space", "free_space_gb": free_space_gb}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return JsonResponse({"message": "Disk space is sufficient", "free_space_gb": free_space_gb}, status=status.HTTP_200_OK)


@never_cache
@require_GET
def memory_usage_health_check(request):
    memory_percent = psutil.virtual_memory().percent
    if memory_percent > float(request.GET.get('max_memory_percent', 80)):
        return JsonResponse({"message": "High memory usage", "memory_percent": memory_percent}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return JsonResponse({"message": "Memory usage is normal", "memory_percent": memory_percent}, status=status.HTTP_200_OK)


@never_cache
@require_GET
def cpu_load_health_check(request):
    cpu_load_percent = psutil.cpu_percent(interval=1)
    if cpu_load_percent > float(request.GET.get('max_cpu_load_percent', 75)):
        return JsonResponse({"message": "High CPU load", "cpu_load_percent": cpu_load_percent}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return JsonResponse({"message": "CPU load is normal", "cpu_load_percent": cpu_load_percent}, status=status.HTTP_200_OK)


@never_cache
@require_GET
def basic_health_check(request):
    return JsonResponse({}, status=status.HTTP_200_OK)
