from rest_framework import mixins
from rest_framework.generics import GenericAPIView
from api.permissions.user_info_and_settings import FrontendUserSettingsPermission
from core.models.frontend_user_settings import FrontendUserSettings
from api.serializers.frontend_user_settings import FrontendUserSettingsSerializer
from api.permissions.base_permissions import MaintenanceModePublicPermission
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache


class DetailsFrontendUserSettingsView(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, GenericAPIView):
    model = FrontendUserSettings
    serializer_class = FrontendUserSettingsSerializer
    permission_classes = [FrontendUserSettingsPermission, MaintenanceModePublicPermission]
    filter_backends = []

    def get_object(self):
        return FrontendUserSettings.objects.get(user=self.request.user)

    @method_decorator(never_cache, name='dispatch')
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)
