from django.db.models import Q
from rest_framework_gis.filters import InBBoxFilter, DistanceToPointOrderingFilter
from rest_framework_gis.pagination import GeoJsonPagination

from api.views.base import ListedBaseView
from core.models.violation import Violation
from core.models.applicant import Applicant
from core.models.application import Application
from api.filtersets.violation import ViolationFilterSet, ViolationFilterSetForStaff
from api.serializers.violation import ListedViolationSerializerForStaff, ListedViolationSerializerForSiteMap, ListedViolationSerializerForMap
from api.permissions.violation import ViolationPermission, ViolationPermissionForStaff
from api.permissions.base_permissions import ReadOnlyPermission
from api.permissions.base_permissions import MaintenanceModeModeratorPermission, MaintenanceModePublicPermission


class ListedViolationViewForSiteMap(ListedBaseView):
    model = Violation
    permission_classes = [ReadOnlyPermission, MaintenanceModePublicPermission]
    serializer_class = ListedViolationSerializerForSiteMap
    filterset_class = None
    search_fields = []
    filter_backends = []

    def get_queryset(self):
        return Violation.objects.prefetch_related('car_plate__photo').filter(car_plate__photo__is_rejected=False, is_published=True)


class ListedViolationViewForMap(ListedBaseView):
    model = Violation
    permission_classes = [ViolationPermission, MaintenanceModePublicPermission]
    serializer_class = ListedViolationSerializerForMap
    filterset_class = ViolationFilterSet
    search_fields = ["car_plate__plate_number",]
    bbox_filter_field = "car_plate__photo__geom"
    filter_backends = ListedBaseView.filter_backends + [InBBoxFilter,]
    pagination_class = GeoJsonPagination

    def get_queryset(self):
        qs_all = Violation.objects.select_related('car_plate__photo__author', 'violation_type').prefetch_related('car_plate__photo')
        if self.request.user and self.request.user.is_authenticated:
            return qs_all.filter(car_plate__photo__author=self.request.user) | qs_all.exclude(car_plate__photo__author=self.request.user).filter(car_plate__photo__is_rejected=False, is_published=True)
        return qs_all.filter(car_plate__photo__is_rejected=False, is_published=True)


class ListedViolationViewForStaff(ListedBaseView):
    model = Violation
    serializer_class = ListedViolationSerializerForStaff
    filterset_class = ViolationFilterSetForStaff
    permission_classes = [ViolationPermissionForStaff, MaintenanceModeModeratorPermission]
    distance_ordering_filter_field = "car_plate__photo__geom"
    distance_filter_field = "car_plate__photo__geom"
    distance_filter_convert_meters = True
    filter_backends = ListedBaseView.filter_backends + [DistanceToPointOrderingFilter, ]

    def get_queryset(self):
        qs = Violation.objects.all().filter(
            Q(car_plate__photo__moderation_status=Violation.NOT_MODERATED) |
            Q(car_plate__photo__moderation_status=Violation.CONFIRMED)
        ).filter(
            Q(car_plate__moderation_status=Violation.NOT_MODERATED) |
            Q(car_plate__moderation_status=Violation.CONFIRMED)
        ).filter(
            Q(moderation_status=Violation.NOT_MODERATED) |
            Q(moderation_status=Violation.CONFIRMED)
        )
        if not self.request.user.is_superuser:
            qs = qs.exclude(car_plate__photo__author=self.request.user)
        processed_applications = Application.objects.filter(violation__in=qs).filter(is_confirmed__isnull=False)
        qs = qs.exclude(id__in=processed_applications.values_list('violation', flat=True))
        return qs.order_by('created_at')
