import os
import logging

from django.core.files.storage import FileSystemStorage
from django.db import transaction
from rest_framework import status
from rest_framework.response import Response
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from rest_framework_gis.filters import InBBoxFilter, DistanceToPointOrderingFilter

from api.decorators import idempotency_method
from api.views.base import ListedBaseView
from core.models.geo_photo import GeoPhoto
from api.filtersets.geo_photo import GeoPhotoFilterSet
from api.serializers.geo_photo import ListedGeoPhotoSerializer, CreateGeoPhotoSerializer
from api.tasks.photo_geocoding import photo_geocoding
from api.tasks.car_plate_recognition import car_plate_recognition
from api.permissions.geo_photo import GeoPhotoPermission
from api.permissions.base_permissions import MaintenanceModePublicPermission

logger = logging.getLogger(__name__)


class ListedGeoPhotoView(ListedBaseView):
    model = GeoPhoto
    serializer_class = ListedGeoPhotoSerializer
    serializer_class_for_create = CreateGeoPhotoSerializer
    filterset_class = GeoPhotoFilterSet
    queryset = GeoPhoto.objects.all()
    search_fields = []
    permission_classes = [GeoPhotoPermission, MaintenanceModePublicPermission]
    parser_classes = (JSONParser, MultiPartParser, FormParser)
    bbox_filter_field = "geom"
    distance_ordering_filter_field = "geom"
    distance_filter_field = "geom"
    distance_filter_convert_meters = True
    filter_backends = ListedBaseView.filter_backends + [InBBoxFilter, DistanceToPointOrderingFilter]

    @idempotency_method
    def post(self, request, *args, **kwargs):
        ser: CreateGeoPhotoSerializer = self.get_serializer(data=request.data)
        if ser.is_valid():
            geo_photo = ser.save(author=request.user)
            fs = FileSystemStorage(location='/container_image_exchange')
            ext = os.path.splitext(geo_photo.src_photo.name)[-1].lower()
            filename = f"{geo_photo.id}_src_photo{ext}"
            fs.save(filename, ser.validated_data['src_photo'])

            def run_tasks():
                photo_geocoding.delay(geo_photo.id)
                car_plate_recognition.delay(geo_photo.id)

            transaction.on_commit(run_tasks)

            return Response(ser.data, status=status.HTTP_201_CREATED)
        logger.error(f'Error while creating geo photo: {ser.errors}')
        return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
