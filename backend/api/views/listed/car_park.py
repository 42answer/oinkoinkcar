import logging
from rest_framework_gis.filters import InBBoxFilter, DistanceToPointOrderingFilter
from rest_framework_gis.pagination import GeoJsonPagination

from api.permissions.base_permissions import ReadOnlyPermission
from ..base import ListedBaseView
from api.serializers.car_park import ListedCarParkSerializer
from api.filtersets.car_park import CarParkFilterSet
from core.models import CarPark

logger = logging.getLogger(__name__)


class ListedCarParkView(ListedBaseView):
    model = CarPark
    serializer_class = ListedCarParkSerializer
    filterset_class = CarParkFilterSet
    permission_classes = [ReadOnlyPermission]
    filter_backends = ListedBaseView.filter_backends + [InBBoxFilter, DistanceToPointOrderingFilter]
    bbox_filter_field = "geom"
    distance_ordering_filter_field = "geom"
    distance_filter_field = "geom"
    distance_filter_convert_meters = True
    search_fields = ["address", ]
    pagination_class = GeoJsonPagination

    def get_queryset(self):
        return CarPark.objects.filter(is_active=True, is_published=True).order_by('-created_at')

    def post(self, request, *args, **kwargs):
        logger.warning(f"User {request.user} is trying to create a car park")
        return self.http_method_not_allowed(request, *args, **kwargs)
