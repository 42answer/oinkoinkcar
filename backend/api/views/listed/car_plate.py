import logging

from rest_framework_gis.filters import InBBoxFilter, DistanceToPointOrderingFilter

from ..base import ListedBaseView
from core.models.car_plate import CarPlate
from api.serializers.car_plate import ListedCarPlateSerializer
from api.filtersets.car_plate import CarPlateFilterSet
from api.permissions.car_plate import CarPlatePermission
from api.permissions.base_permissions import MaintenanceModePublicPermission

logger = logging.getLogger(__name__)


class ListedCarPlateView(ListedBaseView):
    model = CarPlate
    queryset = CarPlate.objects.all()
    serializer_class = ListedCarPlateSerializer
    filterset_class = CarPlateFilterSet
    search_fields = ["plate_number"]
    permission_classes = [CarPlatePermission, MaintenanceModePublicPermission]
    bbox_filter_field = "photo__geom"
    distance_filter_field = "photo__geom"
    distance_filter_convert_meters = True
    filter_backends = ListedBaseView.filter_backends + [InBBoxFilter, DistanceToPointOrderingFilter]

    def patch(self, request, *args, **kwargs):
        return self.http_method_not_allowed(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        logger.warning(f"User {request.user} is trying to create a car plate")
        return self.http_method_not_allowed(request, *args, **kwargs)
