import logging
from rest_framework import status
from rest_framework.response import Response

from api.decorators import idempotency_method
from api.views.base import ListedBaseView
from core.models.application import Application
from api.filtersets.application import ApplicationFilterSet
from api.serializers.application import ListedApplicationSerializerForStaff, ListedApplicationSerializerForCreate
from api.permissions.application import ApplicationPermissionForStaff
from api.permissions.base_permissions import MaintenanceModeModeratorPermission
from api.tasks.generate_and_fill_application import generate_and_fill_application

logger = logging.getLogger(__name__)


class ListedApplicationViewForStaff(ListedBaseView):
    model = Application
    serializer_class = ListedApplicationSerializerForStaff
    filterset_class = ApplicationFilterSet
    permission_classes = [ApplicationPermissionForStaff, MaintenanceModeModeratorPermission]

    def get_queryset(self):
        return Application.objects.all().filter(applicant__user=self.request.user)

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return ListedApplicationSerializerForCreate
        return ListedApplicationSerializerForStaff

    @idempotency_method
    def post(self, request, *args, **kwargs):
        ser: ListedApplicationSerializerForCreate = self.get_serializer(data=request.data)
        if ser.is_valid():
            application = ser.save()
            data = ser.data
            data['background_tasks'] = [str(generate_and_fill_application.delay(application.id)),]
            data['id'] = application.id
            return Response(data, status=status.HTTP_201_CREATED)
        logger.error(f'Error while creating application: {ser.errors}')
        return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
