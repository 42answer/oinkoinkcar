from api.views.base import ListedBaseView

from api.permissions.base_permissions import ReadOnlyPermission
from core.models import TypeOfViolation
from api.serializers.type_of_violation import TypeOfViolationSerializer
from api.filtersets.type_of_violation import TypeOfViolationFilterSet, TypeOfViolationFilterByPoint
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache


class ListedTypeOfViolationView(ListedBaseView):
    permission_classes = [ReadOnlyPermission]
    filter_backends = ListedBaseView.filter_backends + [TypeOfViolationFilterByPoint]
    model = TypeOfViolation
    search_fields = ["name", "description"]
    serializer_class = TypeOfViolationSerializer
    queryset = TypeOfViolation.objects.all()
    filterset_class = TypeOfViolationFilterSet

    @method_decorator(never_cache, name='dispatch')
    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        place_header = getattr(request, '_type_of_violation_place_header', None)
        if place_header:
            response['ooc_x_place'] = place_header
        return response
