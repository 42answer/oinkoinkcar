from api.views.base import ListedBaseView
from core.models.applicant import Applicant
from api.filtersets.applicant import ApplicantFilterSet
from api.serializers.applicant import ListedApplicantSerializerForStaff
from api.permissions.base_permissions import MaintenanceModeModeratorPermission
from api.permissions.applicant import ApplicantPermissionForStaff


class ListedApplicantViewForStaff(ListedBaseView):
    http_method_names = ['get', ]
    model = Applicant
    serializer_class = ListedApplicantSerializerForStaff
    filterset_class = ApplicantFilterSet
    permission_classes = [ApplicantPermissionForStaff, MaintenanceModeModeratorPermission]

    def get_queryset(self):
        return Applicant.objects.all().filter(user=self.request.user)
