from django_filters import FilterSet, DateFromToRangeFilter, DateTimeFilter, Filter
from django.contrib.gis.geos import Point


class BaseFilterSet(FilterSet):

    created_at_range = DateFromToRangeFilter(field_name="created_at", lookup_expr="range")
    modified_at_range = DateFromToRangeFilter(field_name="modified_at", lookup_expr="range")
    created_at = DateTimeFilter(field_name="created_at", lookup_expr="date")
    modified_at = DateTimeFilter(field_name="modified_at", lookup_expr="date")

    class Meta:
        abstract = True
        model = None
        fields = ["created_at", "modified_at", "created_at_range", "modified_at_range"]


class PointWithinPolygonFilter(Filter):
    """
    Custom filter to filter points within a polygon.
    Used to get violations for map view (violations within a polygon - view bounds).
    """

    def filter(self, queryset, value):
        if value:
            point = Point(value)
            return queryset.filter(geom__contains=point)
        return queryset
