from django_filters import CharFilter, BooleanFilter, ChoiceFilter

from .base import BaseFilterSet
from core.models import CarPark


class CarParkFilterSet(BaseFilterSet):
    is_active = BooleanFilter(field_name="is_active")
    address = CharFilter(field_name="address", lookup_expr="icontains")
    type = ChoiceFilter(field_name="type")

    class Meta:
        model = CarPark
        fields = BaseFilterSet.Meta.fields + ["is_active", "address", "type"]
