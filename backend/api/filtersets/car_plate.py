from django_filters import CharFilter, UUIDFilter, BooleanFilter

from .base import BaseFilterSet
from core.models.car_plate import CarPlate


class CarPlateFilterSet(BaseFilterSet):

    plate_number = CharFilter(field_name="plate_number", lookup_expr="icontains")
    number_confirmed = BooleanFilter(field_name="number_confirmed")
    photo = UUIDFilter(field_name="photo__id")

    class Meta:
        model = CarPlate
        fields = BaseFilterSet.Meta.fields + ["plate_number", "number_confirmed", "photo"]
