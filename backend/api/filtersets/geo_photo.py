from django_filters import CharFilter, DateTimeFromToRangeFilter, BooleanFilter, DateTimeFilter

from .base import BaseFilterSet
from core.models.geo_photo import GeoPhoto


class GeoPhotoFilterSet(BaseFilterSet):
    address = CharFilter(field_name="address", lookup_expr="icontains")
    captured_at_range = DateTimeFromToRangeFilter(field_name="captured_at", lookup_expr="range")
    captured_at = DateTimeFilter(field_name="captured_at", lookup_expr="date")
    address_confirmed = BooleanFilter(field_name="address_confirmed")

    class Meta(BaseFilterSet.Meta):
        model = GeoPhoto
        fields = BaseFilterSet.Meta.fields + ["address", "captured_at", "address_confirmed"]
