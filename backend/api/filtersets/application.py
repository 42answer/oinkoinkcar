from django_filters import CharFilter, UUIDFilter, BooleanFilter, DateTimeFromToRangeFilter

from .base import BaseFilterSet
from core.models.application import Application


class ApplicationFilterSet(BaseFilterSet):
    applicant = UUIDFilter(field_name="applicant__id")
    violation = UUIDFilter(field_name="violation__id")
    powers = UUIDFilter(field_name="powers__id")
    violation_type = UUIDFilter(field_name="violation__violation_type__id")
    place = UUIDFilter(field_name="powers__place__id")
    sent_at = DateTimeFromToRangeFilter(field_name="sent_at")
    is_violator_fined = BooleanFilter(field_name="violation__is_violator_fined")
    plate_number = CharFilter(field_name="violation__car_plate__plate_number", lookup_expr="icontains")
    text = CharFilter(field_name="text", lookup_expr="icontains")

    class Meta(BaseFilterSet.Meta):
        model = Application
        fields = BaseFilterSet.Meta.fields + ["applicant", "violation", "powers", "violation_type", "place", "sent_at", "is_violator_fined", "plate_number", "text"]
