from django_filters import UUIDFilter, BooleanFilter, CharFilter, ModelMultipleChoiceFilter

from core.models import TypeOfViolation
from .base import BaseFilterSet
from core.models.violation import Violation


class ViolationFilterSet(BaseFilterSet):
    type_confirmed = BooleanFilter(field_name="type_confirmed")
    is_published = BooleanFilter(field_name="is_published")
    application_sent = BooleanFilter(field_name="application_sent")
    is_violator_fined = BooleanFilter(field_name="is_violator_fined")
    is_author = BooleanFilter(method="get_is_author")
    violation_type = ModelMultipleChoiceFilter(field_name="violation_type__id", to_field_name="id", queryset=TypeOfViolation.objects.all())
    car_plate_number = CharFilter(field_name="car_plate__plate_number", lookup_expr="icontains")

    class Meta:
        model = Violation
        fields = BaseFilterSet.Meta.fields + ["type_confirmed", "is_published", "application_sent", "is_violator_fined", "violation_type", "car_plate"]

    def get_is_author(self, queryset, name, value):
        """
        Filter violations by author. Return only violations that were created by the current user.
        """
        if self.request and self.request.user.is_authenticated and value:
            return queryset.filter(car_plate__photo__author=self.request.user)
        return queryset


class ViolationFilterSetForStaff(ViolationFilterSet):
    car_plate_id = UUIDFilter(field_name="car_plate__id")
    moderator = UUIDFilter(field_name="moderator__id")

    class Meta:
        model = Violation
        fields = ViolationFilterSet.Meta.fields + ["moderator", "car_plate_id"]
