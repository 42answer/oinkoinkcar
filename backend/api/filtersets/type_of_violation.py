import logging
from rest_framework.exceptions import ParseError
from rest_framework.filters import BaseFilterBackend
from django_filters import FilterSet, CharFilter
from django.contrib.gis.geos import Point

from core.models import TypeOfViolation, Place, Powers

logger = logging.getLogger(__name__)


class TypeOfViolationFilterByPoint(BaseFilterBackend):
    """
    Filter type of violation, available in the place, which contains the point. If the place is not found, the queryset
    will be empty and the request will have the header "places_not_found". If the place is found, but the type of
    violation is not found, the queryset will be empty and the request will have the header "violation_types_not_found".
    """
    point_param = 'point'

    def get_filter_point(self, request, **kwargs):
        point_string = request.query_params.get(self.point_param, None)
        if not point_string:
            return None

        try:
            (x, y) = (float(n) for n in point_string.split(','))
        except ValueError:
            logger.error(f'Invalid geometry string supplied for parameter {self.point_param}')
            raise ParseError('Invalid geometry string supplied for parameter {0}'.format(self.point_param))
        p = Point(x, y, **kwargs)
        return p

    def filter_queryset(self, request, queryset, view):
        point = self.get_filter_point(request)
        if not point:
            return queryset
        places = Place.objects.filter(geom__contains=point)
        if places.exists():
            powers = Powers.objects.filter(place__in=places)
            violation_types = [power.type_of_violation for power in powers]
            result = queryset.filter(id__in=[violation_type.id for violation_type in violation_types]).distinct()
            request._type_of_violation_place_header = "violation_types_found" if result.exists() else "violation_types_not_found"
            return result
        request._type_of_violation_place_header = "places_not_found"
        return queryset.none()


class TypeOfViolationFilterSet(FilterSet):
    name = CharFilter(field_name="name", lookup_expr="icontains")

    class Meta:
        model = TypeOfViolation
        fields = ["name"]
