from django_filters import CharFilter, UUIDFilter

from .base import BaseFilterSet
from core.models import Applicant


class ApplicantFilterSet(BaseFilterSet):

    first_name = CharFilter(field_name="first_name", lookup_expr="icontains")
    last_name = CharFilter(field_name="last_name", lookup_expr="icontains")
    email = CharFilter(field_name="email", lookup_expr="icontains")
    phone_number = CharFilter(field_name="phone_number", lookup_expr="icontains")
    user = UUIDFilter(field_name="user__id")

    class Meta(BaseFilterSet.Meta):
        model = Applicant
        fields = BaseFilterSet.Meta.fields + ["first_name", "last_name", "user", "email", "phone_number"]
