# Generated by Django 4.2.9 on 2024-02-02 09:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0020_application_failed_attempts_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='last_checking_time',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Time of last check by administrator'),
        ),
        migrations.AddIndex(
            model_name='user',
            index=models.Index(fields=['last_checking_time'], name='core_user_last_ch_05416b_idx'),
        ),
    ]
