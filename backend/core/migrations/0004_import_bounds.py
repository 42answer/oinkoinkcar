import os, sys

from django.db import migrations
from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import GEOSGeometry

from config.settings import PRODUCTION


def import_places_data(apps, schema_editor):
    Place = apps.get_model('core', 'Place')

    migration_dir = os.path.dirname(os.path.realpath(__file__))

    if 'test' in sys.argv or not PRODUCTION:
        geojson_file = os.path.join(migration_dir, 'data_to_import/saratov-region-bmr-balakovo.geojson')
    else:
        geojson_file = os.path.join(migration_dir, 'data_to_import/russia-osm-bounds.geojson')

    ds = DataSource(geojson_file)

    for layer in ds:
        for feature in layer:
            name = feature.get('local_name') or feature.get('name')
            geom = GEOSGeometry(feature.geom.wkt, srid=4326)

            Place.objects.create(name=name, geom=geom)


def remove_all_places_data(apps, schema_editor):
    Place = apps.get_model('core', 'Place')
    Place.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_alter_geophoto_author'),
    ]

    operations = [
        migrations.RunPython(import_places_data, remove_all_places_data),
    ]
