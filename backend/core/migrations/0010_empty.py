from django.db import migrations, models


# Empty migration for numbering consistency
class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_alter_geophoto_options_alter_typeofviolation_options_and_more'),
    ]

    operations = [
    ]
