import os
import uuid
from django.core.files.images import ImageFile
from config.storages import S3Storage
from django.db import migrations
from django.db import models


def import_new_data_for_balakovo(apps, schema_editor):
    TypeOfViolation = apps.get_model('core', 'TypeOfViolation')
    Place = apps.get_model('core', 'Place')
    bmr = Place.objects.get(name='Балаковский район')

    migration_folder = os.path.dirname(os.path.realpath(__file__))
    images_folder = os.path.join(migration_folder, 'data_to_import', 'images')

    wrong_parking_vt = TypeOfViolation.objects.create(
        name='Неправильная парковка',
        description='Нарушение (общее), которое выражается в размещение транспортных средств в нарушение  статьи 12.1 ПДД РФ.',
        image=ImageFile(open(os.path.join(images_folder, 'wrong_parking_illustration.jpg'), 'rb'), name=f"{str(uuid.uuid4())}_wrong_parking_illustration.jpg"),
        icon=ImageFile(open(os.path.join(images_folder, 'wrong_parking_icon.png'), 'rb'), name=f"{str(uuid.uuid4())}_wrong_parking_icon.png"),
        order=6,
        use_by_default=True
    )

    Rule = apps.get_model('core', 'Rule')

    wrong_parking_rule = Rule.objects.create(
        name='Правила остановки и стоянки ТС общее, Россия',
        description='Согласно статьи 12.1 ПДД РФ, утвержденных Постановлением Правительства РФ от 23.10.1993 N 1090 (ред. от 02.06.2023) "О Правилах дорожного движения" остановка и стоянка транспортных средств разрешаются на правой стороне дороги на обочине, а при ее отсутствии - на проезжей части у ее края и в случаях, установленных пунктом 12.2 Правил, - на тротуаре.',
        statement='разместил транспортное средство с целью стоянки за пределами автомобильной дороги и её обочины, тем самым нарушил статью 12.1 ПДД РФ',
        is_active=True
    )

    Liability = apps.get_model('core', 'Liability')

    wrong_parking_liability = Liability.objects.create(
        name='Ответственность за нарушение правил остановки или стоянки транспортных средств (общая)',
        description='Часть 1 Статьи 12.19. КоАП РФ предусматривает административную ответственность за нарушение правил остановки или стоянки транспортных средств в виде предупреждения или наложения административного штрафа.',
        request='Прошу привлечь к административной ответственности, предусмотренной частью 1 Статьи 12.19. КоАП РФ собственника/водителя транспортного средства, государственный регистрационный знак: ',
        is_active=True,
        min_fine=0,
        max_fine=500
    )

    Authority = apps.get_model('core', 'Authority')
    Powers = apps.get_model('core', 'Powers')

    gibdd_bmr = Authority.objects.get(
        name='ОГИБДД МУ МВД России "Балаковское" Саратовской области',
    )

    Powers.objects.create(
        name='Полномочия ОГИБДД МУ МВД России "Балаковское" Саратовской области привлекать к ответственности за нарушение правил остановки или стоянки транспортных средств (общая)',
        description='ОГИБДД МУ МВД России "Балаковское" Саратовской области уполномочен привлекать к ответственности '
                    'водителей транспортных средств, нарушивших пункт 12.1. ПДД РФ за размещение транспортных средств '
                    'с целью стоянки за пределами автомобильной дороги и её обочины, тем самым нарушив статью 12.1 ПДД РФ.',
        is_active=True,
        rule=wrong_parking_rule,
        liability=wrong_parking_liability,
        place=bmr,
        authority=gibdd_bmr,
        type_of_violation=wrong_parking_vt
    )


def delete_basic_data(apps, schema_editor):
    Powers = apps.get_model('core', 'Powers')
    Powers.objects.filter(name='Полномочия ОГИБДД МУ МВД России "Балаковское" Саратовской области привлекать к ответственности за нарушение правил остановки или стоянки транспортных средств (общая)').delete()

    TypeOfViolation = apps.get_model('core', 'TypeOfViolation')
    tov = TypeOfViolation.objects.filter(name='Неправильная парковка').first()
    if tov is not None:
        S3Storage.delete(tov.image.name)
        S3Storage.delete(tov.icon.name)
        tov.delete()

    Rule = apps.get_model('core', 'Rule')
    Rule.objects.filter(name='Правила остановки и стоянки ТС общее, Россия').delete()

    Liability = apps.get_model('core', 'Liability')
    Liability.objects.filter(name='Ответственность за нарушение правил остановки или стоянки транспортных средств (общая)').delete()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0028_remove_carpark_contact_info'),
    ]

    operations = [
        migrations.AddField(
            model_name='typeofviolation',
            name='use_by_default',
            field=models.BooleanField(default=False, verbose_name='Use by default'),
        ),
        migrations.AddIndex(
            model_name='typeofviolation',
            index=models.Index(fields=['use_by_default'], name='core_typeof_use_by__21ea4c_idx'),
        ),
        migrations.RunPython(import_new_data_for_balakovo, delete_basic_data)
    ]
