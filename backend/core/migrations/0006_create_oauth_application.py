from django.db import migrations

from config import settings


def create_app(apps, schema_editor):
    Application = apps.get_model('oauth2_provider', 'Application')
    new_application = Application(client_type="confidential",
                                  authorization_grant_type="password",
                                  name="core",
                                  client_id=settings.OAUTH_CLIENT_ID_KEY,
                                  client_secret=settings.OAUTH_CLIENT_SECRET_KEY)
    new_application.save()


def reverse_create_app(apps, schema_editor):
    Application = apps.get_model('oauth2_provider', 'Application')
    app = Application.objects.filter(name="core").first()
    app.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('oauth2_provider', '0007_application_post_logout_redirect_uris'),
        ('core', '0005_set_places_parents'),
    ]

    operations = [
        migrations.RunPython(create_app, reverse_code=reverse_create_app),
    ]