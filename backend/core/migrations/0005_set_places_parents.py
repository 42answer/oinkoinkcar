from django.db import migrations
from django.contrib.gis.db.models.functions import Area


def assign_parents(apps, schema_editor):
    Place = apps.get_model('core', 'Place')

    places = Place.objects.all().annotate(area=Area('geom')).order_by('area')

    for place in places:
        centroid = place.geom.centroid

        containing_places = Place.objects.filter(geom__contains=centroid).exclude(id=place.id).annotate(area=Area('geom')).order_by('area')

        if containing_places.exists():
            smallest_parent = None
            for candidate_parent in containing_places:
                intersection_area = place.geom.intersection(candidate_parent.geom).area
                child_area = place.geom.area
                overlap_threshold = 0.90
                if intersection_area / child_area >= overlap_threshold:
                    smallest_parent = candidate_parent
                    break
            if smallest_parent:
                place.parent = smallest_parent
                place.save()


def unassign_parents(apps, schema_editor):
    Place = apps.get_model('core', 'Place')

    Place.objects.all().update(parent=None)


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_import_bounds'),
    ]

    operations = [
        migrations.RunPython(assign_parents, unassign_parents),
    ]
