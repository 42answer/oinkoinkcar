import os
import uuid
from django.core.files.images import ImageFile
from django.db import migrations
from config.storages import S3Storage
from config.settings import PRODUCTION, FAKE_EMAIL_AUTHORITY_1, FAKE_EMAIL_AUTHORITY_2


def import_basic_data_for_balakovo(apps, schema_editor):
    Place = apps.get_model('core', 'Place')
    balakovo = Place.objects.get(name='городское поселение Балаково')
    bmr = Place.objects.get(name='Балаковский район')

    TypeOfViolation = apps.get_model('core', 'TypeOfViolation')
    migration_folder = os.path.dirname(os.path.realpath(__file__))
    images_folder = os.path.join(migration_folder, 'data_to_import', 'images')

    car_on_the_lawn_vt = TypeOfViolation.objects.create(
        name='Стоянка на газоне',
        description='Нарушение, которое выражается в размещение транспортных средств на газоне или иной территории, '
                    'занятой зелеными насаждениями, а равно благоустроенной и предназначенной для озеленения территории, '
                    'за исключением техники, связанной с производством работ по содержанию территории, и ремонту объектов, '
                    'инженерных сетей и коммуникаций, расположенных на указанных территориях.',
        image=ImageFile(open(os.path.join(images_folder, 'car_on_the_lawn_illustration.jpg'), 'rb'), name=f"{str(uuid.uuid4())}_car_on_the_lawn_illustration.jpg"),
        icon=ImageFile(open(os.path.join(images_folder, 'car_on_the_lawn_icon.png'), 'rb'), name=f"{str(uuid.uuid4())}_car_on_the_lawn_icon.png"),
        order=1
    )

    car_on_the_playground_vt = TypeOfViolation.objects.create(
        name='Стоянка на дет. площад.',
        description='Нарушение, которое выражается в размещение транспортных средств на спортивных и детских площадках, '
                    'за исключением техники, связанной с производством работ по содержанию территории, и ремонту объектов, '
                    'инженерных сетей и коммуникаций, расположенных на указанных территориях.',
        image=ImageFile(open(os.path.join(images_folder, 'car_on_the_playground_illustration.jpg'), 'rb'), name=f"{str(uuid.uuid4())}_car_on_the_playground_illustration.jpg"),
        icon=ImageFile(open(os.path.join(images_folder, 'car_on_the_playground_icon.png'), 'rb'), name=f"{str(uuid.uuid4())}_car_on_the_playground_icon.png"),
        order=4
    )

    car_on_the_crosswalk_vt = TypeOfViolation.objects.create(
        name='Стоянка у пеш. перехода',
        description='Нарушение, которое выражается в остановке на пешеходном переходе или возле него, ближе пяти метров от него (ПДД - Остановка запрещается на пешеходных переходах и ближе 5 м перед ними).',
        image=ImageFile(open(os.path.join(images_folder, 'car_on_the_crosswalk_illustration.jpg'), 'rb'), name=f"{str(uuid.uuid4())}_car_on_the_crosswalk_illustration.jpg"),
        icon=ImageFile(open(os.path.join(images_folder, 'car_on_the_crosswalk_icon.png'), 'rb'), name=f"{str(uuid.uuid4())}_car_on_the_crosswalk_icon.png"),
        order=3
    )

    car_on_the_sidewalk_vt = TypeOfViolation.objects.create(
        name='Стоянка на тротуаре',
        description='Нарушение, которое выражается в размещение транспортных средств на тротуаре, при отсутствии знака "Парковка (парковочное место)" с одной из табличек 8.6.2, 8.6.3 и 8.6.6 - 8.6.9. '
                    'Стоянка на тротуаре не является нарушением если имеется знак "Парковка (парковочное место)" с одной из табличек "Способ постановки транспортного средства на стоянку", '
                    'разрешающих размещение автомобиля на тротуаре.',
        image=ImageFile(open(os.path.join(images_folder, 'car_on_the_sidewalk_illustration.jpg'), 'rb'), name=f"{str(uuid.uuid4())}_car_on_the_sidewalk_illustration.jpg"),
        icon=ImageFile(open(os.path.join(images_folder, 'car_on_the_sidewalk_icon.png'), 'rb'), name=f"{str(uuid.uuid4())}_car_on_the_sidewalk_icon.png"),
        order=2
    )

    Rule = apps.get_model('core', 'Rule')

    car_on_the_lawn_rule = Rule.objects.create(
        name='Запрет размещения автомобиля на газоне, Саратовская область',
        description='Согласно части 15 статьи 8.2 Закона Саратовской области от 29 июля 2009 г. № 104-ЗСО '
                    '«ОБ АДМИНИСТРАТИВНЫХ ПРАВОНАРУШЕНИЯХ НА ТЕРРИТОРИИ САРАТОВСКОЙ ОБЛАСТИ» не допускается размещение транспортных '
                    'средств на газоне или иной территории, занятой зелеными насаждениями, а равно благоустроенной и '
                    'предназначенной для озеленения территории, за исключением техники, связанной с производством работ '
                    'по содержанию территории, занятой зелеными насаждениями, и ремонту объектов, инженерных сетей и '
                    'коммуникаций, расположенных на указанных территориях.',
        statement='разместил транспортное средство на территории, занятой зелеными насаждениями, тем самым нарушил часть 15 статьи 8.2 Закона Саратовской области от 29 июля 2009 г. № 104-ЗСО «ОБ АДМИНИСТРАТИВНЫХ ПРАВОНАРУШЕНИЯХ НА ТЕРРИТОРИИ САРАТОВСКОЙ ОБЛАСТИ»',
        is_active=True
    )

    car_on_the_playground_rule = Rule.objects.create(
        name='Запрет размещения автомобиля на спортивной/детской площадке, Саратовская область',
        description='Согласно части 15 статьи 8.2 Закона Саратовской области от 29 июля 2009 г. № 104-ЗСО '
                    '«ОБ АДМИНИСТРАТИВНЫХ ПРАВОНАРУШЕНИЯХ НА ТЕРРИТОРИИ САРАТОВСКОЙ ОБЛАСТИ» не допускается размещение транспортных '
                    'средств на спортивных и детских площадках, за исключением техники, связанной с производством работ '
                    'по содержанию детских и спортивных площадок и ремонту объектов, инженерных сетей и коммуникаций, '
                    'расположенных на указанных территориях.',
        statement='разместил транспортное средство на спортивной/детской площадке, тем самым нарушил часть 15 статьи 8.2 Закона Саратовской области от 29 июля 2009 г. № 104-ЗСО «ОБ АДМИНИСТРАТИВНЫХ ПРАВОНАРУШЕНИЯХ НА ТЕРРИТОРИИ САРАТОВСКОЙ ОБЛАСТИ»',
        is_active=True
    )

    car_on_the_crosswalk_rule = Rule.objects.create(
        name='Правила остановки возле пешеходного перехода, Россия',
        description='Согласно части 4 статьи 12.4 ПДД РФ, утвержденных Постановлением Правительства РФ от 23.10.1993 N 1090 (ред. от 02.06.2023) '
                    '"О Правилах дорожного движения" остановка запрещается на пешеходных переходах и ближе 5 м перед ними.',
        statement='остановил транспортное средство ближе пяти метров перед пешеходным переходом, тем самым нарушил часть 4 статьи 12.4 ПДД РФ',
        is_active=True
    )

    car_on_the_sidewalk_rule = Rule.objects.create(
        name='Правило, стоянки на тротуаре, Россия',
        description='Согласно пункту 12.2. ПДД РФ, утвержденных Постановлением Правительства РФ от 23.10.1993 N 1090 (ред. от 02.06.2023) '
                    '"О Правилах дорожного движения" стоянка на краю тротуара, граничащего с проезжей частью, разрешается только легковым '
                    'автомобилям, мотоциклам и мопедам в местах, обозначенных знаком "Парковка (парковочное место)" с одной из табличек 8.6.2, 8.6.3 и 8.6.6 - 8.6.9.',
        statement='разместил транспортное средство на тротуаре, при отсутствии знака "Парковка (парковочное место)" с одной из табличек 8.6.2, 8.6.3 и 8.6.6 - 8.6.9., '
                  'тем самым нарушил пункт 12.2. ПДД РФ',
        is_active=True
    )

    Liability = apps.get_model('core', 'Liability')

    car_on_the_lawn_liability = Liability.objects.create(
        name='Автомобиль на газоне, Саратовская область',
        description='Часть 15 статьи 8.2 Закона Саратовской области от 29 июля 2009 г. № 104-ЗСО '
                    '«ОБ АДМИНИСТРАТИВНЫХ ПРАВОНАРУШЕНИЯХ НА ТЕРРИТОРИИ САРАТОВСКОЙ ОБЛАСТИ» предусматривает ответственность '
                    'за указанное нарушение в виде предупреждения или наложения административного штрафа.',
        request='Прошу привлечь к административной ответственности, предусмотренной частью 15 статьи 8.2 Закона Саратовской области от 29 июля 2009 г. № 104-ЗСО «ОБ АДМИНИСТРАТИВНЫХ ПРАВОНАРУШЕНИЯХ НА ТЕРРИТОРИИ САРАТОВСКОЙ ОБЛАСТИ» '
                'собственника транспортного средства, государственный регистрационный знак: ',
        is_active=True,
        min_fine=0,
        max_fine=100000
    )

    car_on_the_playground_liability = Liability.objects.create(
        name='Автомобиль на спортивной/детской площадке, Саратовская область',
        description='Часть 15 статьи 8.2 Закона Саратовской области от 29 июля 2009 г. № 104-ЗСО '
                    '«ОБ АДМИНИСТРАТИВНЫХ ПРАВОНАРУШЕНИЯХ НА ТЕРРИТОРИИ САРАТОВСКОЙ ОБЛАСТИ» предусматривает ответственность '
                    'за указанное нарушение в виде предупреждения или наложения административного штрафа.',
        request='Прошу привлечь к административной ответственности, предусмотренной частью 15 статьи 8.2 Закона Саратовской '
                'области от 29 июля 2009 г. № 104-ЗСО «ОБ АДМИНИСТРАТИВНЫХ ПРАВОНАРУШЕНИЯХ НА ТЕРРИТОРИИ САРАТОВСКОЙ ОБЛАСТИ» '
                'собственника транспортного средства, государственный регистрационный знак: ',
        is_active=True,
        min_fine=0,
        max_fine=100000
    )

    car_on_the_crosswalk_liability = Liability.objects.create(
        name='Автомобиль на пешеходном переходе, Россия',
        description='Часть 3 Статьи 12.19. КоАП РФ предусматривает административную ответственность за остановку или стоянку транспортных '
                    'средств на пешеходном переходе и ближе 5 метров перед ним в виде административного штрафа.',
        request='Прошу привлечь к административной ответственности, предусмотренной частью 3 Статьи 12.19. КоАП РФ '
                'собственника/водителя транспортного средства, государственный регистрационный знак: ',
        is_active=True,
        min_fine=1000,
        max_fine=1000
    )

    car_on_the_sidewalk_liability = Liability.objects.create(
        name='Ответственность за остановку на тротуаре в России',
        description='Часть 3 Статьи 12.19. КоАП РФ предусматривает административную ответственность за нарушение правил '
                    'остановки или стоянки транспортных средств на тротуаре в виде административного штрафа.',
        request='Прошу привлечь к административной ответственности, предусмотренной частью 3 Статьи 12.19. КоАП РФ '
                'собственника/водителя транспортного средства, государственный регистрационный знак: ',
        is_active=True,
        min_fine=1000,
        max_fine=1000
    )

    Authority = apps.get_model('core', 'Authority')
    administration_bmr = Authority.objects.create(
        name='Администрация Балаковского муниципального района',
        dative_name='В администрацию Балаковского муниципального района Саратовской области',
        address='413840, Саратовская область, г. Балаково, ул. Трнавская, 12',
        recipient='Глава Балаковского муниципального района Грачев Сергей Евгеньевич',
        dative_recipient='Главе Балаковского муниципального района Грачеву Сергею Евгеньевичу',
        email='admbal@bk.ru' if PRODUCTION else FAKE_EMAIL_AUTHORITY_1,
        feedback_page='https://www.admbal.ru/internet-priemnaya/',
        application_method='by_email',
        gosuslugi_number='6440100010000000373'
    )

    Powers = apps.get_model('core', 'Powers')
    Powers.objects.create(
        name='Полномочия администрации БМР привлекать к ответственности за парковку на газоне',
        description='Административная комиссия БМР при администрации уполномочена привлекать к ответственности '
                    'собственников транспортных средств, нарушивших часть 15 статьи 8.2 Закона Саратовской области '
                    'от 29 июля 2009 г. № 104-ЗСО «ОБ АДМИНИСТРАТИВНЫХ ПРАВОНАРУШЕНИЯХ НА ТЕРРИТОРИИ САРАТОВСКОЙ '
                    'ОБЛАСТИ» за размещение транспортных средств на газоне или иной территории, занятой зелеными '
                    'насаждениями, а равно благоустроенной и предназначенной для озеленения территории, за исключением '
                    'техники, связанной с производством работ по содержанию территории, занятой зелеными насаждениями, '
                    'и ремонту объектов, инженерных сетей и коммуникаций, расположенных на указанных территориях.',
        is_active=True,
        rule=car_on_the_lawn_rule,
        liability=car_on_the_lawn_liability,
        place=balakovo,
        authority=administration_bmr,
        type_of_violation=car_on_the_lawn_vt
    )

    Powers.objects.create(
        name='Полномочия администрации БМР привлекать к ответственности за парковку на спортивной/детской площадке',
        description='Административная комиссия БМР при администрации уполномочена привлекать к ответственности '
                    'собственников транспортных средств, нарушивших часть 15 статьи 8.2 Закона Саратовской области '
                    'от 29 июля 2009 г. № 104-ЗСО «ОБ АДМИНИСТРАТИВНЫХ ПРАВОНАРУШЕНИЯХ НА ТЕРРИТОРИИ САРАТОВСКОЙ '
                    'ОБЛАСТИ» за размещение транспортных средств на спортивных и детских площадках, за исключением '
                    'техники, связанной с производством работ по содержанию детских и спортивных площадок и ремонту '
                    'объектов, инженерных сетей и коммуникаций, расположенных на указанных территориях.',
        is_active=True,
        rule=car_on_the_playground_rule,
        liability=car_on_the_playground_liability,
        place=balakovo,
        authority=administration_bmr,
        type_of_violation=car_on_the_playground_vt
    )

    gibdd_bmr = Authority.objects.create(
        name='ОГИБДД МУ МВД России "Балаковское" Саратовской области',
        dative_name='В ОГИБДД МУ МВД России "Балаковское" Саратовской области',
        address='413840, Саратовская обл., Балаково г., Дорожная ул., 11',
        recipient='Начальник ОГИБДД МУ МВД России "Балаковское" Саратовской области, Корнилов Алексей Иванович',
        dative_recipient='Начальнику ОГИБДД МУ МВД России "Балаковское" Саратовской области, Корнилову Алексею Ивановичу',
        email='gibdd.blk.srt@mvd.ru' if PRODUCTION else FAKE_EMAIL_AUTHORITY_2, # https://www.gosuslugi.ru/structure/348921291
        feedback_page='https://xn--80aabg3bexb.64.xn--b1aew.xn--p1ai/request_main', # балаково.64.мвд.рф
        application_method='by_email',
        gosuslugi_number='348921291'
    )

    Powers.objects.create(
        name='Полномочия ОГИБДД МУ МВД России "Балаковское" Саратовской области привлекать к ответственности за парковку на пешеходном переходе',
        description='ОГИБДД МУ МВД России "Балаковское" Саратовской области уполномочен привлекать к ответственности '
                    'водителей транспортных средств, нарушивших часть 4 статьи 12.4 ПДД РФ за остановку на пешеходном '
                    'переходе или возле него, ближе пяти метров от него.',
        is_active=True,
        rule=car_on_the_crosswalk_rule,
        liability=car_on_the_crosswalk_liability,
        place=bmr,
        authority=gibdd_bmr,
        type_of_violation=car_on_the_crosswalk_vt
    )

    Powers.objects.create(
        name='Полномочия ОГИБДД МУ МВД России "Балаковское" Саратовской области привлекать к ответственности за парковку на тротуаре',
        description='ОГИБДД МУ МВД России "Балаковское" Саратовской области уполномочен привлекать к ответственности '
                    'водителей транспортных средств, нарушивших пункт 12.2. ПДД РФ за размещение транспортных средств '
                    'на тротуаре, при отсутствии знака "Парковка (парковочное место)" с одной из табличек 8.6.2, 8.6.3 и '
                    '8.6.6 - 8.6.9.',
        is_active=True,
        rule=car_on_the_sidewalk_rule,
        liability=car_on_the_sidewalk_liability,
        place=bmr,
        authority=gibdd_bmr,
        type_of_violation=car_on_the_sidewalk_vt
    )


def delete_basic_data(apps, schema_editor):
    Powers = apps.get_model('core', 'Powers')
    Powers.objects.all().delete()

    TypeOfViolation = apps.get_model('core', 'TypeOfViolation')
    for tov in TypeOfViolation.objects.all():
        S3Storage.delete(tov.image.name)
        S3Storage.delete(tov.icon.name)
        tov.delete()

    Rule = apps.get_model('core', 'Rule')
    Rule.objects.all().delete()

    Liability = apps.get_model('core', 'Liability')
    Liability.objects.all().delete()

    Authority = apps.get_model('core', 'Authority')
    Authority.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_alter_typeofviolation_options_and_more'),
    ]

    operations = [
        migrations.RunPython(import_basic_data_for_balakovo, delete_basic_data),
    ]
