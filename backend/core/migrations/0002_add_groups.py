from django.db import migrations


def apply_migration(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    banned_group = Group(name='Banned')
    banned_group.save()

    moderator_group = Group(name='Moderator')
    moderator_group.save()


def revert_migration(apps, schema_editor):
    Group = apps.get_model('auth', 'Group')
    Group.objects.filter(name='Banned').delete()
    Group.objects.filter(name='Moderator').delete()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(apply_migration, revert_migration)
    ]
