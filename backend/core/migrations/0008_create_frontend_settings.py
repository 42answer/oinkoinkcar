from django.db import migrations


def create_frontend_user_settings_for_existing_users(apps, schema_editor):
    User = apps.get_model('core', 'User')
    FrontendUserSettings = apps.get_model('core', 'FrontendUserSettings')

    for user in User.objects.all():
        FrontendUserSettings.objects.get_or_create(user=user)


def delete_frontend_user_settings_for_existing_users(apps, schema_editor):
    User = apps.get_model('core', 'User')
    FrontendUserSettings = apps.get_model('core', 'FrontendUserSettings')

    for user in User.objects.all():
        FrontendUserSettings.objects.filter(user=user).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_frontendusersettings'),
    ]

    operations = [
        migrations.RunPython(create_frontend_user_settings_for_existing_users, delete_frontend_user_settings_for_existing_users),
    ]
