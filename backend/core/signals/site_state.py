import logging
from django.db.models.signals import pre_save
from django.dispatch import receiver
from core.models.site_state import SiteState
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@receiver(pre_save, sender=SiteState)
def enforce_single_instance(sender, instance, **kwargs):
    existing_instance = SiteState.objects.first()

    if existing_instance and existing_instance != instance:
        logger.error("Only one instance of SiteState is allowed.")
        raise ValidationError(_("Only one instance of SiteState is allowed."))
