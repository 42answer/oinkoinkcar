import os

from django.dispatch import receiver
from config import storages
from django.db.models.signals import pre_delete
from core.models import *


@receiver(pre_delete, sender=Applicant)
def applicant_delete(sender, instance, **kwargs):
    if instance.sign_scan and storages.S3PrivateStorage.exists(instance.sign_scan.name):
        storages.S3PrivateStorage.delete(instance.sign_scan.name)


@receiver(pre_delete, sender=Application)
def application_delete(sender, instance, **kwargs):
    if instance.pdf_file and storages.S3PrivateStorage.exists(instance.pdf_file.name):
        storages.S3PrivateStorage.delete(instance.pdf_file.name)


@receiver(pre_delete, sender=GeoPhoto)
def geophoto_delete(sender, instance, **kwargs):
    ext = os.path.splitext(instance.src_photo.name)[-1].lower()
    file_path = f'/container_image_exchange/{instance.id}_src_photo{ext}'
    if os.path.exists(file_path):
        os.remove(file_path)
    if instance.src_photo and storages.S3Storage.exists(instance.src_photo.name):
        storages.S3Storage.delete(instance.src_photo.name)
    if instance.compressed_photo and storages.S3Storage.exists(instance.compressed_photo.name):
        storages.S3Storage.delete(instance.compressed_photo.name)


@receiver(pre_delete, sender=TypeOfViolation)
def type_of_violation_delete(sender, instance, **kwargs):
    if instance.image and storages.S3Storage.exists(instance.image.name):
        storages.S3Storage.delete(instance.image.name)
    if instance.icon and storages.S3Storage.exists(instance.icon.name):
        storages.S3Storage.delete(instance.icon.name)


@receiver(pre_delete, sender=CarPark)
def car_park_delete(sender, instance, **kwargs):
    if instance.photo and storages.S3Storage.exists(instance.photo.name):
        storages.S3Storage.delete(instance.photo.name)
