from django.db.models.signals import post_save
from django.dispatch import receiver
from core.models.user import User
from core.models.frontend_user_settings import FrontendUserSettings


@receiver(post_save, sender=User)
def create_frontend_user_settings(sender, instance, created, **kwargs):
    if created:
        FrontendUserSettings.objects.create(user=instance)
