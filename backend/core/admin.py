from django.contrib.gis import admin
from django.contrib.gis import forms
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from .models import User
from .models import Applicant
from .models import Application
from .models import Authority
from .models import GeoPhoto
from .models import Place
from .models import Powers
from .models import ResponseOfAuthority
from .models import Violation
from .models import CarPlate
from .models import Rule
from .models import Liability
from .models import TypeOfViolation
from .models import SiteState
from .models import TileLayer
from .models import CarPark
from .models import Template
from .models import ParkState
from .models import Person
from .models import Organization

from .widgets import LocalOSMWidget


class GeoAdmin(admin.GISModelAdmin):
    gis_widget = LocalOSMWidget
    gis_widget_kwargs = {'attrs': {'default_lon': 47.819667, 'default_lat': 52.018424, 'default_zoom': 10}}


@admin.register(Place)
class PlaceAdmin(GeoAdmin):
    list_display = ("name", "parent")
    search_fields = ("name",)
    autocomplete_fields = ("parent",)


@admin.register(GeoPhoto)
class GeoPhotoAdmin(GeoAdmin):
    list_display = ("captured_at", "address", "author", "initial_type_of_violation", "moderation_status")
    search_fields = ("address", )
    autocomplete_fields = ("author", "moderator", "initial_type_of_violation")
    list_filter = ("captured_at", "address_confirmed", "is_recognized", "is_violation_selected", "is_geocoded", "initial_type_of_violation", "moderation_status")


@admin.register(CarPark)
class CarParkAdmin(GeoAdmin):
    list_display = ("internal_id", "address", "is_published", "organization")
    list_filter = ("created_at", "modified_at", "is_published", "is_active", "type")
    search_fields = ("address", "internal_id")
    autocomplete_fields = ("organization",)


@admin.register(ParkState)
class ParkStateAdmin(admin.ModelAdmin):
    list_display = ("total_spaces", "free_spaces",)
    list_filter = ()
    search_fields = ("free_spaces", "park",)
    autocomplete_fields = ("park",)


admin.site.site_header = _('Oinkoinkcar admin panel.')
admin.site.site_title = _('Oinkoinkcar admin panel.')


@admin.register(Template)
class TemplateAdmin(admin.ModelAdmin):
    list_filter = ("created_at", "modified_at")
    search_fields = ("id", )


@admin.register(Powers)
class PowersAdmin(admin.ModelAdmin):
    list_display = ("name", "description", "is_active", "place", "authority", )
    list_filter = ("created_at", "modified_at", "is_active", "type_of_violation")
    search_fields = ("name", "description", )
    autocomplete_fields = ("place", "liability", "authority", "rule", "type_of_violation", "template")


@admin.register(Applicant)
class ApplicantAdmin(admin.ModelAdmin):
    list_display = ("first_name", "patronymic", "last_name", "email", "phone_number", "user")
    search_fields = ("first_name", "patronymic", "last_name", "registration_address", "identity_document", "email", "phone_number")
    autocomplete_fields = ("user", )
    list_filter = ("created_at", "modified_at")


@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    list_display = ("applicant", "violation", "powers", "is_confirmed", "sent_at", "pdf_file", "rejected_reason")
    search_fields = ("applicant__user__email", "violation__car_plate__plate_number")
    autocomplete_fields = ("applicant", "violation", "powers")
    list_filter = ("created_at", "modified_at", "is_confirmed", "sent_at")


@admin.register(Authority)
class AuthorityAdmin(admin.ModelAdmin):
    list_display = ("name", "address", "recipient", "email", "feedback_page", "application_method")
    search_fields = ("name", "dative_name", "address", "recipient", "dative_recipient", "email", "feedback_page", "gosuslugi_number")
    list_filter = ("created_at", "modified_at", "application_method")


@admin.register(ResponseOfAuthority)
class ResponseOfAuthorityAdmin(admin.ModelAdmin):
    list_display = ("response_type", "authority", "application")
    search_fields = ("response_type", "authority__name", "application__text")
    autocomplete_fields = ("authority", "application")
    list_filter = ("created_at", "modified_at", "response_type")


@admin.register(TypeOfViolation)
class TypeOfViolationAdmin(admin.ModelAdmin):
    list_display = ("name", "description", "order")
    list_filter = ("created_at", "modified_at")
    search_fields = ("name", "description", )


@admin.register(Violation)
class ViolationAdmin(admin.ModelAdmin):
    list_display = ("car_plate", "is_published", "violation_type", "moderation_status", "moderator")
    list_filter = ("moderation_status", "created_at", "modified_at", "is_published", "is_violator_fined", "type_confirmed")
    search_fields = ("car_plate__plate_number", )
    autocomplete_fields = ("violation_type", "car_plate", "moderator")


@admin.register(CarPlate)
class CarPlateAdmin(admin.ModelAdmin):
    list_display = ("plate_number", "moderation_status", "number_confirmed", "photo",)
    list_filter = ("number_confirmed", "moderation_status", "created_at", "modified_at", )
    search_fields = ("plate_number", )
    autocomplete_fields = ("moderator", "photo")


@admin.register(Rule)
class RuleAdmin(admin.ModelAdmin):
    list_display = ("name", "description", "is_active")
    list_filter = ("created_at", "modified_at", "is_active")
    search_fields = ("name", "description", "statement")


@admin.register(Liability)
class LiabilityAdmin(admin.ModelAdmin):
    list_display = ("name", "description", "request", "is_active", "min_fine", "max_fine")
    list_filter = ("created_at", "modified_at", "is_active")
    search_fields = ("name", "description", "request")


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ("name", "phone_number")
    search_fields = ("name", "phone_number")
    autocomplete_fields = ("user",)


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    list_display = ("name", )
    search_fields = ("name", )
    autocomplete_fields = ("head", "contact_person", "pub_contact_person")


@admin.register(User)
class OinkUserAdmin(UserAdmin):
    autocomplete_fields = ()
    search_fields = ('first_name', 'last_name', 'email',)
    list_filter = ('is_staff', 'last_login', 'date_joined', 'initialized')
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'initialized')

    fieldsets = (
        (None, {'fields': (
                'username', 'password')}),
        (_('Personal info'), {'fields': (
                'first_name', 'last_name', 'email', 'initialized')}),
        (_('Permissions'), {'fields': (
                'is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': (
                'last_login', 'date_joined', 'modified_at',)}),)

    add_fieldsets = (
        (None, {'fields': (
                'username', 'password1', 'password2')}),
        ('Personal info', {'fields': (
                'first_name', 'last_name', 'email', 'initialized')}),
        ('Permissions', {'fields': (
                'is_active', 'is_staff', 'is_superuser', 'groups')}),)


admin.site.register(SiteState)
admin.site.register(TileLayer)
