import uuid
from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from core.models import User


class BaseModel(models.Model):
    id = models.UUIDField(default=uuid.uuid4, unique=True, editable=False, primary_key=True, help_text=_("Unique ID (UUID4)"))
    created_at = models.DateTimeField(default=now, verbose_name=_("Time of creation"))
    modified_at = models.DateTimeField(blank=True, null=True, verbose_name=_("Time of change"))

    class Meta:
        abstract = True
        ordering = ["-created_at"]
        indexes = [
            models.Index(fields=["created_at"]),
            models.Index(fields=["modified_at"]),
        ]


class ModeratedMixin(models.Model):
    NOT_MODERATED = 'not_moderated'
    CONFIRMED = 'confirmed'
    REJECTED_REASON_BLURRED = 'blurred'
    REJECTED_REASON_FACES = 'faces'
    REJECTED_REASON_NO_CAR = 'no_car'
    REJECTED_REASON_CAR_NOT_FIT = 'car_not_fit'
    REJECTED_REASON_CAR_PLATE_NOT_READABLE = 'car_plate_not_readable'
    REJECTED_INTERNATIONAL_CAR_PLATE = 'international_car_plate'
    REJECTED_REASON_VIOLATION_TYPE_NOT_CONFIRMED = 'violation_type_not_confirmed'
    REJECTED_REASON_VIOLATION_ALREADY_EXISTS = 'violation_already_exists'
    REJECTED_REASON_GEOLOCATION = 'geolocation_error'
    REJECTED_REASON_FLOOD = 'flood'
    REJECTED_REASON_ADULT = 'adult'
    REJECTED_REASON_OTHER = 'other'
    REJECTED_REASON_TIMEOUT = 'timeout'
    MODERATION_STATUS_CHOICES = (
        (NOT_MODERATED, _('Not moderated')),
        (CONFIRMED, _('Confirmed')),
        (REJECTED_REASON_BLURRED, _('Blurred')),
        (REJECTED_REASON_FACES, _('Faces')),
        (REJECTED_REASON_NO_CAR, _('No car')),
        (REJECTED_REASON_CAR_NOT_FIT, _('Car not fit')),
        (REJECTED_REASON_CAR_PLATE_NOT_READABLE, _('Car plate not readable')),
        (REJECTED_INTERNATIONAL_CAR_PLATE, _('International car plate')),
        (REJECTED_REASON_VIOLATION_TYPE_NOT_CONFIRMED, _('Violation type not confirmed')),
        (REJECTED_REASON_VIOLATION_ALREADY_EXISTS, _('Violation already exists')),
        (REJECTED_REASON_GEOLOCATION, _('Geolocation error')),
        (REJECTED_REASON_FLOOD, _('Flood')),
        (REJECTED_REASON_ADULT, _('Adult')),
        (REJECTED_REASON_OTHER, _('Other')),
        (REJECTED_REASON_TIMEOUT, _('Timeout')),
    )
    moderation_status = models.CharField(max_length=255, choices=MODERATION_STATUS_CHOICES, default=NOT_MODERATED, verbose_name=_("Moderation status"))
    moderator = models.ForeignKey(User, related_name='%(class)s_moderator', on_delete=models.CASCADE, verbose_name=_("Moderator"), null=True, blank=True, help_text=_("Moderator who confirmed or rejected object"))

    class Meta:
        abstract = True
        indexes = [
            models.Index(fields=["moderation_status"]),
        ]
