import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _


IDEMPOTENCY_RECOVERY_POINT_STARTED = 'started'
IDEMPOTENCY_RECOVERY_POINT_FINISHED = 'finished'


class RecoveryPoint(models.TextChoices):
    STARTED = IDEMPOTENCY_RECOVERY_POINT_STARTED, IDEMPOTENCY_RECOVERY_POINT_STARTED
    FINISHED = IDEMPOTENCY_RECOVERY_POINT_FINISHED, IDEMPOTENCY_RECOVERY_POINT_FINISHED


class IdempotencyKey(models.Model):
    id = models.UUIDField(default=uuid.uuid4, unique=True, editable=False, primary_key=True, help_text=_("Unique ID (UUID4)"))
    idempotency_key = models.UUIDField(unique=True)

    created_at = models.DateTimeField(auto_now_add=True)
    last_run_at = models.DateTimeField(auto_now=True)
    locked_at = models.DateTimeField(null=True)

    request_method = models.CharField(max_length=16)
    request_params = models.TextField()
    request_path = models.CharField(max_length=255)
    request_digest = models.BinaryField(max_length=32)

    response_code = models.PositiveSmallIntegerField(null=True)
    response_body = models.TextField(null=True)

    recovery_point = models.CharField(max_length=64, default=RecoveryPoint.STARTED.value, choices=RecoveryPoint.choices)

    class Meta:
        verbose_name = _("Idempotency Key")
        verbose_name_plural = _("Idempotency Keys")
        indexes = [
            models.Index(fields=["idempotency_key"]),
        ]
