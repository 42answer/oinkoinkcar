from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel
from .person import Person


class Organization(BaseModel):

    objects = models.Manager()

    name = models.CharField(max_length=128, verbose_name=_("Name"))
    head = models.ForeignKey(Person, on_delete=models.SET_NULL, null=True, blank=True, verbose_name=_("Head of organization"), related_name="head_of_organization")
    contact_person = models.ForeignKey(Person, on_delete=models.SET_NULL, blank=True, null=True, verbose_name=_("Contact person for admins"), related_name="contact_person")
    pub_contact_person = models.ForeignKey(Person, on_delete=models.SET_NULL, blank=True, null=True, verbose_name=_("Public contact person"), related_name="public_contact_person")

    class Meta:
        verbose_name = _("Organization")
        verbose_name_plural = _("Organizations")
        indexes = [
            models.Index(fields=["name"]),
            ] + BaseModel.Meta.indexes

    def __str__(self):
        return self.name
