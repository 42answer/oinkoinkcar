import uuid
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.timezone import now
from .user import User


class FrontendUserSettings(models.Model):
    STYLE_LIGHT = 'light'
    STYLE_DARK = 'dark'
    STYLE_CHOICES = (
        (STYLE_LIGHT, _('Light')),
        (STYLE_DARK, _('Dark'))
    )

    objects = models.Manager()

    id = models.UUIDField(default=uuid.uuid4, unique=True, editable=False, primary_key=True, help_text=_("Unique ID (UUID4)"))
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name=_("User"), related_name='settings', unique=True)
    modified_at = models.DateTimeField(blank=True, null=True, verbose_name=_("Time of change"))
    created_at = models.DateTimeField(default=now, verbose_name=_("Time of creation"))
    theme = models.CharField(max_length=32, verbose_name=_("Theme"), choices=STYLE_CHOICES, default=STYLE_LIGHT)
    show_info_messages = models.BooleanField(default=True, verbose_name=_("Show info messages"))
    show_warning_messages = models.BooleanField(default=True, verbose_name=_("Show warning messages"))
    show_error_messages = models.BooleanField(default=True, verbose_name=_("Show error messages"))
    show_debug_messages = models.BooleanField(default=False, verbose_name=_("Show debug messages"))

    class Meta:
        verbose_name = _("User settings")
        verbose_name_plural = _("User settings")

    def __str__(self):
        return str(self.user)
