from django.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel
from .rule import Rule
from .liability import Liability
from .authority import Authority
from .place import Place
from .type_of_violation import TypeOfViolation
from .template import Template


class Powers(BaseModel):

    objects = models.Manager()

    name = models.CharField(max_length=256, verbose_name=_("Name"))
    description = models.CharField(max_length=1024, verbose_name=_("Description"))
    is_active = models.BooleanField(default=True, verbose_name=_("Is active"))
    rule = models.ForeignKey(Rule, on_delete=models.PROTECT, verbose_name=_("Rule"))
    liability = models.ForeignKey(Liability, on_delete=models.PROTECT, verbose_name=_("Liability"))
    place = models.ForeignKey(Place, on_delete=models.PROTECT, verbose_name=_("Place"))
    authority = models.ForeignKey(Authority, on_delete=models.PROTECT, verbose_name=_("Authority"))
    type_of_violation = models.ForeignKey(TypeOfViolation, on_delete=models.PROTECT, verbose_name=_("Type of violation"))
    template = models.ForeignKey(Template, on_delete=models.SET_NULL, verbose_name=_("Application template"), null=True, blank=True)

    class Meta:
        verbose_name = _("Power")
        verbose_name_plural = _("Powers")
        ordering = BaseModel.Meta.ordering
        indexes = BaseModel.Meta.indexes + [
            models.Index(fields=["name"]),
            models.Index(fields=["description"]),
            models.Index(fields=["is_active"]),
        ]

    def __str__(self):
        return self.name
