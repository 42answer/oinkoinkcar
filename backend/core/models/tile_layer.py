from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel


class TileLayer(BaseModel):

    objects = models.Manager()

    name = models.CharField(max_length=128, verbose_name=_("Name"))
    layer_url = models.CharField(max_length=255, verbose_name=_("Layer URL"))

    class Meta:
        verbose_name = _("Tile layer")
        verbose_name_plural = _("Tile layers")

    def __str__(self):
        return f'{self.name} | {self.layer_url}'
