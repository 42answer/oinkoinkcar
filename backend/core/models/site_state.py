import uuid
from django.db import models
from django.utils.translation import gettext_lazy as _
from .user import User
from .tile_layer import TileLayer


class SiteState(models.Model):
    MODE_NORMAL = 'normal'
    MODE_MAINTENANCE = 'maintenance'

    PUBLIC_MODE_CHOICES = (
        (MODE_NORMAL, _('Normal')),
        (MODE_MAINTENANCE, _('Maintenance'))
    )

    MODERATION_MODE_CHOICES = (
        (MODE_NORMAL, _('Normal')),
        (MODE_MAINTENANCE, _('Maintenance'))
    )

    objects = models.Manager()

    id = models.UUIDField(default=uuid.uuid4, unique=True, editable=False, primary_key=True, help_text=_("Unique ID (UUID4)"))
    modified_at = models.DateTimeField(blank=True, null=True, verbose_name=_("Time of change"))
    modified_by = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, verbose_name=_("Modified by"), related_name='site_state_modified_by')
    public_mode = models.CharField(max_length=32, verbose_name=_("Public part operating mode"), choices=PUBLIC_MODE_CHOICES, default=MODE_NORMAL)
    message_for_public = models.TextField(blank=True, null=True, verbose_name=_("Message for public"))
    moderation_mode = models.CharField(max_length=32, verbose_name=_("Moderators part operating mode"), choices=MODERATION_MODE_CHOICES, default=MODE_NORMAL)
    message_for_moderators = models.TextField(blank=True, null=True, verbose_name=_("Message for moderators"))
    tile_layer = models.ForeignKey(TileLayer, on_delete=models.SET_NULL, blank=True, null=True, verbose_name=_("Tile layer"))

    class Meta:
        verbose_name = _("Site state")
        verbose_name_plural = _("Site state")
