from django.db import models
from django.utils.translation import gettext_lazy as _
from config.storages import S3Storage

from .base_model import BaseModel


class TypeOfViolation(BaseModel):
    objects = models.Manager()

    name = models.CharField(max_length=256, verbose_name=_("Name"))
    description = models.CharField(max_length=1024, verbose_name=_("Description"), null=True, blank=True)
    image = models.ImageField(storage=S3Storage, verbose_name=_("Image for illustration"), null=True, blank=True)
    icon = models.ImageField(storage=S3Storage, verbose_name=_("Icon"), null=True, blank=True)
    order = models.IntegerField(default=0, verbose_name=_("Order"))
    use_by_default = models.BooleanField(default=False, verbose_name=_("Use by default"))

    class Meta(BaseModel.Meta):
        verbose_name = _('Type of violation')
        verbose_name_plural = _('Types of violation')
        ordering = ["order"] + BaseModel.Meta.ordering
        indexes = BaseModel.Meta.indexes + [
            models.Index(fields=["name"]),
            models.Index(fields=["order"]),
            models.Index(fields=["use_by_default"]),
        ]

    def __str__(self):
        return self.name
