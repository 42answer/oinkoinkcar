from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel
from .car_park import CarPark


class ParkState(BaseModel):

    objects = models.Manager()

    total_spaces = models.PositiveIntegerField(verbose_name=_("Total parking spaces"), null=True, blank=True)
    free_spaces = models.PositiveIntegerField(verbose_name=_("Free parking spaces"), null=True, blank=True)
    monthly_rent = models.PositiveIntegerField(verbose_name=_("Monthly rent"), null=True, blank=True)
    park = models.ForeignKey(CarPark, on_delete=models.CASCADE, verbose_name=_("Car park"))

    class Meta:
        verbose_name = _("Car park state")
        verbose_name_plural = _("Car park states")
        indexes = [
            models.Index(fields=["total_spaces"]),
            models.Index(fields=["free_spaces"]),
            ] + BaseModel.Meta.indexes

    def __str__(self):
        return f"total spaces: {self.total_spaces}, free: {self.free_spaces}"
