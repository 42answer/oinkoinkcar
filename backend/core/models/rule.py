from django.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel


class Rule(BaseModel):

    objects = models.Manager()

    name = models.CharField(max_length=256, verbose_name=_("Name"))
    description = models.CharField(max_length=1024, verbose_name=_("Description"), help_text=_("According to the law, it is forbidden to do so-and-so"))
    statement = models.CharField(max_length=1024, verbose_name=_("Statement"), help_text=_("Thereby violated the provision of clause ... of the law ..."))
    is_active = models.BooleanField(default=True, verbose_name=_("Active"))

    class Meta:
        verbose_name = _("Rule")
        verbose_name_plural = _("Rules")
        ordering = BaseModel.Meta.ordering
        indexes = BaseModel.Meta.indexes + [
            models.Index(fields=["name"]),
            models.Index(fields=["description"]),
            models.Index(fields=["statement"]),
            models.Index(fields=["is_active"]),
        ]

    def __str__(self):
        return self.name
