from django.core.validators import RegexValidator, EmailValidator
from django.db import models
from django.utils.translation import gettext_lazy as _
from config.storages import S3PrivateStorage

from .base_model import BaseModel
from .user import User


class Applicant(BaseModel):

    objects = models.Manager()
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=_("The phone number must be entered in the format: '+999999999'. Up to 15 digits are allowed."))

    first_name = models.CharField(max_length=64, verbose_name=_("First name"))
    patronymic = models.CharField(max_length=64, verbose_name=_("Patronymic"))
    last_name = models.CharField(max_length=64, verbose_name=_("Last name"))
    registration_address = models.CharField(max_length=1024, verbose_name=_("Registration address"))
    identity_document = models.CharField(max_length=1024, verbose_name=_("Identity document"))
    email = models.EmailField(verbose_name=_("Email"), unique=True, validators=[EmailValidator(message=_("Please enter a valid email address."))])
    phone_number = models.CharField(validators=[phone_regex], max_length=15, blank=True, null=True, verbose_name=_("Phone number"))
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=_("User"))
    sign_scan = models.ImageField(storage=S3PrivateStorage, verbose_name=_("Sign scan"), null=True, blank=True)

    class Meta:
        verbose_name = _("Applicant")
        verbose_name_plural = _("Applicants")
        ordering = BaseModel.Meta.ordering
        indexes = BaseModel.Meta.indexes + [
            models.Index(fields=["first_name"]),
            models.Index(fields=["patronymic"]),
            models.Index(fields=["last_name"]),
            models.Index(fields=["registration_address"]),
            models.Index(fields=["identity_document"]),
            models.Index(fields=["email"]),
            models.Index(fields=["phone_number"]),
        ]

    def __str__(self):
        return f"{self.first_name} {self.patronymic} {self.last_name} | {self.email} | {self.id}"
