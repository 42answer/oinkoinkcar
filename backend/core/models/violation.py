from django.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel, ModeratedMixin
from .car_plate import CarPlate
from .type_of_violation import TypeOfViolation


class Violation(BaseModel, ModeratedMixin):

    objects = models.Manager()

    is_published = models.BooleanField(verbose_name=_("Is published"), default=False)
    is_violator_fined = models.BooleanField(verbose_name=_("Is violator fined"), null=True, blank=True)
    violation_type = models.ForeignKey(TypeOfViolation, on_delete=models.CASCADE, verbose_name=_("Type of violation"))
    type_confirmed = models.BooleanField(null=True, blank=True, verbose_name=_("Type of violation confirmed by moderator"))
    car_plate = models.ForeignKey(CarPlate, on_delete=models.CASCADE, verbose_name=_("Car plate"))

    class Meta(BaseModel.Meta, ModeratedMixin.Meta):
        verbose_name = _("Violation")
        verbose_name_plural = _("Violations")
        ordering = BaseModel.Meta.ordering
        indexes = BaseModel.Meta.indexes + [
            models.Index(fields=["is_published"]),
            models.Index(fields=["is_violator_fined"]),
            models.Index(fields=["type_confirmed"]),
        ] + ModeratedMixin.Meta.indexes

    def __str__(self):
        return f"{self.car_plate} {self.violation_type} {self.created_at}"
