from django.db import models
from django.utils.translation import gettext_lazy as _
from config.storages import S3PrivateStorage

from .base_model import BaseModel
from core.models import Applicant


class Application(BaseModel):

    REJECTED_REASON_FLOOD = 'flood'
    REJECTED_REASON_ADULT = 'adult'
    REJECTED_REASON_OTHER = 'other'
    REJECTED_REASON_TIMEOUT = 'timeout'
    REJECTED_REASON_ENCODING_ERROR = 'encoding_error'
    REJECTED_REASON_MODERATION_ERROR = 'moderation_error'
    REJECTED_REASON_LAYOUT_ERROR = 'layout_error'
    REJECTED_REASON_APPLICANT_ERROR = 'applicant_error'
    REJECTED_REASON_AUTHORITY_ERROR = 'authority_error'
    REJECTED_REASON_CHOICES = (
        (REJECTED_REASON_FLOOD, _('Flood')),
        (REJECTED_REASON_ADULT, _('Adult')),
        (REJECTED_REASON_OTHER, _('Other')),
        (REJECTED_REASON_TIMEOUT, _('Timeout')),
        (REJECTED_REASON_ENCODING_ERROR, _('Encoding error')),
        (REJECTED_REASON_MODERATION_ERROR, _('Moderation error')),
        (REJECTED_REASON_LAYOUT_ERROR, _('Layout error')),
        (REJECTED_REASON_APPLICANT_ERROR, _('Error in applicant data')),
        (REJECTED_REASON_AUTHORITY_ERROR, _('Error in authority data')),
    )

    objects = models.Manager()

    text = models.TextField(null=True, blank=True, verbose_name=_("Text"))
    applicant = models.ForeignKey(Applicant, on_delete=models.CASCADE, verbose_name=_("Applicant"))
    violation = models.ForeignKey("Violation", on_delete=models.CASCADE, verbose_name=_("Violation"))
    powers = models.ForeignKey("Powers", on_delete=models.CASCADE, verbose_name=_("Powers"))
    is_confirmed = models.BooleanField(null=True, blank=True, verbose_name=_("Confirmed by applicant"))
    sent_at = models.DateTimeField(null=True, blank=True, verbose_name=_("Time of sending"))
    pdf_file = models.FileField(storage=S3PrivateStorage, verbose_name=_("PDF file"), null=True, blank=True)
    rejected_reason = models.CharField(max_length=255, verbose_name=_("Rejected reason"), null=True, blank=True, choices=REJECTED_REASON_CHOICES)
    failed_attempts = models.PositiveIntegerField(default=0, verbose_name=_("Number of failed attempts"))
    last_failure_attempt_at = models.DateTimeField(null=True, blank=True, verbose_name=_("Time of last failed attempt"))
    task_id = models.CharField(max_length=36, verbose_name=_("Task ID"), null=True, blank=True)

    class Meta:
        verbose_name = _("Application")
        verbose_name_plural = _("Applications")
        ordering = BaseModel.Meta.ordering
        indexes = BaseModel.Meta.indexes + [
            models.Index(fields=["is_confirmed"]),
            models.Index(fields=["sent_at"]),
            models.Index(fields=["rejected_reason"]),
            models.Index(fields=["failed_attempts"]),
            models.Index(fields=["last_failure_attempt_at"]),
            models.Index(fields=["task_id"]),
        ]
