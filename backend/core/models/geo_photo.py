from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel, ModeratedMixin
from .type_of_violation import TypeOfViolation
from config.storages import S3Storage
from .user import User


class GeoPhoto(BaseModel, ModeratedMixin):

    objects = models.Manager()

    geom = models.PointField(srid=4326, verbose_name=_("Point on the map"))
    src_photo = models.ImageField(storage=S3Storage, verbose_name=_("Photo"))
    compressed_photo = models.ImageField(storage=S3Storage, verbose_name=_("Compressed photo"), null=True, blank=True)
    panorama_shot = models.ImageField(storage=S3Storage, verbose_name=_("Panorama shot (for an overview of the place)"), null=True, blank=True)
    address = models.CharField(max_length=512, verbose_name=_("Address"), null=True, blank=True)
    captured_at = models.DateTimeField(verbose_name=_("The date the photo was captured in the user's device"))
    address_confirmed = models.BooleanField(null=True, blank=True, verbose_name=_("Address confirmed by moderator"))
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=_("Author"), null=True, blank=True)
    accuracy_horizontal = models.FloatField(blank=True, null=True, verbose_name=_("Horizontal accuracy"))
    compass_angle = models.FloatField(blank=True, null=True, verbose_name=_("Compass angle"))
    is_recognized = models.BooleanField(default=False, verbose_name=_("Plates is recognized"))
    is_violation_selected = models.BooleanField(default=False, verbose_name=_("Violation has been selected"))
    is_geocoded = models.BooleanField(default=False, verbose_name=_("Photo has been geocoded"))
    initial_type_of_violation = models.ForeignKey(TypeOfViolation, on_delete=models.SET_NULL, verbose_name=_("Initial type of violation"), null=True, blank=True, help_text=_("The type of violation that was selected by the user when uploading the photo"))
    is_rejected = models.BooleanField(default=False, verbose_name=_("Photo has been rejected"))
    nearest_parking_distance = models.IntegerField(default=None, null=True, blank=True, verbose_name=_("Distance to the nearest parking"))

    def __str__(self):
        return f"{self.captured_at.strftime('%d.%m.%Y %H:%M:%S')} | {self.address}"

    class Meta(BaseModel.Meta, ModeratedMixin.Meta):
        verbose_name = _("Photo")
        verbose_name_plural = _("Photos")
        ordering = BaseModel.Meta.ordering
        indexes = BaseModel.Meta.indexes + [
            models.Index(fields=["address"]),
            models.Index(fields=["captured_at"]),
            models.Index(fields=["address_confirmed"]),
            models.Index(fields=["is_rejected"]),
            models.Index(fields=["is_recognized"]),
            models.Index(fields=["is_violation_selected"]),
            models.Index(fields=["is_geocoded"]),
            models.Index(fields=["initial_type_of_violation"]),
        ] + ModeratedMixin.Meta.indexes
