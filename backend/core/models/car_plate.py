from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel, ModeratedMixin
from .geo_photo import GeoPhoto


class CarPlate(BaseModel, ModeratedMixin):

    objects = models.Manager()

    plate_number = models.CharField(max_length=16, verbose_name=_("Name"))
    b_box = models.JSONField(verbose_name=_("Bounding box"))
    photo = models.ForeignKey(GeoPhoto, on_delete=models.CASCADE, verbose_name=_("Photo"))
    number_confirmed = models.BooleanField(null=True, blank=True, verbose_name=_("Plate number confirmed by moderator"))

    class Meta(BaseModel.Meta, ModeratedMixin.Meta):
        verbose_name = _("Car plate")
        verbose_name_plural = _("Car plates")
        indexes = [
            models.Index(fields=["plate_number"]),
            models.Index(fields=["number_confirmed"]),
        ] + BaseModel.Meta.indexes + ModeratedMixin.Meta.indexes

    def __str__(self):
        return self.plate_number
