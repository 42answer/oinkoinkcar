from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel
from config.storages import S3Storage


class CarPark(BaseModel):
    FLAT_PARKING = "flat_parking"
    ROTARY = "rotary"
    MULTI_LEVEL = "multi_level"
    COMBINED = "combined"
    UNDEFINED = "undefined"
    PARKING_TYPES = [
        (FLAT_PARKING, _('Flat parking')),
        (ROTARY, _('Rotary')),
        (MULTI_LEVEL, _('Multi-level')),
        (COMBINED, _('Combined')),
        (UNDEFINED, _('Undefined'))
    ]

    objects = models.Manager()

    is_active = models.BooleanField(default=True, verbose_name=_("Is active"))
    geom = models.PolygonField(verbose_name=_("Parking lot boundaries on the map"))
    address = models.CharField(max_length=1024, verbose_name=_("Address"), blank=True, null=True)
    type = models.CharField(max_length=16, choices=PARKING_TYPES, default=UNDEFINED)
    organization = models.ForeignKey("Organization", on_delete=models.PROTECT, verbose_name=_("Organization"), null=True, blank=True)
    photo = models.ImageField(storage=S3Storage, verbose_name=_("Photo"), null=True, blank=True)
    internal_id = models.CharField(max_length=64, verbose_name=_("Internal ID"), null=True, blank=True)
    is_published = models.BooleanField(default=False, verbose_name=_("Is published"))

    class Meta:
        verbose_name = _("Car parking")
        verbose_name_plural = _("Car parkings")
        indexes = [
            models.Index(fields=["is_active"]),
            models.Index(fields=["is_published"]),
        ] + BaseModel.Meta.indexes

    def __str__(self):
        return _("Сar parking at: %(address)s") % {"address": self.address} if self.address else _("Car parking: %(park_id)s") % {"park_id": self.id}
