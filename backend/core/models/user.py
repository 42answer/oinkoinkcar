import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from django.utils.timezone import now


class User(AbstractUser):
    GOOGLE = "google"
    VKONTAKTE = "vkontakte"
    OAUTH_PROVIDERS = [
        (GOOGLE, _("Google")),
        (VKONTAKTE, _("VK")),
    ]

    id = models.UUIDField(default=uuid.uuid4, unique=True, editable=False, primary_key=True, help_text=_("Unique ID (UUID4)"))
    modified_at = models.DateTimeField(blank=True, null=True, verbose_name=_("Time of change"))
    created_at = models.DateTimeField(default=now, verbose_name=_("Time of creation"))
    oauth_provider = models.CharField(default=GOOGLE, max_length=32, choices=OAUTH_PROVIDERS, verbose_name=_("OAuth provider"))
    provider_id = models.CharField(blank=True, null=True, max_length=256, verbose_name=_("Provider id"))
    initialized = models.BooleanField(default=False, verbose_name=_("Initialized"))
    last_checking_time = models.DateTimeField(blank=True, null=True, verbose_name=_("Time of last check by administrator"))

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")
        unique_together = ['oauth_provider', 'provider_id']
        indexes = [
            models.Index(fields=['username']),
            models.Index(fields=['email']),
            models.Index(fields=['modified_at']),
            models.Index(fields=['created_at']),
            models.Index(fields=['last_checking_time']),
            models.Index(fields=['oauth_provider', 'provider_id']),
        ]
