from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel


class Place(BaseModel):
    objects = models.Manager()

    name = models.CharField(max_length=256, verbose_name=_("Name"))
    parent = models.ForeignKey("Place", blank=True, null=True, on_delete=models.CASCADE, verbose_name=_("Parent place"))
    geom = models.MultiPolygonField(srid=4326, help_text=_("Boundaries of the territory (multipolygon)"), verbose_name=_("Boundaries"))

    class Meta:
        verbose_name = _("Place")
        verbose_name_plural = _("Places")
        ordering = BaseModel.Meta.ordering
        indexes = BaseModel.Meta.indexes + [
            models.Index(fields=["name"]),
        ]

    def __str__(self):
        return self.name
