from django.contrib.gis.db import models
from django.core.validators import EmailValidator
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel


class Authority(BaseModel):
    IN_PERSON = 'in_person'
    BY_MAIL = 'by_mail'
    BY_EMAIL = 'by_email'
    THROUGH_POS_ON_GOSUSLUGI = 'through_pos_on_gosuslugi'
    THROUGH_FEEDBACK_FORM = 'through_feedback_form'
    THROUGH_GOSUSLUGI_API = 'through_gosuslugi_api'
    APPLICATION_METHODS = (
        (IN_PERSON, _('In person')),
        (BY_MAIL, _('By mail')),
        (BY_EMAIL, _('By email')),
        (THROUGH_POS_ON_GOSUSLUGI, _('Through the POS of Gosuslugi')),
        (THROUGH_GOSUSLUGI_API, _('Through the Gosuslugi API')),
    )
    objects = models.Manager()

    name = models.CharField(max_length=128, verbose_name=_("Name"))
    dative_name = models.CharField(max_length=128, verbose_name=_("Name (in dative case)"))
    address = models.CharField(max_length=512, verbose_name=_("Address"))
    recipient = models.CharField(max_length=128, verbose_name=_("Name of recipient"))
    dative_recipient = models.CharField(max_length=128, verbose_name=_("Name of recipient (in dative case)"))
    email = models.EmailField(verbose_name=_("Email"), unique=True, validators=[EmailValidator(message=_("Please enter a valid email address."))], null=True, blank=True)
    feedback_page = models.URLField(verbose_name=_("Page with feedback form"), blank=True, null=True)
    application_method = models.CharField(max_length=64, choices=APPLICATION_METHODS, verbose_name=_('Application Method'))
    gosuslugi_number = models.CharField(max_length=64, null=True, blank=True, verbose_name=_('Special number on gosuslugi portal.'), help_text=_('Example: 6440100010000000373. Used in urls, like thisL: https://do.gosuslugi.ru/orgs/6440100010000000373/'))

    class Meta:
        verbose_name = _("Authority")
        verbose_name_plural = _("Authorities")
        ordering = BaseModel.Meta.ordering
        indexes = BaseModel.Meta.indexes + [
            models.Index(fields=["name"]),
            models.Index(fields=["dative_name"]),
            models.Index(fields=["address"]),
            models.Index(fields=["recipient"]),
            models.Index(fields=["dative_recipient"]),
        ]

    def __str__(self):
        return self.name
