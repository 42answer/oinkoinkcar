from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel
from .user import User


class Person(BaseModel):

    objects = models.Manager()

    name = models.CharField(max_length=128, verbose_name=_("Name"), null=True, blank=True)
    phone_number = models.CharField(max_length=128, blank=True, null=True, verbose_name=_("Phone number"))
    notes = models.TextField(verbose_name=_("Notes"), null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, verbose_name=_("User"))

    class Meta:
        verbose_name = _("Person")
        verbose_name_plural = _("Persons")
        indexes = [
            models.Index(fields=["name"]),
            models.Index(fields=["phone_number"])
        ] + BaseModel.Meta.indexes

    def __str__(self):
        return f'{self.name}, {self.phone_number}' if self.phone_number else self.name