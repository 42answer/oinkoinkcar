from django.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel
from .authority import Authority
from .application import Application


class ResponseOfAuthority(BaseModel):
    REJECT = 'reject'
    SATISFACTION = 'satisfaction'
    EXTENSION = 'extension'
    REDIRECT = 'redirect'
    RESPONSE_TYPE = [
        (REJECT, _('Reject')),
        (SATISFACTION, _('Satisfaction')),
        (EXTENSION, _('Extension of processing time')),
        (REDIRECT, _('Redirect')),
    ]
    objects = models.Manager()

    response_type = models.CharField(max_length=16, verbose_name=_("Type of response"), choices=RESPONSE_TYPE)
    authority = models.ForeignKey(Authority, on_delete=models.CASCADE, verbose_name=_("Authority"))
    application = models.ForeignKey(Application, on_delete=models.CASCADE, verbose_name=_("Application"))

    class Meta:
        verbose_name = _("Response of the Authority")
        verbose_name_plural = _("Responses of the Authority")
        ordering = BaseModel.Meta.ordering
        indexes = BaseModel.Meta.indexes + [
            models.Index(fields=["response_type"]),
        ]

    def __str__(self):
        return f"{self.response_type} {self.authority} {self.application}"
