from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel


class Template(BaseModel):
    template = models.TextField(verbose_name=_("Template"))

    class Meta:
        verbose_name = _("Template")
        verbose_name_plural = _("Templates")

    def __str__(self):
        return f"Template {self.id}"
