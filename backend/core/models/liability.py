from django.db import models
from django.utils.translation import gettext_lazy as _

from .base_model import BaseModel


class Liability(BaseModel):
    objects = models.Manager()

    name = models.CharField(max_length=256, verbose_name=_("Name"))
    description = models.CharField(max_length=1024, verbose_name=_("Description"), help_text=_("Paragraph 3 of Article 4 of Federal Law 5 of 11/11/2005 'On Administrative Liability' for misconduct No. 6 provides for liability in the form of a fine of 7"))
    request = models.CharField(max_length=1024, verbose_name=_("Request"), help_text=_("I ask you to bring to administrative responsibility, provided for by paragraph .... article ...."))
    is_active = models.BooleanField(default=True, verbose_name=_("Is active"))
    min_fine = models.PositiveIntegerField(verbose_name=_("Minimum fine"))
    max_fine = models.PositiveIntegerField(verbose_name=_("Maximum fine"))

    class Meta:
        verbose_name = _("Liability")
        verbose_name_plural = _("Liabilities")
        ordering = BaseModel.Meta.ordering
        indexes = BaseModel.Meta.indexes + [
            models.Index(fields=["name"]),
            models.Index(fields=["description"]),
            models.Index(fields=["is_active"]),
            models.Index(fields=["request"]),
        ]

    def __str__(self):
        return self.name
