import os
import uuid
import piexif
import logging

from PIL import Image
from datetime import datetime
from django.core.files.images import ImageFile
from django.core.files.storage import FileSystemStorage
from django.contrib.gis.geos import Point
from django.core.management.base import BaseCommand

from core.models import GeoPhoto, TypeOfViolation, User
from api.tasks.photo_geocoding import photo_geocoding
from api.tasks.car_plate_recognition import car_plate_recognition


codec = 'latin-1'


def exif_to_tag(exif_dict):
    exif_tag_dict = {}
    thumbnail = exif_dict.pop('thumbnail')
    exif_tag_dict['thumbnail'] = thumbnail.decode(codec)

    for ifd in exif_dict:
        exif_tag_dict[ifd] = {}
        for tag in exif_dict[ifd]:
            try:
                element = exif_dict[ifd][tag].decode(codec)

            except AttributeError:
                element = exif_dict[ifd][tag]

            exif_tag_dict[ifd][piexif.TAGS[ifd][tag]["name"]] = element

    return exif_tag_dict


def get_exif_data(image_path):
    image = Image.open(image_path)
    exif_dict = piexif.load(image.info.get('exif'))
    exif_dict = exif_to_tag(exif_dict)
    return exif_dict


class Command(BaseCommand):
    help = '''Import photos from ./photo_to_import/<TypeOfViolation_name>/*.jpg, create GeoPhoto with selected 
    TypeOfViolation, author = superuser, geom, captured_at from EXIF'''

    def handle(self, *args, **kwargs):
        super_user = User.objects.filter(is_superuser=True).first()

        current_folder = os.path.dirname(os.path.realpath(__file__))
        photo_to_import_folder = os.path.join(current_folder, 'photo_to_import')

        folder_names_in_photo_to_import = [folder_name for folder_name in os.listdir(photo_to_import_folder) if
                                           os.path.isdir(os.path.join(photo_to_import_folder, folder_name))]

        for folder_name in folder_names_in_photo_to_import:
            type_of_violation = TypeOfViolation.objects.filter(name=folder_name).first()
            if not type_of_violation:
                print(f'Error: TypeOfViolation with name "{folder_name}" not found')
                continue

            print(f'Importing photos from folder "{folder_name}"')

            folder_path = os.path.join(photo_to_import_folder, folder_name)
            file_names_in_folder = [file_name for file_name in os.listdir(folder_path) if
                                    os.path.isfile(os.path.join(folder_path, file_name))]

            for file_name in file_names_in_folder:
                if not file_name.lower().endswith('.jpg'):
                    print(f'Info: File "{file_name}" is not JPG. Skipping.')
                    continue
                try:
                    image_path = os.path.join(folder_path, file_name)
                    exif_data = get_exif_data(image_path)

                    geotagging = exif_data.get('GPS', None)

                    creation_time = None
                    lat, lon = None, None

                    if 'Exif' in exif_data and 'DateTimeOriginal' in exif_data['Exif']:
                        creation_time_txt = exif_data['Exif']['DateTimeOriginal']
                        creation_time = datetime.strptime(creation_time_txt, '%Y:%m:%d %H:%M:%S')

                    if geotagging is not None and 'GPSLatitude' in geotagging and 'GPSLongitude' in geotagging:
                        lat = geotagging['GPSLatitude'][0][0] / geotagging['GPSLatitude'][0][1] + \
                                geotagging['GPSLatitude'][1][0] / geotagging['GPSLatitude'][1][1] / 60 + \
                                geotagging['GPSLatitude'][2][0] / geotagging['GPSLatitude'][2][1] / 3600

                        lon = geotagging['GPSLongitude'][0][0] / geotagging['GPSLongitude'][0][1] + \
                                geotagging['GPSLongitude'][1][0] / geotagging['GPSLongitude'][1][1] / 60 + \
                                geotagging['GPSLongitude'][2][0] / geotagging['GPSLongitude'][2][1] / 3600

                        if geotagging['GPSLatitudeRef'] == 'S':
                            lat = -lat

                        if geotagging['GPSLongitudeRef'] == 'W':
                            lon = -lon

                    if not creation_time or not lat or not lon:
                        print(f'Error: Creation time or lat or lon not found in EXIF')
                        continue
                    photo = ImageFile(open(image_path, 'rb'), name=f"{str(uuid.uuid4())}.jpg")
                    geo_photo = GeoPhoto.objects.create(
                        src_photo=photo,
                        initial_type_of_violation=type_of_violation,
                        author=super_user,
                        geom=Point(lon, lat),
                        captured_at=creation_time
                    )
                    fs = FileSystemStorage(location='/container_image_exchange')
                    ext = os.path.splitext(geo_photo.src_photo.name)[-1].lower()
                    filename = f"{geo_photo.id}_src_photo{ext}"
                    fs.save(filename, photo)

                    print(f'GeoPhoto with src_photo "{file_name}" created')
                    tasks = [str(photo_geocoding.delay(geo_photo.id)), str(car_plate_recognition.delay(geo_photo.id))]
                    print(f'Background tasks created: {tasks}')

                except ValueError as e:
                    print(f'Error: {e}')
                    continue
