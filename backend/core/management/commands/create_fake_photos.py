import os
import uuid
import random
from django.core.files.images import ImageFile
from django.core.files.storage import FileSystemStorage
from datetime import datetime
from django.contrib.gis.geos import Point
from django.core.management.base import BaseCommand
from django.utils import timezone
from core.models import GeoPhoto, TypeOfViolation, User, Place
from api.tasks.photo_geocoding import photo_geocoding
from api.tasks.car_plate_recognition import car_plate_recognition


class Command(BaseCommand):
	help = 'Create 100 or more photos with irregularities in random locations around the center of the place geometry'

	def add_arguments(self, parser):
		parser.add_argument('photos_count', nargs='?', type=int, default=10, help='Number of photos to create')
		parser.add_argument('--random_time', action='store_true', help='Randomize the creation time of the photos')

	def handle(self, *args, **kwargs):
		super_user = User.objects.filter(is_superuser=True).first()
		place_geom = Place.objects.get(name="городское поселение Балаково").geom
		type_of_violation = TypeOfViolation.objects.first()
		
		current_folder = os.path.dirname(os.path.realpath(__file__))
		photo_to_import_folder = os.path.join(current_folder, 'test_photo.jpg')
		
		min_x, min_y, max_x, max_y = place_geom.extent

		curr_timedelta = timezone.timedelta(minutes=0)

		for _ in range(kwargs['photos_count']):
			random_lat = random.uniform(min_y, max_y)
			random_lon = random.uniform(min_x, max_x)
			
			creation_time = datetime.now()
			if kwargs['random_time']:
				creation_time = creation_time - curr_timedelta
				curr_timedelta += timezone.timedelta(minutes=1)
			
			photo = ImageFile(open(photo_to_import_folder, 'rb'), name=f"{str(uuid.uuid4())}.jpg")
			geo_photo = GeoPhoto.objects.create(
				src_photo=photo,
				initial_type_of_violation=type_of_violation,
				author=super_user,
				geom=Point(random_lon, random_lat),
				captured_at=creation_time,
				created_at=creation_time
			)
			
			fs = FileSystemStorage(location='/container_image_exchange')
			ext = os.path.splitext(geo_photo.src_photo.name)[-1].lower()
			filename = f"{geo_photo.id}_src_photo{ext}"
			fs.save(filename, photo)
			
			print(f'GeoPhoto with geom ({random_lat}, {random_lon}) created')
			tasks = [str(photo_geocoding.delay(geo_photo.id)), str(car_plate_recognition.delay(geo_photo.id))]
			print(f'Background tasks created: {tasks}')
