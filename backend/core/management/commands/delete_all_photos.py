from django.core.management.base import BaseCommand
from core.models import GeoPhoto


class Command(BaseCommand):
    help = 'Delete all photos from the database'

    def handle(self, *args, **kwargs):
        GeoPhoto.objects.all().delete()
