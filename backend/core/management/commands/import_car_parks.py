import os

from django.core.management.base import BaseCommand
from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import GEOSGeometry

from core.models import CarPark

codec = 'latin-1'


class Command(BaseCommand):
    help = 'creation of CarPark model objects based on GeoJSON'

    def handle(self, *args, **kwargs):
        current_dir = os.path.dirname(os.path.realpath(__file__))

        geojson_file = os.path.join(current_dir, 'car_parks_to_import/balakovo-car-parks.geojson')
        ds = DataSource(geojson_file)

        for layer in ds:
            for feature in layer:
                is_active = feature.get('is_active')
                total_spaces = feature.get('total_spaces')
                monthly_rent = feature.get('monthly_rent')
                phone_numbers = feature.get('phone_numbers')
                address = feature.get('address')
                geom = GEOSGeometry(feature.geom.wkt, srid=4326)

                CarPark.objects.create(is_active=is_active, total_spaces=total_spaces, monthly_rent=monthly_rent, phone_numbers=phone_numbers, address=address, geom=geom)
