from django.core.management.base import BaseCommand
from django.contrib.gis.geos import Point
from django.contrib.gis.db.models.functions import Distance

from core.models import CarPark,GeoPhoto


class Command(BaseCommand):
    help = 'Redefining the distance to the nearest parking lot'

    def handle(self, *args, **kwargs):
        photos = GeoPhoto.objects.all()

        for photo in photos:
            closest_car_park = CarPark.objects.filter(is_active=True).annotate(distance=Distance('geom', Point(photo.geom.x,photo.geom.y, srid=4326))).order_by('distance').first()
            if closest_car_park is not None and closest_car_park.distance.m < 1000:
                photo.nearest_parking_distance = closest_car_park.distance.m
                photo.save(update_fields=["nearest_parking_distance"])
