from django.core.management.base import BaseCommand
from core.models import GeoPhoto, CarPlate, Violation, User


class Command(BaseCommand):
    help = 'Confirm all objects in the database.'

    def handle(self, *args, **kwargs):
        su = User.objects.filter(is_superuser=True).first()
        GeoPhoto.objects.all().update(address_confirmed=True, moderation_status='confirmed', moderator=su)
        CarPlate.objects.all().update(number_confirmed=True, moderation_status='confirmed', moderator=su)
        Violation.objects.all().update(type_confirmed=True, moderation_status='confirmed', moderator=su, is_published=True)
