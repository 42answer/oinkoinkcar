from django.contrib.gis.forms.widgets import OSMWidget
from django.utils.safestring import mark_safe
from django.templatetags.static import static


class LocalOSMWidget(OSMWidget):
    class Media:
        css = {
            'all': (static('ol/ol.css'),)
        }
        js = (static('ol/ol.js'),)

    def render(self, name, value, attrs=None, renderer=None):
        html = super().render(name, value, attrs, renderer)
        return mark_safe(html)
