#!/bin/bash
set -a

export STAGE="staging"

export REPLICA=1

./app.runscript.sh "$@"
