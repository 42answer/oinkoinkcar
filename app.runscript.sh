#!/bin/bash
set -a

# TODO: We must check that environment servers are running

if [ -z "$STAGE" ]; then
    echo "\$STAGE is not set. Please set it to 'staging' or 'prod' before running this script."
    exit 1
fi

if [ "$STAGE" != "staging" ] && [ "$STAGE" != "prod" ]; then
    echo "\$STAGE is set to '$STAGE'. Please set it to 'staging' or 'prod' before running this script."
    exit 1
fi

echo "Running in $STAGE mode. Replica #$REPLICA"

DEBUG=0
DOWN=0
FORCE_RECREATE=""
BUILD=0
MIGRATE=0
RUN=0
SHOWHELP=0
LOGS=0
NO_FLAGS=1

while [[ $# -gt 0 ]]; do
    case "$1" in
        -d|--debug)
            DEBUG=1
            ;;
        --down)
            DOWN=1
            NO_FLAGS=0
            ;;
        -f|--force)
            FORCE_RECREATE="--force-recreate"
            ;;
        -b|--build)
            BUILD=1
            NO_FLAGS=0
            ;;
        -m|--migrate)
            MIGRATE=1
            NO_FLAGS=0
            ;;
        -r|--run)
            RUN=1
            NO_FLAGS=0
          ;;
        -l|--logs)
            LOGS=1
            NO_FLAGS=0
          ;;
        -h|--help)
            SHOWHELP=1
            ;;
        *)
            echo "Unknown option: $1, use --help to see the available options."
            exit 1
            ;;
    esac
    shift
done

if [ $SHOWHELP -eq 1 ] || [ $NO_FLAGS -eq 1 ]; then
    echo "Usage: ./runscript.sh [OPTIONS]"
    echo "Options:"
    echo "  -d, --debug          Run the server in debug mode."
    echo "  --down               Stop and remove the containers."
    echo "  -f, --force          Force recreate the containers."
    echo "  -b, --build          Build the containers."
    echo "  -m, --migrate        Run the database migrations."
    echo "  -r, --run            Run the backend server."
    echo "  -l, --logs           Show the logs of the running containers."
    echo "  -h, --help           Show this help message."
    exit 0
fi

if [ ! -f ./frontend/mdb/mdb.tar ]; then
    echo "The frontend/mdb/mdb.tar file does not exist. Please read the PROPRIETARY_SOFTWARE_DEPENDENCY_NOTICE.MD file to see how to get it."
    exit 1
fi

if command -v git-lfs &>/dev/null; then
    echo "Checking if the Git LFS files are downloaded..."
    git lfs pull
else
    echo "Git LFS is not installed. Please install it before running this script."
    echo "You can install it by running the following command: sudo apt-get install git-lfs"
    exit 1
fi

echo "Checking if the .oinkrc.$STAGE file exists..."
if [ ! -f ./.oinkrc."$STAGE" ]; then
    echo "The .oinkrc.$STAGE file does not exist. Please create it before running this script."
    exit 1
fi

echo "Sourcing the .oinkrc.$STAGE file..."
# shellcheck source=./oinkrc.$STAGE
source ./.oinkrc."$STAGE"

if [ -z "$REPLICA" ]; then
    echo "\$REPLICA is not set. Please set it to the number of current replica before running this script."
    exit 1
fi

export COMPOSE_PROJECT_NAME=oink-"$STAGE"-"$REPLICA"
export BACKEND_URL="http://backend-${STAGE}-${REPLICA}:8000"

USER=oinkuser
GROUP=oinkgroup
USER_ID=1000
GROUP_ID=1000

export USER GROUP USER_ID GROUP_ID

if [ $DOWN -eq 1 ]; then
    echo "Stopping and removing the containers..."
    docker compose -f docker-compose.prod.app.yml -p "$COMPOSE_PROJECT_NAME" down
    exit 0
fi

if [ -n "$YAC_LOCKBOX_SID" ]; then
    echo "\$YAC_LOCKBOX_SID is found. Loading secrets from Yandex.Cloud Lockbox..."
    IAM_TOKEN="$(yc iam create-token)"
    secrets=$(curl -X GET -H "Authorization: Bearer ${IAM_TOKEN}" https://payload.lockbox.api.cloud.yandex.net/lockbox/v1/secrets/$YAC_LOCKBOX_SID/payload)

    # Thanks a lot to the author of this gist https://gist.github.com/IAmStoxe/a36b6f043819fad1821e7cfd7e903a5b
    for row in $(echo "${secrets}" | jq -r '.entries | .[] | @base64'); do
        _jq() {
        echo "${row}" | base64 --decode | jq -r "${1}"
        }

        name=$(_jq '.key')
        value=$(_jq '.textValue')

        export "$name"="$value"
    done
else
    echo "\$YAC_LOCKBOX_SID is not set. Skipping loading secrets from Yandex.Cloud Lockbox..."
fi

export CELERY_WORKER_RUN_COMMAND="celery -A config worker --autoscale=2,1 -l info --concurrency=2 -n worker1@${APP_DOMAIN}"

if [ $DEBUG -eq 1 ]; then
    export BACKEND_RUN_SRVR_COMMAND="python manage.py runserver 0.0.0.0:8000"
else
    export BACKEND_RUN_SRVR_COMMAND="gunicorn --timeout 120 config.wsgi:application --bind \"0.0.0.0:8000\" --workers 2 --threads 2 --log-level info --enable-stdio-inheritance"
fi

export DJANGO_ALLOWED_HOSTS="$DJANGO_ALLOWED_HOSTS backend-${STAGE}-${REPLICA}"

if [ -z "$CELERY_RESULT_BACKEND_HOST" ]; then
    export CELERY_RESULT_BACKEND_HOST="redis-${STAGE}-${REPLICA}"
fi

if [ -z "$CELERY_BROKER_HOST" ]; then
    export CELERY_BROKER_HOST="redis-${STAGE}-${REPLICA}"
fi

if [ $LOGS -eq 1 ]; then
    echo "Showing the logs of the running containers..."
    docker compose -f docker-compose.prod.app.yml -p "$COMPOSE_PROJECT_NAME" logs -f
    exit 0
fi

if [ $BUILD -eq 1 ]; then
    echo "Building the containers..."

    if [ -f ./traefik/conf-prod/traefik-conf-prod-001.yml ]; then
        cp ./traefik/conf-prod/traefik-conf-prod-001.yml ./traefik/conf-prod/traefik-conf-prod-001.yml.bak
    fi

    git stash
    git pull

    # rename the traefik config file (overwrite the existing one)
    if [ -f ./traefik/conf-prod/traefik-conf-prod-001.yml.bak ]; then
        mv ./traefik/conf-prod/traefik-conf-prod-001.yml.bak ./traefik/conf-prod/traefik-conf-prod-001.yml
    fi

    if [ ! -d ./nginx/static-build/"$STAGE"/"$REPLICA"/front-static ]; then
        mkdir -p ./nginx/static-build/"$STAGE"/"$REPLICA"/front-static
    fi

    if [ ! -d ./nginx/static-build/"$STAGE"/"$REPLICA"/parkings-static ]; then
        mkdir -p ./nginx/static-build/"$STAGE"/"$REPLICA"/parkings-static
    fi

    if [ ! -d ./nginx/static-build/"$STAGE"/"$REPLICA"/django-static ]; then
        mkdir -p ./nginx/static-build/"$STAGE"/"$REPLICA"/django-static
    fi

    echo "Stopping and removing the containers before building..."
    docker compose -f docker-compose.prod.app.yml -p "$COMPOSE_PROJECT_NAME" down

    delete_files_as_root() {
        sudo rm -rf ./nginx/static-build/"$STAGE"/"$REPLICA"/front-static/*
        sudo rm -rf ./nginx/static-build/"$STAGE"/"$REPLICA"/parkings-static/*
        sudo rm -rf ./nginx/static-build/"$STAGE"/"$REPLICA"/django-static/*
    }

    delete_files_as_root

    cp -r ./static/ol ./nginx/static-build/"$STAGE"/"$REPLICA"/django-static

    docker compose -f docker-compose.prod.app.yml -p "$COMPOSE_PROJECT_NAME" build
fi

if [ $MIGRATE -eq 1 ]; then
    echo "Running the database migrations..."
    docker compose -f docker-compose.prod.app.yml -p "$COMPOSE_PROJECT_NAME" run --rm backend python manage.py migrate
fi

if [ $RUN -eq 1 ]; then
    echo "Running the backend server..."
    docker compose -f docker-compose.prod.app.yml -p "$COMPOSE_PROJECT_NAME" up $FORCE_RECREATE
fi

exit 0
