# Уведомление о зависимости от несвободного ПО

В настоящее время этот проект частично зависит от стороннего ПО, распространяемого под [EULA](https://mdbootstrap.com/general/license/#license-commercial:~:text=according%20to%20the-,End%20User%20License%20Agreement,-(EULA)%20detailed%20below) и пренадлежащего  StartupFlow s.c. (MDB / MDBootstrap).

Зависимость выражается в использовании на стороне фронтэнда следующих компонентов, доступных только [в платных подписках](https://mdbootstrap.com/docs/vue/pro/#section-pricing:~:text=1%20project%20(domain/app)).
Перечень используемых платных компонентов:

- [MDBToast](https://mdbootstrap.com/docs/vue/components/toasts/)
- [MDBAlert](https://mdbootstrap.com/docs/vue/components/alerts/)
- [MDBStepper](https://mdbootstrap.com/docs/vue/components/stepper/)
- [MDBPopconfirm](https://mdbootstrap.com/docs/vue/components/popconfirm/)
- [MDBLightbox](https://mdbootstrap.com/docs/vue/components/lightbox/)
- [MDBChip](https://mdbootstrap.com/docs/vue/components/chips/)
- [MDBLoading](https://mdbootstrap.com/docs/vue/methods/loading-management/)
- [MDBDatepicker](https://mdbootstrap.com/docs/vue/forms/datepicker/)
- [MDBSelect](https://mdbootstrap.com/docs/vue/forms/select/)

Все исходные коды проекта ХрюХрюКар доступны [под открытой лицензией MIT](https://gitlab.com/theansweris42/oinkoinkcar/-/blob/main/LICENSE), однако в настоящее время запустить проект в полном объеме без выполнения дополнительных действий не получится.

# Что это означает?
## Вариантов решения несколько:

### 1. Покупка MDB Pro для Vue
Купить любую платную подписку на MDB Vue (достаточно самого дешевого варианта на 1 проект и 1 домен), по которой Вам
будет предоставлен доступ к закрытым репозиториям MDB, содержащим PRO Components (core UI Kit). Из указанного репозитория
Вам будет необъходимо сформировать пакет `mdb.tar` И положить его в директорию `frontend/mdb`. Указанный пакет это архив со следующей структурой папок и файлов:

```
package/
|-- types/
|   |-- ...
|-- src/
|   |-- ...
|-- js/
|   |-- ...
|-- css/
|   |-- ...
|-- package.json
|-- README.md
|-- ...
```

После покупки вы будете иметь доступ к закрытым репозиториям и примерам кода на сайте [MDBootstrap](https://mdbootstrap.com/) в течение 1 года.
После окончания срока действия подписки вы можете продолжить использовать MDB Pro, но без доступа к закрытым репозиториям и примерам кода.
Указанный вариант не является рекламой, а лишь информацией о возможности решения проблемы. Когда мы начинали проект, мы выбирали UI-фреймворк, исходя из своих потребностей и возможностей.
Команда проекта ХрюХрюКар не имеет никакого отношения к MDBootstrap/StartupFlow s.c. и не получает никакой выгоды от продажи их продуктов. 

### 2. Замена компонентов
Все указанные компоненты помечены в исходном коде проекта ХрюХрюКар следующим блоком кода:

```html
<!-- Warning about the use of a non-free component. Attention! The MDBToast component is provided under the "MDB Vue Essential license" (EULA), 
     which prohibits the distribution of source code as part of open-source software. To run the "frontend" app of the OinkOinkCar system, you 
     must either purchase the specified license from the provider (https://mdbootstrap.com/docs/vue/pro/#section-pricing) or make changes to 
     this source code to eliminate the use of non-free components.
-->
```

Вы можете заменить эти компоненты на аналогичные из других библиотек, либо написать свои компоненты.
Мы с радостью рассмотрим и примем ваши запросы на слияние и избавимся от зависимости от несвободного ПО, но просим заранее согласовать план действий с нами, для чего вы можете создать [задачу](https://gitlab.com/theansweris42/oinkoinkcar/-/issues) или [обсудить](https://gitlab.com/theansweris42/oinkoinkcar/-/issues) вопрос в открытом доступе.

### 3. Удаление компонентов
Большинство несвободных компонентов используются для удобства пользователей и вы можете удалить их из проекта либо заменить на свободные компоненты MDB (например селектор типов нарушений заменить на модальное окно со списком и кнопками выбора).
В таком случае вам необходимо будет внести правку в файл `frontend/package.json`, заменив строку `"mdb-vue-ui-kit": "file:mdb/mdb.tar",` на `"mdb-vue-ui-kit": "^5.0.0",` и выполнить команду `npm install` в директории `frontend`.
Указанные изменения не обязательно будут приняты в основной репозиторий, но мы с радостью рассмотрим и примем ваши запросы на слияние и избавимся от зависимости от несвободного ПО, если они будут соответствовать нашим целям и задачам.

### 4. Использование проекта без фронтэнда
Вы можете использовать проект ХрюХрюКар для создания собственных проектов, не используя фронтэнд. 
В таком случае вам необходимо будет внести правки в файлы `docker-compose.*.yml`, `dev.sh`, `runscript.sh` и другие. 
Как пример, вы вполне можете реализовать телеграм-бота со схожим функционалом, использую готовый бэкэнд и API ХрюХрюКара.
