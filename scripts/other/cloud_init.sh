#!/bin/sh
# This script is run by cloud-init on first boot of the instance
apt-get update
apt-get install -y --no-install-recommends curl wget jq nano netcat cron

# Install Docker
apt update -y
apt install -y apt-transport-https ca-certificates software-properties-common gnupg2 git apache2-utils

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

apt update -y

apt install docker-ce -y

# Install Docker Compose
mkdir -p ~/.docker/cli-plugins/
curl -SL https://github.com/docker/compose/releases/download/v2.24.3/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose

chmod +x ~/.docker/cli-plugins/docker-compose

systemctl enable docker

curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
