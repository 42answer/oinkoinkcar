sudo apt update \
&& sudo apt upgrade \
&& sudo apt install -y apt-transport-https ca-certificates software-properties-common gnupg2 git apache2-utils \
&& curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg \
&& sudo echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null \
&& sudo apt update -y \
&& sudo apt install docker-ce -y \
&& mkdir -p ~/.docker/cli-plugins/ \
&& curl -SL https://github.com/docker/compose/releases/download/v2.24.5/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose \
&& sudo chmod +x ~/.docker/cli-plugins/docker-compose \
&& sudo systemctl enable docker \
&& sudo wget -O /usr/local/bin/ufw-docker https://github.com/chaifeng/ufw-docker/raw/master/ufw-docker \
&& sudo chmod +x /usr/local/bin/ufw-docker \
&& sudo ufw-docker install \
&& sudo systemctl restart ufw
