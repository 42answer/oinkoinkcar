#!/bin/bash

SUBNET="10.0.0.0/24"

if [ "$(id -u)" -ne 0 ]; then
    echo "This script must be run with sudo" >&2
    exit 1
fi

echo "Allowing outgoing connections on ports 5432 and 465 within the subnet $SUBNET..."
sudo ufw allow out to $SUBNET port 5432
sudo ufw allow out to $SUBNET port 465
echo "Outgoing connections on ports 5432 and 465 to the subnet $SUBNET are allowed."

echo "Allowing incoming connections on ports 80 and 8000 within the subnet $SUBNET..."
sudo ufw allow in from $SUBNET to any port 80
sudo ufw allow in from $SUBNET to any port 8000
echo "Incoming connections on ports 80 and 8000 from the subnet $SUBNET are allowed."

echo "Reloading UFW configuration..."
sudo ufw reload
echo "UFW configuration reloaded."

echo "UFW status:"
sudo ufw status
echo "UFW status shown."
