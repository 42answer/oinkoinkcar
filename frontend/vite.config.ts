import { sentryVitePlugin } from "@sentry/vite-plugin";
// frontend/vite.config.ts
import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from "vite-plugin-pwa";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), VitePWA({
    registerType: "autoUpdate",
    injectRegister: "auto",
    devOptions: {
      enabled: false,
    },
    workbox: {
      maximumFileSizeToCacheInBytes: 10 * 1024 * 1024,
      globPatterns: ["**/*.{js,ts,css,scss,html,ico,png,svg,vue,otf,ttf}"],
      navigateFallback: '/index.html',
      navigateFallbackAllowlist: [
        new RegExp('^/(\\?.*)?$'),
        new RegExp('^/hunting(\\?.*)?$'),
        new RegExp('^/manifesto(\\?.*)?$'),
        new RegExp('^/profile(\\?.*)?$'),
        new RegExp('^/sign-in(\\?.*)?$'),
        new RegExp('^/hunt(\\?.*)?$'),
        new RegExp('^/moderator(\\?.*)?$'),
        new RegExp('^/sitemap(\\?.*)?$'),
        new RegExp('^/sitemap/\\d+(\\?.*)?$'),
        new RegExp('^/error-auth/login-error(\\?.*)?$'),
        new RegExp('^/error-auth/inactive-user(\\?.*)?$'),
        new RegExp('^/privacy(\\?.*)?$'),
        new RegExp('^/rules(\\?.*)?$'),
      ],
    },
    includeAssets: [
      "favicon.ico",
      "Maler.ttf",
      "Veles.otf",
      "btn_google_light_normal.svg",
      "btn_google_light_pressed.svg",
      "logo.png",
      "hunting-wrapper-bgr.jpg",
      "vertical_logo.png",
    ],
    manifest: {
      name: "ХрюХрюКар",
      short_name: "ХрюХрюКар",
      description: "Социальный проект по борьбе с нарушениями автомобилистов, связанными со стоянкой или остановкой в неположенных местах.",
      background_color: "#ffffff",
      theme_color: "#ffffff",
      display: "standalone",
      start_url: "/",
      scope: "/",
      handle_links: "preferred",
      icons: [
        {
          src: "pwa-192x192.png",
          sizes: "192x192",
          type: "image/png",
        },
        {
          src: "pwa-512x512.png",
          sizes: "512x512",
          type: "image/png",
        },
      ],
    },
  }), sentryVitePlugin({
    org: "theansweris42",
    project: "xxkap",
    telemetry: false
  })],

  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },

  server: {
    port: 5173
  },

  css: {
    devSourcemap: true,
  },

  build: {
    sourcemap: true
  }
})