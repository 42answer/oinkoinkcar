// src/api/index.ts is a module that exports all the modules in the api folder.

import api from "./api";
import { authModule } from "./auth";
import { applicationModule } from "./application";
import { objectsModule } from "./objects";
import { mapModule } from "./map";

export { api, authModule, applicationModule, objectsModule, mapModule };
