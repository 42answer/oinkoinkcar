export interface Application {
  id?: string;
  modified_at?: string | null;
  created_at?: string;
  applicant?: string;
  violation?: string;
  powers?: string;
  is_confirmed?: boolean | null;
  sent_at?: string | null;
  pdf_file?: string | null;
  rejected_reason?: string | null;
  text?: string | null;
}
