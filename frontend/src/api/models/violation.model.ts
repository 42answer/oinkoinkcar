import { TypeOfViolation, TypeOfViolationForMap } from '@/api/models/type-of-violation.model'
import { CarPlateForMap, CarPlateForModeration } from '@/api/models/car-plate.model'
import { MODERATION_STATUS } from '@/helpers/enums/moderation-status'

export interface ViolationForModeration {
  id?: string;
  modified_at?: string | null;
  created_at?: string;
  type_confirmed?: boolean | null;
  is_published?: boolean;
  violation_type?: TypeOfViolation | null | string | any;
  car_plate?: CarPlateForModeration;
  is_violator_fined?: boolean | null;
  moderation_status?: MODERATION_STATUS;
}

export interface ViolationForMap {
  id: string;
  modified_at: string | null;
  created_at: string;
  is_published: boolean;
  is_violator_fined: boolean | null;
  moderation_status: MODERATION_STATUS;
  is_author: boolean;
  car_plate: CarPlateForMap;
  violation_type: TypeOfViolationForMap;
  nearest_parking_distance: number | null;
}
