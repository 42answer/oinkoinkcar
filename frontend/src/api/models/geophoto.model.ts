import { MODERATION_STATUS } from '@/helpers/enums/moderation-status';

export interface GeoPhoto {
  id?: string;
  modified_at?: string | null;
  created_at?: string;
  geom: { type: string; coordinates: number[] };
  src_photo: string;
  compressed_photo?: string | null;
  address?: string | null;
  captured_at?: string;
  address_confirmed?: boolean | null;
  author?: string | null;
  accuracy_horizontal?: number | null;
  compass_angle?: number | null;
  is_recognized?: boolean;
  is_violation_selected?: boolean;
  is_geocoded?: boolean;
  initial_type_of_violation?: string | null;
  moderation_status?: MODERATION_STATUS;
  panorama_shot?: string | null;
}

export interface GeoPhotoModerationPatch {
  address?: string;
  address_confirmed?: boolean;
  is_rejected?: boolean;
  panorama_shot?: string | null;
  moderation_status?: MODERATION_STATUS;
  geom?: { type: string; coordinates: number[] };
}

export interface GeoPhotoForMap {
  id: string;
  modified_at: string | null;
  created_at: string;
  captured_at: string;
  geom: { type: string; coordinates: number[] };
  src_photo: string;
  compressed_photo: string | null;
  address: string | null;
  moderation_status: MODERATION_STATUS;
  panorama_shot: string| null;
}
