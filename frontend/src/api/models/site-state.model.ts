import { OPERATION_MODE } from "@/helpers/enums/operation-mode";
import { APP_STAGE } from "@/helpers/enums/app-stage";

export interface SiteState {
  public_mode: OPERATION_MODE;
  message_for_public: string | null;
  moderation_mode: OPERATION_MODE;
  message_for_moderators: string | null;
  stage: APP_STAGE;
  frontend_ver: string;
  tile_layer_url: string;
}
