export interface Applicant {
  id: string;
  modified_at?: string | null;
  created_at: string;
  first_name: string;
  patronymic: string;
  last_name: string;
  email: string;
  phone_number: string | null;
}
