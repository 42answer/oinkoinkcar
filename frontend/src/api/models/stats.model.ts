export interface ViolationStats {
  total_violations_last_30_days: number;
  percentage_total_violations_change: number;
  total_type_confirmed_last_30_days: number;
  percentage_type_confirmed_change: number;
  total_not_confirmed_last_30_days: number;
  percentage_not_confirmed_change: number;
}
