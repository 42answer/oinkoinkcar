import { GeoPhoto, GeoPhotoForMap } from '@/api/models/geophoto.model'
import { MODERATION_STATUS } from '@/helpers/enums/moderation-status'

export interface CarPlate {
  id?: string;
  modified_at?: string | null;
  created_at?: string;
  plate_number: string;
  number_confirmed?: boolean | null;
  b_box?: number[][];
}

export interface CarPlateForModeration extends CarPlate {
  photo?: GeoPhoto;
  moderation_status?: MODERATION_STATUS;
}

export interface CarPlateForMap {
  id: string;
  modified_at: string | null;
  created_at: string;
  plate_number: string;
  moderation_status: MODERATION_STATUS;
  photo: GeoPhotoForMap;
}
