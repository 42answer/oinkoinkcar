export interface TypeOfViolation {
  id: string;
  modified_at?: string | null;
  created_at: string;
  name: string;
  description?: string | null;
  image?: string | null;
  icon?: string | null;
}

export interface TypeOfViolationForMap {
  id: string;
  name: string;
  description: string | null;
  icon: string | null;
}
