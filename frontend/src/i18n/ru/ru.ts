import about from "./about";
import feedback from "./feedback";
import footer from "./footer";
import header from "./header";
import home from "./home";
import profile from "./profile";
import settings from "./settings";
import sitemap from "./sitemap";
import application from "@/i18n/ru/application";
import auth from "@/i18n/ru/auth";
import locale from "@/i18n/ru/locale";
import content from "@/i18n/ru/content";
import filter from "@/i18n/ru/filter";
import hunting  from "@/i18n/ru/hunting";
import mdb from "@/i18n/ru/mdb";
import moderatorView from "@/i18n/ru/moderator-view";
import pageTitles from "@/i18n/ru/page-titles";

const ru = {
  about,
  feedback,
  footer,
  header,
  home,
  profile,
  settings,
  sitemap,
  application,
  auth,
  locale,
  content,
  filter,
  hunting,
  mdb,
  moderatorView,
  pageTitles,
};

export default ru;
