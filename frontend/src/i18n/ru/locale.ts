const locale = {
  localeNotAvailable: "Локаль ${locale} недоступна.",
};

export default locale;
