const locale = {
  localeNotAvailable: "Locale ${locale} is not available",
};

export default locale;
