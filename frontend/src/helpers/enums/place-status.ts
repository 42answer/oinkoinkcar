// src/helpers/enums/place-status.ts
export enum PLACE_STATUS {
  NOT_INIT = 'not_init',
  VIOLATION_TYPES_FOUND = 'violation_types_found',
  VIOLATION_TYPES_NOT_FOUND = 'violation_types_not_found',
  PLACES_NOT_FOUND = 'places_not_found',
}
