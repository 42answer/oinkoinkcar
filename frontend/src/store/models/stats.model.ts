export interface ViolationStats {
  totalViolationsLast30Days: number;
  percentageTotalViolationsChange: number;
  totalTypeConfirmedLast30Days: number;
  percentageTypeConfirmedChange: number;
  totalNotConfirmedLast30Days: number;
  percentageNotConfirmedChange: number;
}
