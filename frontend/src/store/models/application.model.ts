import { BaseModel } from './base.model';
import { APPLICATION_REJECT_REASON } from '@/helpers/enums/application-reject-reason'

export interface Application extends BaseModel {
  applicant?: string;
  violation?: string;
  powers?: string;
  isConfirmed?: boolean | null;
  sentAt?: Date | null;
  pdfFile?: string | null;
  rejectedReason?: string | null | APPLICATION_REJECT_REASON;
  text?: string | null;
}
