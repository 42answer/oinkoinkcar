import { OAUTH_PROVIDER } from "@/helpers/enums/oauth-provider";

interface AbstractUser {
  username: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  isStaff?: boolean;
  isActive?: boolean;
  isSuperuser?: boolean;
  dateJoined?: Date;
}

export interface User extends AbstractUser {
  id: string;
  modifiedAt?: Date | null;
  createdAt?: Date;
  oauthProvider?: OAUTH_PROVIDER;
  isModerator?: boolean;
  isBanned?: boolean;
}
