import { BaseModel } from './base.model';

export interface TypeOfViolation extends BaseModel {
  name: string;
  description?: string | null;
  image?: string | null;
  icon?: string | null;
}

export interface TypeOfViolationForSelect {
  text: string;
  value: string;
  disabled?: boolean;
  icon?: string;
}

export interface TypeOfViolationForMap {
  id: string;
  name: string;
  description: string | null;
  icon: string | null;
}
