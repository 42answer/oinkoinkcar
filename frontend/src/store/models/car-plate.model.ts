// src/store/models/car-plate.model.ts
import { BaseModel, ModerationMixin } from '@/store/models/base.model'
import { GeoPhoto, GeoPhotoForMap } from '@/store/models/geophoto.model'

export interface CarPlate extends BaseModel {
  plateNumber?: string;
  numberConfirmed?: boolean | null;
  bBox?: number[][];
}

export interface CarPlateForModeration extends CarPlate, ModerationMixin {
  photo?: GeoPhoto;
}

export interface CarPlateForMap extends BaseModel, ModerationMixin {
  plateNumber: string;
  photo: GeoPhotoForMap;
}
