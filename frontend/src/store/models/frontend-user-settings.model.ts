export interface FrontendUserSettings {
  id: string;
  modifiedAt?: Date | null;
  createdAt: Date;
  showInfoMessages: boolean;
  showWarningMessages: boolean;
  showErrorMessages: boolean;
  showDebugMessages: boolean;
}
