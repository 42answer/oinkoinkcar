// src/store/models/application/system-message.model.ts
import { SYSTEM_MESSAGE_TYPE } from "@/helpers/enums/system-message-type";

export interface SystemMessage {
  id: string;
  type: SYSTEM_MESSAGE_TYPE;
  message: string;
  createdAt: Date;
  closedAt?: Date;
  isClosed: boolean;
  isToast: boolean;
  showOnTop: boolean;
  toastDelay: number;
}
