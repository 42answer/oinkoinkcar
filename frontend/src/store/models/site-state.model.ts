import { OPERATION_MODE } from "@/helpers/enums/operation-mode";
import { APP_STAGE } from "@/helpers/enums/app-stage";

export interface SiteState {
  publicMode: OPERATION_MODE;
  messageForPublic: string | null;
  moderationMode: OPERATION_MODE;
  messageForModerators: string | null;
  stage: APP_STAGE;
  frontendVer: string;
  frontendVerBuild: string;
  tileLayerUrl: string;
}
