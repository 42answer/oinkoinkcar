// src/store/models/violation.model.ts
import { TypeOfViolation, TypeOfViolationForMap } from '@/store/models/type-of-violation.model'
import { CarPlateForModeration } from '@/store/models/car-plate.model'
import { BaseModel, ModerationMixin } from '@/store/models/base.model'
import { CarPlateForMap } from '@/store/models/car-plate.model'

export interface ViolationForModeration extends BaseModel, ModerationMixin {
  typeConfirmed?: boolean | null;
  isPublished?: boolean;
  violationType?: TypeOfViolation;
  carPlate?: CarPlateForModeration;
  isViolatorFined?: boolean | null;
}

export interface ViolationForMap extends BaseModel, ModerationMixin {
  isPublished?: boolean;
  isViolatorFined?: boolean | null;
  isAuthor?: boolean;
  carPlate?: CarPlateForMap;
  violationType?: TypeOfViolationForMap;
  nearestParkingDistance?: number | null;
}

export interface ViolationDetails extends ViolationForMap {
  isNotFound: boolean;
}
