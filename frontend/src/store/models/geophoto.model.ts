// src/store/models/geophoto.model.ts
import { BaseModel, ModerationMixin } from '@/store/models/base.model'
import { User } from '@/store/models/user.model'

export interface GeoPhoto extends BaseModel, ModerationMixin {
  geom: { type: string; coordinates: number[] };
  srcPhoto: string;
  compressedPhoto?: string | null;
  address: string | null;
  capturedAt?: Date;
  addressConfirmed?: boolean | null;
  author?: User | null;
  accuracyHorizontal?: number | null;
  compassAngle?: number | null;
  isRecognized?: boolean;
  isViolationSelected?: boolean;
  isGeocoded?: boolean;
  localId?: string;
  initialTypeOfViolation?: string | null;
  uploaded: boolean;
  panoramaShot?: string;
}

export interface GeoPhotoModerationPatch extends ModerationMixin {
  address?: string;
  addressConfirmed?: boolean;
  isRejected?: boolean;
  panoramaShot?: string;
  geom?: { type: string; coordinates: number[] };
}

export interface GeoPhotoForMap extends BaseModel, ModerationMixin {
  geom: [number, number];
  srcPhoto: string;
  compressedPhoto: string | null;
  address: string | null;
  panoramaShot: string | null;
  capturedAt: Date;
}
