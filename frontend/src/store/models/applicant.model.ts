import { BaseModel } from '@/store/models/base.model'

export interface Applicant extends BaseModel {
  id: string;
  firstName: string;
  patronymic: string;
  lastName: string;
  email: string;
  phoneNumber: string | null;
}
