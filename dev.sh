#!/bin/bash
set -a

export STAGE="dev"
echo "Running in $STAGE mode."

if [ ! -f ./frontend/mdb/mdb.tar ]; then
    echo "The frontend/mdb/mdb.tar file does not exist. Please read the PROPRIETARY_SOFTWARE_DEPENDENCY_NOTICE.MD file to see how to get it."
    exit 1
fi

if command -v git-lfs &>/dev/null; then
    echo "Checking if the Git LFS files are downloaded..."
    git lfs pull
else
    echo "Git LFS is not installed. Please install it before running this script."
    echo "You can install it by running the following command: sudo apt-get install git-lfs"
    exit 1
fi

echo "Checking if the .oinkrc file exists..."
if [ ! -f ./.oinkrc ]; then
    echo "The .oinkrc file does not exist. Please create it before running this script."
    exit 1
fi

echo "Sourcing the .oinkrc file..."

source ./.oinkrc

USER=$(whoami)
GROUP=$(whoami)
USER_ID=$(id -u)
GROUP_ID=$(id -g)

if [ -z "$USER" ]; then
  USER=oinkuser
fi

if [ -z "$GROUP" ]; then
  GROUP=oinkgroup
fi

if [ -z "$USER_ID" ]; then
  USER_ID=1000
fi

if [ -z "$GROUP_ID" ]; then
  GROUP_ID=1000
fi

export USER GROUP USER_ID GROUP_ID

DEBUG=0
FORCE_RECREATE=""
BUILD=0
MAKEMIGRATIONS=0
MIGRATE=0
CREATESUPERUSER=0
RUNSERVER=0
SHOWHELP=0
NO_FLAGS=1

while [[ $# -gt 0 ]]; do
    case "$1" in
        -d|--debug)
            DEBUG=1
            ;;
        -f|--force)
            FORCE_RECREATE="--force-recreate"
            ;;
        -b|--build)
            BUILD=1
            NO_FLAGS=0
            ;;
        --makemigrations)
            MAKEMIGRATIONS=1
            NO_FLAGS=0
            ;;
        --migrate)
            MIGRATE=1
            NO_FLAGS=0
            ;;
        --createsuperuser)
            CREATESUPERUSER=1
            NO_FLAGS=0
            ;;
        -r|--runserver)
            RUNSERVER=1
            NO_FLAGS=0
          ;;
        -h|--help)
            SHOWHELP=1
            ;;
        *)
            echo "Unknown option: $1, use --help to see the available options."
            exit 1
            ;;
    esac
    shift
done

if [ $SHOWHELP -eq 1 ] || [ $NO_FLAGS -eq 1 ]; then
    echo "Usage: ./runscript.sh [OPTIONS]"
    echo "Options:"
    echo "  -d, --debug         Run the server in debug mode."
    echo "  -f, --force         Force recreate the containers."
    echo "  -b, --build         Build the containers."
    echo "  --makemigrations    Create the database migrations."
    echo "  --migrate           Run the database migrations."
    echo "  --createsuperuser   Create the superuser."
    echo "  -r, --runserver     Run the backend server."
    echo "  -h, --help          Show this help message."
    exit 0
fi

if [ ! -d "$HOME/oink-database" ]; then
    echo "Creating the $HOME/oink-database directory..."
    mkdir -p $HOME/oink-database
fi

if [ ! -d "./static/admin" ]; then
    echo "Creating the ./static/* directories..."
    mkdir -p ./static/admin
    mkdir -p ./static/rest_framework
    mkdir -p ./static/gis
fi

if [ $DEBUG -eq 1 ]; then
    # FIXME: hardcoded backend ports
    export BACKEND_RUN_SRVR_COMMAND="python manage.py runserver 0.0.0.0:8000"
    export NOMEROFF_NET_RUN_SRVR_COMMAND="flask run -h 0.0.0.0 -p 3116"
else
    CPU_COUNT=$(nproc)
    CPU_COUNT_NOMEROFF=$((CPU_COUNT / 2))
    if [ $CPU_COUNT_NOMEROFF -lt 2 ]; then
        CPU_COUNT_NOMEROFF=2
    fi
    if [ "$STAGE" = "staging" ]; then
        GUNICORN_TIMEOUT=300
    else
        GUNICORN_TIMEOUT=120
    fi
    GUNICORN_TIMEOUT_COMMAND="--timeout $GUNICORN_TIMEOUT"
    # FIXME: hardcoded ports
    export BACKEND_RUN_SRVR_COMMAND="gunicorn $GUNICORN_TIMEOUT_COMMAND config.wsgi:application --bind \"0.0.0.0:8000\" --workers $CPU_COUNT --threads $CPU_COUNT --log-level info --enable-stdio-inheritance"
    export NOMEROFF_NET_RUN_SRVR_COMMAND="gunicorn $GUNICORN_TIMEOUT_COMMAND main:app --threads $CPU_COUNT_NOMEROFF --workers $CPU_COUNT_NOMEROFF --log-level info --bind \"0.0.0.0:3116\""
fi

if [ $BUILD -eq 1 ]; then
    echo "Building the containers..."
    docker compose -f docker-compose.dev.yml -p oink build
fi

if [ $MAKEMIGRATIONS -eq 1 ]; then
    echo "Creating the database migrations..."
    docker compose -f docker-compose.dev.yml -p oink run --rm backend python manage.py makemigrations
fi

if [ $MIGRATE -eq 1 ]; then
    echo "Running the database migrations..."
    docker compose -f docker-compose.dev.yml -p oink run --rm backend python manage.py migrate
fi

if [ $CREATESUPERUSER -eq 1 ]; then
    echo "Creating the superuser..."
    docker compose -f docker-compose.dev.yml -p oink run --rm backend python manage.py createsuperuser --noinput
fi

if [ $RUNSERVER -eq 1 ]; then
    echo "Running the backend server..."
    docker compose -f docker-compose.dev.yml -p oink up $FORCE_RECREATE
fi

echo "Script finished. You can pass --help or -h to see the available options."
exit 0
